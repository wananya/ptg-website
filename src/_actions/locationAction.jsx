import { Base_API } from "../_constants/matcher";
import axios from "axios";

export const ShopAction = {
    getShopProduct,
    getPTLocation,
    GetShopById,
    GetProvince,
    GetAmphure
};

function getShopProduct() {
    return axios.get(`${Base_API.apis}/shop/GetProduct`).then(res => {
        return res.data;
    });
}

function getPTLocation(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist) {
    return axios.get(`${Base_API.apis}/Location/GetLocationList?pageNo=${pageNo}&lang=${lang}&textsearch=${textsearch}&distance=${distance}&latitude=${lat}&longitude=${lng}&province=${province}&amphure=${amphure}&isMap=${isMap}&productlist=${productlist}`).then(res => {
        return res.data;
    });
}

function GetShopById(lang, shopId, lat, lng) {
    return axios.get(`${Base_API.apis}/shop/GetShopById?lang=${lang}&shopId=${shopId}&latitude=${lat}&longitude=${lng}`).then(res => {
        return res.data;
    });
}


function GetProvince() {
    return axios.get(`${Base_API.apis}/Promotion/GetProvince`).then(res => {
        return res.data;
    });
}

function GetAmphure(formData) {
    return axios.post(`${Base_API.apis}/WebView/GetDropDownAMPHURE`, formData).then(res => {
        return res.data;
    });
}

