import React from 'react';
import '../../register/register.css';
import moment from "moment-timezone";
import 'moment/locale/th';
import DatePicker from 'react-datepicker';
import { registerAction } from '../../../_actions/registerActions'; 

const monthMap = {
    '1': '01',
    '2': '02',
    '3': '03',
    '4': '04',
    '5': '05',
    '6': '06',
    '7': '07',
    '8': '08',
    '9': '09',
    '10': '10',
    '11': '11',
    '12': '12',
  };
  
  const dateConfig = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: 'YYYY'
    },
  }
  
  const dateConfigTH = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: value => moment(value).add(543,'y').format('YYYY')
    },
  }

export class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            errors: {},
            errorsFocus:{},
            setActive: "active",
            fields : {
                PHONE_NO:'',
                EMAIL:'',
                PASSWORD:'',
                CHECKPASSWORD:'',
                FIRSTNAME:'',
                LASTNAME:'',
                BIRTHDATE:'',
                GENDER:''
            },
            show:false,
            modal:null,
            time: new Date(),
            isOpenDatepickerMobile: false,
            errors: {},
            errorsFocus: {},
            email:'',
            dp:false
        };
        // this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        let { fields , dp } = this.state;
        console.log(this.props.location.state)
        if(this.props.location.state) {
            let newObject = Object.assign(this.props.location.state , this.state.fields)
            fields = newObject
            this.setState({ fields })
        }
        if(this.props.history.location.state.data !== "" && this.props.history.location.state.type === 0){
            fields['EMAIL'] = this.props.history.location.state.data;
            dp = true
            this.setState({ email :  this.props.history.location.state.data , fields , dp})
        }else if(this.props.history.location.state.data !== "" && this.props.history.location.state.type === 1){
            fields['PHONE_NO'] = this.props.history.location.state.data;
            dp = true
            this.setState({ tel : this.props.history.location.state.data , fields , dp})
        }else{
            this.props.history.location.state.data = '';
        }
        if(this.props.location.state.phone !== ""){
            fields['PHONE_NO'] = this.props.history.location.state.phone;
            dp = true
        }
        if(this.props.location.state.email !== ""){
            fields['EMAIL'] = this.props.history.location.state.email;
            dp = true
        }
    }

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    handChange(e){
        let { errors,errorsFocus, fields } = this.state;
        if (e.target.name === "PHONE_NO") {
          const re = /^[0-9\b]+$/;
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            errors[e.target.name] = 'กรุณากรอกเฉพาะตัวเลข'
            errorsFocus[e.target.name] = ''
            e.preventDefault();
          }
        } else {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        this.setState({ errors, fields , errorsFocus });
    }

    async handleSubmit(event) {
        console.log(event)
        var { fields } = this.state;
        event.preventDefault();
        if (await this.validateForm()) {
            sessionStorage.setItem('inputBody', JSON.stringify(this.state.fields))
            var formData = new FormData();
            formData.append('socialId' , '')
            formData.append('socialType' , '')
            formData.append('phoneNo' , this.state.fields['PHONE_NO'])
            formData.append('email' , this.state.fields['EMAIL'])
            formData.append('password' , this.state.fields['PASSWORD'])
            formData.append('preName' , '0')
            formData.append('firstName' , this.state.fields['FIRSTNAME'])
            formData.append('lastName' , this.state.fields['LASTNAME'])
            formData.append('birthDay' , this.state.fields['BIRTH_DATE'])
            formData.append('gender' , this.state.fields['GENDER'])
            formData.append('udId' , this.state.fields.udId)
            formData.append('tokenId' , '')
            formData.append('playerId' , this.state.fields.playerId)
            registerAction.registerWebSite(formData).then(e => {
                console.log(e)
                if(e.data.isSuccess === true){
                    this.setState({ 
                        userid : e.data.data.userId ,
                        phone : e.data.data.phoneNo , 
                        mail : e.data.data.email,
                        password:e.data.data.password ,
                        cardno : e.data.data.ptCardNo ,
                        prename : e.data.data.preName ,
                        firstname : e.data.data.firstName ,
                        lastname : e.data.data.lastName , 
                        birthdate : e.data.data.birthDay,
                        idcard : e.data.data.idCard ,
                        pinCode: e.data.data.pinCode ,
                        gerder:e.data.data.gender ,
                        facebookId : e.data.data.facebookId , 
                        gmailId : e.data.data.gmailId,
                        isActive : e.data.data.isActive,
                        tokeN_ID : e.data.data.tokeN_ID,
                        customeR_ID : e.data.data.customeR_ID,
                        citizeN_TYPE_ID : e.data.data.citizeN_TYPE_ID,
                        citizeN_ID : e.data.data.citizeN_ID,
                        titlE_NAME_ID : e.data.data.titlE_NAME_ID ,
                        titlE_NAME_REMARK : e.data.data.titlE_NAME_REMARK ,
                        fulL_ADDRESS : e.data.data.full_ADDRESS ,
                        customeR_STATUS : e.data.data.customeR_STATUS ,
                        isMemberPTG : e.data.data.isMemberPTG ,
                        tokenMobileApp : e.data.data.tokenMobileApp ,
                    })
                    let formData = new FormData();
                    formData.append('userName' , e.data.data.userId);
                    formData.append('password' , this.state.fields['PASSWORD']);
                    formData.append('udId',this.state.fields.udId);
                    formData.append('tokenId','');
                    formData.append('playerId',this.state.fields.playerId);
                    registerAction.loginWebsite(formData).then(e => {
                        console.log(e)
                        if(e.data.isSuccess === true){
                            localStorage.setItem("user", e.data.data.firstName)
                            localStorage.setItem("login", "true")
                            localStorage.setItem('uid',e.data.data.userId)
                            localStorage.setItem('phone' , e.data.data.phoneNo)
                            localStorage.setItem('tkmb' , e.data.data.tokenMobileApp)
                            localStorage.setItem("lastname", e.data.data.lastName)
                            localStorage.setItem('email',e.data.data.email)
                            localStorage.setItem('birthdate',e.data.data.birthDay)
                            window.location.href = `${process.env.PUBLIC_URL}/home`
                        }
                    })
                }
          })
        } else {
          console.log('formsubmit ' + false);
        }
        this.setState({ fields })
      }
    
      async validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(!fields["PHONE_NO"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["PHONE_NO"] = 'errorFocus'
            errors["PHONE_NO"] = 'กรุณากรอกเบอร์โทรศัพท์'
        }
        if(!fields["EMAIL"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["EMAIL"] = 'errorFocus'
            errors["EMAIL"] = 'กรุณากรอกอีเมล'
        }
        if(!fields["PASSWORD"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["PASSWORD"] = 'errorFocus'
            errors["PASSWORD"] = 'กรุณากรอกรหัสผ่าน'
        }
        if(!fields["CHECKPASSWORD"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["CHECKPASSWORD"] = 'errorFocus'
            errors["CHECKPASSWORD"] = 'กรุณากรอกรหัสผ่านอีกครั้ง'
        }
        if(fields["PASSWORD"] !== fields["CHECKPASSWORD"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["CHECKPASSWORD"] = 'errorFocus'
            errors["CHECKPASSWORD"] = 'รหัสผ่านไม่ตรงกัน'
        }
        if(!fields["FIRSTNAME"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["FIRSTNAME"] = 'errorFocus'
            errors["FIRSTNAME"] = 'กรุณากรอกชื่อ'
        }
        if(!fields["LASTNAME"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["LASTNAME"] = 'errorFocus'
            errors["LASTNAME"] = 'กรุณากรอกนามสกุล'
        }
        if(!fields["BIRTH_DATE"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["BIRTH_DATE"] = 'errorFocus'
            errors["BIRTH_DATE"] = 'กรุณาเลือกวัน/เดือน/ปีเกิด'
        }else{
            var dateBeforDiff = moment(fields["BIRTH_DATE"]).format("YYYY-MM-DD");
            var diff = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "years");
            var diffDay = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "days");
            if (diff < 7 || diffDay <= 2556) {
                formIsValid = false;
                errorsFocus["BIRTH_DATE"] = 'errorFocus'
                errors["BIRTH_DATE"] = 'อายุผู้สมัครยังไม่ถึง 7 ปีบริบูรณ์'
            }
        }
        if(!fields["GENDER"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["GENDER"] = 'errorFocus'
            errors["GENDER"] = 'กรุณาเลือกเพศ'
        }
        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });

        return formIsValid;
      }


    render(){
        var yearnow = new Date().getFullYear()
        var start = yearnow - parseInt(100);
        var setYear = [];
        var selectTime = new Date()
        var minDate = new Date();
        minDate.setFullYear(minDate.getFullYear() - 100);
        var date_Config = dateConfigTH;
        return(
            <div>
                <div className="bg-body-regis py-3">
                    <div className="">
                        <div className="container-fluid">
                            <form onSubmit={e => this.handleSubmit(e)}>
                                <div>
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">เบอร์โทรศัพท์</span>
                                    </label>
                                    <div>
                                        <input 
                                            maxLength="10"
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['PHONE_NO']}`}
                                            onInput={this.maxLengthCheck}
                                            placeholder="กรอกเบอร์โทรศัพท์"
                                            type="number"
                                            name="PHONE_NO"
                                            value={this.state.fields['PHONE_NO']}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg PHONE_NO">{this.state.errors["PHONE_NO"]}</div>
                                </div>

                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">อีเมล</span>
                                    </label>
                                    <div>
                                        <input 
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['EMAIL']}`}
                                            placeholder="กรอกอีเมล"
                                            name="EMAIL"
                                            disabled={this.state.dp}
                                            value={(this.state.fields['EMAIL'])}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg EMAIL">{this.state.errors["EMAIL"]}</div>
                                </div>
                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">รหัสผ่าน</span>
                                    </label>
                                    <div>
                                        <input 
                                            type="password"
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['PASSWORD']}`}
                                            placeholder="กรอกรหัสผ่าน"
                                            name="PASSWORD"
                                            value={this.state.fields['PASSWORD']}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg PASSWORD">{this.state.errors["PASSWORD"]}</div>
                                </div>
                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">กรอกรหัสผ่านอีกครั้ง</span>
                                    </label>
                                    <div>
                                        <input
                                            type="password"
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['CHECKPASSWORD']}`}
                                            placeholder="กรอกรหัสผ่านอีกครั้ง"
                                            name="CHECKPASSWORD"
                                            value={this.state.fields['CHECKPASSWORD']}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg CHECKPASSWORD">{this.state.errors["CHECKPASSWORD"]}</div>
                                </div>
                                <div className="header-label">ข้อมูลผู้ใช้งาน</div>
                                <div className="label-mt">
                                    <label className="font-header-label"><span className="label-validate">*</span> ชื่อ</label>
                                    <div>
                                        <input 
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['FIRSTNAME']}`}
                                            placeholder="กรอกชื่อ"
                                            name="FIRSTNAME"
                                            value={this.state.fields['FIRSTNAME']}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg FIRSTNAME">{this.state.errors["FIRSTNAME"]}</div>
                                </div>
                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">นามสกุล</span>
                                    </label>
                                    <div>
                                        <input 
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['LASTNAME']}`}
                                            placeholder="กรอกนามสกุล"
                                            name="LASTNAME"
                                            value={this.state.fields['LASTNAME']}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg LASTNAME">{this.state.errors["LASTNAME"]}</div>
                                </div>
                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">วัน/เดือน/ปีเกิด</span>
                                    </label>
                                    <div>
                                        <input
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['BIRTH_DATE']}`}
                                            placeholder="กรุณาเลือกวัน เดือน ปีเกิด"
                                            type="date" 
                                            id="date"
                                            name="BIRTH_DATE"
                                            value={this.state.fields['BIRTH_DATE']}
                                            // selected={this.state.startDate}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg BIRTH_DATE">{this.state.errors["BIRTH_DATE"]}</div>
                                </div>
                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">เพศ</span>
                                    </label>
                                    <div>
                                        <select 
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['GENDER']}`}
                                            placeholder="เลือกเพศ"
                                            name="GENDER"
                                            value={this.state.fields['GENDER']}
                                            onChange={(e) => this.handChange(e)}
                                        >
                                            <option value="4">เลือกเพศ</option>
                                            <option value="1">ชาย</option>
                                            <option value="2">หญิง</option>
                                            <option value="3">อื่น ๆ</option>
                                        </select>
                                    </div>
                                    <div className="errorMsg GENDER">{this.state.errors["GENDER"]}</div>
                                </div>
                                <div className="btn_nextstep">
                                    <button 
                                        type="submit"
                                        className="btn btn-secondary btn-block btn-check"
                                    >
                                        สมัครสมาชิกใหม่
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

