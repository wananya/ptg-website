import React from "react";
import '../../location/location.css';
import { ShopAction } from '../../../_actions/locationAction';
// import Modal from 'react-bootstrap/Modal'

export class DetailMap extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            lang: "th",
            shopId: "",  
            lat: "",  
            lng: "", 
            GetShopById: {},
            showLoading: 'block',
            productOther: {},
            productPetrol: {},
        }; 
    }

    componentDidMount() {
        const params = this.props.match.params;
        this.setState({
            lang: params.lang,
            shopId: params.shopId,
            lat: params.lat,
            lng: params.lng,
        });
        this.handleGetShopById(params.shopId, params.lat, params.lng)
    }

    handleGetShopById(shopId, lat, lng){
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            ShopAction.GetShopById(this.state.lang, shopId, lat, lng).then(e => {
                if (e.isSuccess === true) {
                    this.setState({ GetShopById: e.data })
                    this.setState({ productPetrol: e.data.productPetrol })
                    this.setState({ productOther: e.data.productOther })
                    this.setState({ showLoading: 'none' })
                } else {
                    this.setState({ showError: true });
                }
            });
        }, 2500);
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/logo.jpg`);
    }
    
    render() {

        var lat = this.state.GetShopById['latitude'];
        var lng = this.state.GetShopById['longitude'];
        let _urllink = `https://maps.google.com/maps?q=${lat},${lng}&hl=en&z=14&amp;output=embed`
        

        let { productPetrol } = this.state
        var productPetrolCards = [];
        for (var x in productPetrol) {
            productPetrolCards.push(
                <img 
                    src={productPetrol[x].producT_IMAGE}
                    className="img-fluid pr-1"
                    onError={(e) => this.onImageError(e)}
                    width="40"
                /> 
            );
        }

        let { productOther } = this.state
        var productOtherCards = [];
        for (var x in productOther) {
            productOtherCards.push(
                <img 
                    src={productOther[x].producT_IMAGE}
                    className="img-fluid pr-1"
                    onError={(e) => this.onImageError(e)}
                    width="40"
                /> 
            );
        }

        return (
            <div> 
                <div className="loader mx-auto" style={{ display: this.state.showLoading}}></div>
                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="container">
                        <a href={"/map/home"}>
                        <div className="row arrow-back">
                            <div className="ml-auto">
                                <img 
                                    src={process.env.PUBLIC_URL +"/images/icon/arrow@3x.png"}
                                    className="img-fluid pt-2 pl-2"
                                    width="35"
                                    onError={(e) => this.onImageError(e)}
                                />
                            </div>
                        </div>
                        </a>
                    </div>
                    <div className="container-fuild">
                        <div className="row">
                            <div className="col-12">
                                <img 
                                    src={this.state.GetShopById['imG_URL_DEFAULT']}
                                    className="img-fluid w-100"
                                    onError={(e) => this.onImageError(e)}
                                />
                            </div>
                        </div>
                        <div className="row img-logodetail">
                            <div className="col-12 text-center">
                                <img 
                                    src={this.state.GetShopById['imG_URL_NORMAL']}
                                    className="img-fluid mx-auto"
                                    width="150"
                                    onError={(e) => this.onImageError(e)}
                                />
                            </div>
                        </div>
                        <div className="row p-2">
                            <div className="col-3 text-right ml-auto">
                                <a 
                                    className="btn btn-success"
                                    href={_urllink}
                                >
                                    นำทาง
                                </a>
                            </div>
                        </div>
                        <div className="row px-3 pt-5">
                            <div className="col-12">
                               <div className="t-bold t-22"> {this.state.GetShopById['placename']} </div>
                            </div>
                        </div>
                        <div className="row px-3 pt-2">
                            <div className="col-12">
                               <div className="t-bold t-20"> ที่ตั้ง </div>
                               <div className="t-height-20 t-18"> {this.state.GetShopById['address']} </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="row px-3 pt-1">
                            <div className="col-12">
                               <div className="t-bold t-20"> บริการ </div>
                               <div className="py-1">
                                    {productPetrolCards}
                               </div>
                               <div className="py-1">
                                    {productOtherCards}
                               </div>
                            </div>
                        </div>
                        <hr/>
                    </div>
                </div>
            </div>
        );
    }
}
    