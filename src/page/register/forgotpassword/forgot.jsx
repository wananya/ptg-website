import React from 'react';
import '../../register/register.css';
import { registerAction } from '../../../_actions/registerActions';
import moment from "moment-timezone";
import SweetAlert from 'react-bootstrap-sweetalert';

export class forgot extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            errors: {},
            errorsFocus:{},
            fields:{
                VALUE:'',
                BIRTH_DATE:''
            },
            show: false, 
            modal: null
        };
    }

    modalAlert(msg,img){
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={img}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(false)}
              onConfirm={() => this.handleChoice(true)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(){
        this.setState({ modal : null , show : false })
    }

    handelSubmit(value,birthday){
        let { fields } = this.state;
        var setType = 0;
        var cvFormat = moment(birthday).format('YYYY-MM-DD');
        console.log(cvFormat)
        if(this.validateForm()){
            var chkType = value.search('@');
            if(chkType === -1){
                setType = 1
            }
            let formData = new FormData();
            formData.append('type' , setType)
            formData.append('value' , value)
            formData.append('birthDay' ,cvFormat)
            registerAction.chkBeforePassword(formData).then(e => {
                console.log(e)
                if(e.data.isSuccess === true){
                    if(setType === 1){
                        this.props.history.push({
                            pathname: `${process.env.PUBLIC_URL}/forgotpassword/otp`,
                            state: {
                                data: value,
                                id : e.data.data.id,
                                userId: e.data.data.userId,
                                token:e.data.data.token
                            }
                        });
                    }else{
                        this.props.history.push({
                            pathname: `${process.env.PUBLIC_URL}/forgotpassword/changpassword`,
                            state: {
                                id : e.data.data.id,
                                userId: e.data.data.userId,
                                token:e.data.data.token
                            }
                        });
                    }

                    // this.setState({ })
                }else{
                    var msgEn = e.data.errMsg_en;
                    var msgTH = e.data.errMsg;
                    var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                    this.modalAlert(msgTH,imgPopup);
                }
            })
        }else{
            console.log('formsubmit ' + false);
        }
        this.setState({ fields })
    }

    async validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(!fields["VALUE"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["VALUE"] = 'errorFocus'
            errors["VALUE"] = 'กรุณากรอกเบอร์โทรหรืออีเมล'
        }
        if(!fields["BIRTH_DATE"]){
            console.log(false)
            formIsValid = false;
            errorsFocus["BIRTH_DATE"] = 'errorFocus'
            errors["BIRTH_DATE"] = 'กรุณาเลือกวัน/เดือน/ปีเกิด'
        }

        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });

        return formIsValid;
    }

    handleChange(e){
        console.log(e.target.name)
        let { errors,errorsFocus, fields } = this.state;
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
        this.setState({ errors, fields , errorsFocus });
    }

    render(){
        return(
            <div className="container-fluid my-5">
                <form>
                    <div className="label-mt">
                        <label>
                            <span className="label-validate">*</span>
                            <span className="font-header-label pl-2">เบอร์โทรศัพท์ / อีเมล</span>
                        </label>
                        <div>
                            <input
                                className={`inputWidth form-control input_form ${this.state.errorsFocus['VALUE']}`}
                                name="VALUE"
                                placeholder="กรุณากรอกเบอร์โทรหรืออีเมล"
                                onChange={(e) => this.handleChange(e)}
                            />
                        </div>
                    </div>
                    <div className="errorMsg VALUE">{this.state.errors["VALUE"]}</div>
                    <div className="label-mt">
                        <label>
                            <span className="label-validate">*</span>
                            <span className="font-header-label pl-2">วัน เดือน ปีเกิด</span>
                        </label>
                        <div>
                            <input
                                className={`inputWidth form-control input_form ${this.state.errorsFocus['BIRTH_DATE']}`}
                                placeholder="กรุณาเลือกวัน เดือน ปีเกิด"
                                type="date" 
                                id="date"
                                name="BIRTH_DATE"
                                onChange={(e) => this.handleChange(e)}
                            />
                        </div>
                    </div>
                    <div className="errorMsg BIRTH_DATE">{this.state.errors["BIRTH_DATE"]}</div>
                    <div  className="btn_nextstep">
                        <button 
                            type="button"
                            className="btn btn-secondary btn-block btn-check"
                            onClick={() => this.handelSubmit(this.state.fields['VALUE'] , this.state.fields['BIRTH_DATE'])}
                        >
                            ดำเนินการต่อ
                        </button>
                    </div>
                </form>
                {this.state.modal}
            </div>
        )
    }
}

