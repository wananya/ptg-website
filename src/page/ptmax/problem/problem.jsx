import React from 'react';
import '../../ptmax/ptmax.css';
import EXIF from "exif-js";
import Img from "react-fix-image-orientation";

var alert;

var disabledimage = true
var disabledimage2 = true

export class Problem extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            file: "",
            imagePreviewUrl: "",
            nexbtn: "button_finish",
            disableAgreement: true,
            disabledBtn: "",
            resultApi: "",
            errorFileSize: "",
            closeBtn: {
              imgPreview1: `none`,
              imgPreview2: `none`,
              imgPreview3: `none`
            },
            classPreview: {
              imgPreview1: `preview-image`,
              imgPreview2: `preview-image`,
              imgPreview3: `preview-image`
            },
            imgPreview: {
              imgPreview1: `${process.env.PUBLIC_URL}/images/mockup.png`,
              imgPreview2: `${process.env.PUBLIC_URL}/images/mockup.png`,
              imgPreview3: `${process.env.PUBLIC_URL}/images/mockup.png`
            },
            fields: {
              CUSTOMER_IMG_INFO: []
            },
            renderPopup: "",
            show: false,
            modal: null
        };
    }
    componentDidMount(){
        this.closeNav();
    }

    handleLoadAvatar(e) {
        let { closeBtn, imgPreview, classPreview, fields } = this.state;
        this.setState({ target_name : e.target.name })
        var system = this;
        EXIF.getData(e.target.files[0], function() {
          var orientation = EXIF.getTag(this, "Orientation");
          var can = document.createElement("canvas");
          var ctx = can.getContext("2d");
    
          var thisImage = new Image();
          thisImage.onload = function() {
            var MAX_WIDTH = 700;
            var MAX_HEIGHT = 700;
            var width = thisImage.width;
            var height = thisImage.height;
    
            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
              }
            }
            can.width = width;
            can.height = height;
            // can.width  = thisImage.width;
            // can.height = thisImage.height;
            ctx.save();
            var width = can.width;
            var styleWidth = can.style.width;
            var height = can.height;
            var styleHeight = can.style.height;
            if (orientation) {
              if (orientation > 4) {
                can.width = height;
                can.style.width = styleHeight;
                can.height = width;
                can.style.height = styleWidth;
              }
              switch (orientation) {
                case 2:
                  ctx.translate(width, 0);
                  // ctx.scale(-1, 1);
                  break;
                case 3:
                  ctx.translate(width, height);
                  ctx.rotate(Math.PI);
                  break;
                case 4:
                  ctx.translate(0, height);
                  // ctx.scale(1, -1);
                  break;
                case 5:
                  ctx.rotate(0.5 * Math.PI);
                  // ctx.scale(1, -1);
                  break;
                case 6:
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(0, -height);
                  break;
                case 7:
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(width, -height);
                  // ctx.scale(-1, 1);
                  break;
                case 8:
                  ctx.rotate(-0.5 * Math.PI);
                  ctx.translate(-width, 0);
                  break;
              }
            }
    
            ctx.drawImage(thisImage, 0, 0, width, height);
            ctx.restore();
            var dataURL = can.toDataURL();
            var dataurlLink = can.toDataURL("image/jpeg");
            console.log(dataurlLink)
            // var get64 = dataURL
            var get64 = dataurlLink.substr(23)
            // var get64 = dataurlLink
            localStorage.setItem(system.state.target_name,get64)
            // imgPreview[system.state.target_name] = dataURL
            imgPreview[system.state.target_name] = dataurlLink
            classPreview[system.state.target_name] = ''
            var obj = {
              CUSTOMER_IMG: get64
            }
            fields['CUSTOMER_IMG_INFO'].push(obj)
            system.setState({ imgPreview, classPreview })
    
            closeBtn[system.state.target_name] = 'block'
            system.setState({ closeBtn })
            // img.src = e.target.result;
    
    
            imgPreview[system.state.target_name] = dataURL
            // imgPreview[system.state.target_name] = dataurlLink;
            classPreview[system.state.target_name] = ''
            // at this point you can save the image away to your back-end using 'dataURL'
            system.setState({ imgPreview, classPreview });
          };
    
          thisImage.src = URL.createObjectURL(this);
        });
    }
    
    checkImg() {
        console.log(this.state.imgBase);
    }
    
    handleUploadFile = (e) => {
        console.log(e)
        if(e === 1) { 
          // this.inputElement_1.value = "";
          // this.inputElement_1.click();
          disabledimage = false;
        }
        if(e === 2) { 
          // this.inputElement_2.value = "";
          // this.inputElement_2.click(); 
          disabledimage2 = false;
        }
        if(e === 3) { 
          // this.inputElement_3.value = "";
          // this.inputElement_3.click(); 
        }
    }
    
    deleteLocalStorage(target) {
        if(target == 'imgPreview1'){
          disabledimage = true;
          disabledimage2 = true;
        }else if(target == 'imgPreview2'){
          disabledimage2 = true;
        }
        let { closeBtn, imgPreview, classPreview } = this.state;
        localStorage.removeItem(target);
        closeBtn[target] = "none";
        classPreview[target] = "preview-image";
        imgPreview[target] = `${process.env.PUBLIC_URL}/images/mockup.png`;
        this.setState({ closeBtn });
    }
    
    handleChange(e) {
        this.setState({ TITLE: e.target.value });
    }
    
    changeHandle(event) {
        this.setState({ DESCRIPTION: event.target.value });
    }

    openNav() {
        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("main").style.marginLeft = "300px";
      }
      
    closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
      }

    render(){
        return(
            <div>
                <div className="bg-ptmax">
                    <div id="mySidenav" className="sidenav">
                        <div className="font-user-profile">
                            <img className="icon-width" src={`${process.env.PUBLIC_URL}/images/iconlogin-01.png`} />&nbsp;&nbsp;
                            Ptmaxpro09
                        </div>
                        <button onClick={() => this.closeNav()}>
                            <div className="closebtn">&times;</div>
                        </button>
                        <div className="block-navbar">
                            <a href={`${process.env.PUBLIC_URL}/ptmaxcard`}>บัตรพีที แมกซ์ ของฉัน</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">แก้ไขข้อมูลส่วนตัว</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">ประวัติการใช้งาน</a>
                        </div>
                        <div className="block-navbar">
                            <a href={`${process.env.PUBLIC_URL}/maxcard/report`}>แจ้งปัญหาการใช้งาน</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">เงื่อนไขและการบริการ</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">คำถามที่พบบ่อย</a>
                        </div>
                        <div className="block-navbar">
                            <a className="signout-font" href="#">ออกจากระบบ</a>
                        </div>
                    </div>
                    <div id="main">
                        <div className="headet-ptmaxcard">แจ้งปัญหาการใช้งาน</div>
                        <span className="span-heading" style={{cursor:'pointer'}} onClick={() => this.openNav()}><i className="fas fa-chevron-left"></i>&nbsp;&nbsp; บัตรพีที แมกซ์ ของฉัน</span>
                        <div className="box-content">
                            <div className="form-group">
                                <select
                                    className="form-control"
                                    id="exampleFormControlSelect1"
                                    name="SELECT_CASE"
                                    onChange={e => this.handleChange(e)}
                                >
                                    <option value={0}>กรุณาเลือก</option>
                                    <option value={0}>กรุณาเลือก</option>
                                    <option value={0}>กรุณาเลือก</option>
                                    <option value={0}>กรุณาเลือก</option>
                                </select>
                            </div>
                            <div className="form-group">
                                <textarea
                                    className="form-control input_form"
                                    id="exampleFormControlTextarea1"
                                    rows="4"
                                    name="DESCRIPTION"
                                >

                                </textarea>
                            </div>
                            <div className="errorImageSize">{this.state.errorFileSize}</div>
                            <div className="">
                                {/* <form onSubmit={this._handleSubmit}> */}
                                <div className="row row_image">
                                    <div className="col-4 col-sm-3 review_image_before">
                                    <div className="upload_preview">
                                        <div
                                        className="close-btn"
                                        onClick={() => this.deleteLocalStorage("imgPreview1")}
                                        style={{
                                            display: this.state.closeBtn["imgPreview1"]
                                        }}
                                        >
                                        <i
                                            className="fa fa-times-circle"
                                            aria-hidden="true"
                                        />
                                        </div>
                                        <div onClick={() => this.handleUploadFile(1)} >
                                        <label className="uploadfile">
                                        <input
                                            type="file"
                                            name="imgPreview1"
                                            style={{ display: "none" }}
                                            // ref={input => (this.inputElement_1 = input)}
                                            onChange={e => this.handleLoadAvatar(e)}
                                        />
                                        </label>
                                        <Img
                                            src={this.state.imgPreview["imgPreview1"]}
                                            alt=""
                                            className={this.state.classPreview["imgPreview1"]}
                                        />
                                        </div>
                                    </div>
                                    </div>
                                    <div className="col-4 col-sm-3 review_image_before">
                                    <div className="upload_preview">
                                        <div
                                        className="close-btn"
                                        onClick={() => this.deleteLocalStorage("imgPreview2")}
                                        style={{
                                            display: this.state.closeBtn["imgPreview2"]
                                        }}
                                        >
                                        <i
                                            className="fa fa-times-circle"
                                            aria-hidden="true"
                                        />
                                        </div>
                                        <div onClick={() => this.handleUploadFile(2)} >
                                        {/* <input type="file" onChange={this._handleImageChange} style={{ display: "none" }} /> */}
                                        <label className="uploadfile">
                                        <input
                                            type="file"
                                            name="imgPreview2"
                                            style={{ display: "none" }}
                                            // ref={input => (this.inputElement_2 = input)}
                                            onChange={e => this.handleLoadAvatar(e)}
                                            disabled={disabledimage}
                                        />
                                        </label>
                                        <Img
                                            src={this.state.imgPreview["imgPreview2"]}
                                            alt=""
                                            className={this.state.classPreview["imgPreview2"]}
                                        />
                                        </div>
                                    </div>
                                    </div>
                                    <div className="col-4 col-sm-3 review_image_before">
                                    <div className="upload_preview">
                                        <div
                                        className="close-btn"
                                        onClick={() => this.deleteLocalStorage("imgPreview3")}
                                        style={{
                                            display: this.state.closeBtn["imgPreview3"]
                                        }}
                                        >
                                        <i
                                            className="fa fa-times-circle"
                                            aria-hidden="true"
                                        />
                                        </div>
                                        <div onClick={() => this.handleUploadFile(3)}>
                                        <label className="uploadfile">
                                        <input
                                            type="file"
                                            name="imgPreview3"
                                            style={{ display: "none" }}
                                            // ref={input => (this.inputElement_3 = input)}
                                            onChange={e => this.handleLoadAvatar(e)}
                                            disabled={disabledimage2}
                                        />
                                        <Img
                                            src={this.state.imgPreview["imgPreview3"]}
                                            alt=""
                                            className={this.state.classPreview["imgPreview3"]}
                                        />
                                        </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                {/* </form> */}
                            </div>
                            <div className="btn-sentdata">
                                <button className="text-btn">ส่งข้อมูล</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


