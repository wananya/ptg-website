import React from "react";
import '../Header/header.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import { registerAction } from '../../_actions/registerActions'; 
import firebase from 'firebase';

var config = {
    apiKey: "AIzaSyAS7sELcK0DVddlXy2LQFkFp5PwLfmMmhU",
    authDomain: "ptg-application.firebaseapp.com",
    databaseURL: "https://ptg-application.firebaseio.com",
    projectId: "ptg-application",
    storageBucket: "ptg-application.appspot.com",
    messagingSenderId: "691162632611",
    appId: "1:691162632611:web:c11fce40b6bf58150eca16"
}

firebase.initializeApp(config);
const auth = firebase.auth
const provider = new firebase.auth.FacebookAuthProvider();
const providergm = new firebase.auth.GoogleAuthProvider();
provider.addScope('email');
provider.addScope('user_friends');
providergm.addScope('https://www.googleapis.com/auth/contacts.readonly');
firebase.auth().languageCode = 'en';
providergm.setCustomParameters({
    'login_hint': 'user@example.com'
});

const publicIp = require('public-ip');

export default class Header extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            show:false,
            modal:null,
            errors: {},
            errorsFocus:{},
            fields:{},
            user:null,
            userid:''
        };
    }
    componentDidMount(){
        // (async () => {
        //     this.setState({ ipaddress : await publicIp.v4() })
        // })();
    }
    logingm = () => {
        var that = this;
        firebase.auth().signInWithPopup(providergm).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
            that.setState({user: user});
            that.loginSocialGm(user)
          }).catch(function(error) {
            console.log(error)
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
          });
    }
    loginSocialGm = (data) => {
        var phone_no = '';
        if(data.phoneNumber !== null){
            phone_no = data.phoneNumber;
        }
        let formData = new FormData();
        formData.append('socialId' , data.uid)
        formData.append('socialType' , 'gmail')
        formData.append('email',data.email)
        formData.append('phone',phone_no)
        formData.append('udId',this.state.ipaddress)
        formData.append('tokenId','')
        formData.append('playerId','WEBSITE')

        registerAction.loginSocial(formData).then(e => {
            console.log(e)
            if(e.data.errCode === 10){
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/register/web`,
                    state: {
                      socialid : data.uid,
                      socialType: 'gmail',
                      udId:this.state.ipaddress,
                      playerId : 'WEBSITE',
                      email:data.email,
                      phone:phone_no,
                    }
                  });
            }else if(e.data.isSuccess === true){
                localStorage.setItem("user", e.data.data.firstName)
                localStorage.setItem("login", "true")
                localStorage.setItem('uid',e.data.data.userId)
                localStorage.setItem('phone' , e.data.data.phoneNo)
                localStorage.setItem('tkmb' , e.data.data.tokenMobileApp)
                localStorage.setItem("lastname", e.data.data.lastName)
                localStorage.setItem('email',e.data.data.email)
                localStorage.setItem('birthdate',e.data.data.birthDay)
                this.setState({ 
                    uid : e.data.data.userId,
                    phone : e.data.data.phoneNo,
                    email : e.data.data.email,
                    ptCardNo : e.data.data.ptCardNo,
                    firstName : e.data.data.firstName,
                    lastName : e.data.data.lastName,
                    birthDay : e.data.data.birthDay,
                    gender : e.data.data.gender,
                    facebookId : e.data.data.facebookId,
                    gmailId:e.data.data.gmailId,
                    idCard:e.data.data.idCard,
                    pinCode:e.data.data.pinCode,
                    isActive:e.data.data.isActive,
                    tokeN_ID:e.data.data.tokeN_ID,
                    customeR_ID:e.data.data.customeR_ID,
                    citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                    citizeN_ID:e.data.data.citizeN_ID,
                    titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                    titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                    fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                    customeR_STATUS:e.data.data.customeR_STATUS,
                    isMemberPTG:e.data.data.isMemberPTG,
                    tokenMobileApp:e.data.data.tokenMobileApp,
                    show: false, 
                    modal: null 
                })
            }
        })
    }
    login = () => {
        var that = this;
        firebase.auth().signInWithPopup(provider).then(function(result) {
          var user = result.user;
          that.setState({user: user});
          that.loginSocialFb(user);
        }).catch(function(error) {
            console.log(error)
          var errorCode = error.code;
          var errorMessage = error.message;
        });
    }
    setupLogout(){
        localStorage.clear();
        window.location.href=`${process.env.PUBLIC_URL}/home`
        var that = this;
        auth().signOut().then(function() {
          that.setState({user: null});
        }).catch(function(error) {
            console.log(error)
        });
    }
    loginSocialFb = (data) => {
            var phone_no = '';
            if(data.phoneNumber !== null){
                phone_no = data.phoneNumber;
            }
            let formData = new FormData();
            formData.append('socialId' , data.uid)
            formData.append('socialType' , 'facebook')
            formData.append('email',data.email)
            formData.append('phone',phone_no)
            formData.append('udId',this.state.ipaddress)
            formData.append('tokenId','')
            formData.append('playerId','WEBSITE')
            registerAction.loginSocial(formData).then(e => {
                console.log(e)
                if(e.data.errCode === 10){
                    this.props.history.push({
                        pathname: `${process.env.PUBLIC_URL}/register/web`,
                        state: {
                          socialid : data.uid,
                          socialType: 'facebook',
                          udId:this.state.ipaddress,
                          playerId : 'WEBSITE',
                          email:data.email,
                          phone:phone_no,
                        }
                      });
                }else if(e.data.isSuccess === true){
                    console.log(e)
                    localStorage.setItem("user", e.data.data.firstName)
                    localStorage.setItem("login", "true")
                    localStorage.setItem('uid',e.data.data.userId)
                    localStorage.setItem('phone' , e.data.data.phoneNo)
                    localStorage.setItem('tkmb' , e.data.data.tokenMobileApp)
                    this.setState({ 
                        uid : e.data.data.userId,
                        phone : e.data.data.phoneNo,
                        email : e.data.data.email,
                        ptCardNo : e.data.data.ptCardNo,
                        firstName : e.data.data.firstName,
                        lastName : e.data.data.lastName,
                        birthDay : e.data.data.birthDay,
                        gender : e.data.data.gender,
                        facebookId : e.data.data.facebookId,
                        gmailId:e.data.data.gmailId,
                        idCard:e.data.data.idCard,
                        pinCode:e.data.data.pinCode,
                        isActive:e.data.data.isActive,
                        tokeN_ID:e.data.data.tokeN_ID,
                        customeR_ID:e.data.data.customeR_ID,
                        citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                        citizeN_ID:e.data.data.citizeN_ID,
                        titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                        titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                        fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                        customeR_STATUS:e.data.data.customeR_STATUS,
                        isMemberPTG:e.data.data.isMemberPTG,
                        tokenMobileApp:e.data.data.tokenMobileApp,
                        show: false, 
                        modal: null 
                    })
                }
            })
    }
    handleChange(e){
        let { fields , errors , errorsFocus } = this.state;
        if(e.target.name === 'USERNAME'){
            fields['USERNAME'] = e.target.value;
        }else if(e.target.name === 'PASSWORD'){
            fields['PASSWORD'] = e.target.value;
        }else{
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
        }

        this.setState({ 
            errorsFocus,
            errors, 
            fields
        })
    }
    submitForm(e){
        e.preventDefault();
        let { fields} = this.state; 
        fields[e.target.name] = e.target.value;
        if(this.validateForm()){
            (async () => {
                var ipaddress = await publicIp.v4();
                let formData = new FormData();
                formData.append('userName' , fields['USERNAME']);
                formData.append('password' , fields['PASSWORD']);
                formData.append('udId',ipaddress);
                formData.append('tokenId','');
                formData.append('playerId','WEBSITE');
                registerAction.loginWebsite(formData).then( e => {
                    if(e.data.isSuccess === true){
                        localStorage.setItem("user", e.data.data.firstName)
                        localStorage.setItem("login", "true")
                        localStorage.setItem('uid',e.data.data.userId)
                        localStorage.setItem('phone' , e.data.data.phoneNo)
                        localStorage.setItem('tkmb' , e.data.data.tokenMobileApp)
                        localStorage.setItem('cardno' , e.data.data.ptCardNo)
                        localStorage.setItem("lastname", e.data.data.lastName)
                        localStorage.setItem('email',e.data.data.email)
                        localStorage.setItem('birthdate',e.data.data.birthDay)
                        this.setState({ 
                            uid : e.data.data.userId,
                            phone : e.data.data.phoneNo,
                            email : e.data.data.email,
                            ptCardNo : e.data.data.ptCardNo,
                            firstName : e.data.data.firstName,
                            lastName : e.data.data.lastName,
                            birthDay : e.data.data.birthDay,
                            gender : e.data.data.gender,
                            facebookId : e.data.data.facebookId,
                            gmailId:e.data.data.gmailId,
                            idCard:e.data.data.idCard,
                            pinCode:e.data.data.pinCode,
                            isActive:e.data.data.isActive,
                            tokeN_ID:e.data.data.tokeN_ID,
                            customeR_ID:e.data.data.customeR_ID,
                            citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                            citizeN_ID:e.data.data.citizeN_ID,
                            titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                            titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                            fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                            customeR_STATUS:e.data.data.customeR_STATUS,
                            isMemberPTG:e.data.data.isMemberPTG,
                            tokenMobileApp:e.data.data.tokenMobileApp,
                            show: false, 
                            modal: null 
                        })
                    }else{
                        var msg = e.data.errMsg;
                        var img = `${process.env.PUBLIC_URL}/images/cancel.png`;
                        this.modalCheckSubmit(msg,img);
                    }
                })
            })();
        }else{
            this.showModalLogin('errorFocus','กรุณากรอกข้อมูล')
        }
    }
    validateForm(){
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(!fields['USERNAME']){
            formIsValid = false;
            errors["USERNAME"] = 'กรุณากรอกข้อมูล'
            errorsFocus["USERNAME"] = 'errorFocus'
        }
        if(!fields['PASSWORD']){
            formIsValid = false;
            errors["PASSWORD"] = 'กรุณากรอกข้อมูล'
            errorsFocus["PASSWORD"] = 'errorFocus'
        }

        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });
      
        return formIsValid;
    }
    modalCheckSubmit(res, img) {
    alert = (
      <SweetAlert
        custom
        showCloseButton
        closeOnClickOutside={false}
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
      >
        <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
        <div className="fontSizeCase">{res}</div>
      </SweetAlert>
    );
    this.setState({ show: true, modal: alert });
    }

  handleChoice(choice) {
    if (choice === false) {
      if(this.state.disableAgreement == true){
        this.setState({ show: false , modal: null })
      }else{
        this.setState({ show: false , modal: null })
      }
    }
  }

    showModalLogin(errorsFocus,errors){
        alert = (
            <SweetAlert
                custom
                closeOnClickOutside = {true}
                showConfirm = {false}
                customClass='setpositionmodal'
            >
                <div className="text-right" onClick={(e) => this.handleChoice(e)}><i className="fas fa-times icon-setfontsize"></i></div>
                <div className="row">
                    <div className="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                        <img style={{width:'100%'}} src={`${process.env.PUBLIC_URL}/images/Untitled-1-02.jpg`} />
                    </div>
                    <div className="col-sm-6 col-xs-6 col-md-6 col-lg-6 mt-2 col-6-lineh">
                    <form onSubmit={(e) => this.submitForm(e)}>
                        <div className="header-sigin">ลงชื่อเข้าใช้</div>
                        <div className="my-3 label-header">
                            <label className="label-text-pc">เบอร์โทรศัพท์ / อีเมล</label>
                            <input 
                                type="text"
                                className={`form-control-set ${errorsFocus}`}
                                name="USERNAME"
                                onChange={(e) => this.handleChange(e)}
                            />
                        </div>
                        <div className="errorMsg USERNAME">{errors}</div>
                        <div className="my-3 label-header">
                            <label className="label-text-pc">รหัสผ่าน</label>
                            <input 
                                type="password"
                                className={`form-control-set ${errorsFocus}`}
                                name="PASSWORD"
                                onChange={(e) => this.handleChange(e)}
                            />
                        </div>
                        <div className="errorMsg PASSWORD">{errors}</div>
                        <a onClick={() => window.location.href = `${process.env.PUBLIC_URL}/forgotpassword`}>
                            <div className="label-forgot">ลืมรหัสผ่าน ?</div>
                        </a>
                        <button className="loginandregis-btn" type="sumbit">
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบ</div>
                        </button>
                    </form>
                        <button className="loginregis-fb-btn" onClick={() => this.login()}>
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบด้วย Facebook</div>
                        </button>
                        <button className="loginregis-gm-btn" onClick={() => this.logingm()}>
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบด้วย Gmail</div>
                        </button>
                        <button className="loginandregis-btn mt-3" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/register`}>
                            <div style={{marginTop: '5px'}}>สมัครสมาชิกใหม่</div>
                        </button>
                    </div>
                </div>
            </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    handleChoice(){
        this.setState({ modal : null , show : false })
    }

    showBtn(showBtn){
        if(showBtn === 'none'){
            this.setState({ showDropDown : 'block' })
        }else{
            this.setState({ showDropDown : 'none' })
        }
    }

    render(){ 
        console.log(this.state.uid)
        return (
            <div>
                <div className="container-center">
                <img style={{width:'100%'}} src={`${process.env.PUBLIC_URL}/images/PT_WEBSITE-01-HomePage-07.jpg`} />
                <img className="centered logo-header" src={`${process.env.PUBLIC_URL}/images/Logo_web.png`} />
                {/* <nav className="navbar navbar-expand-lg mobile-pt navbarsetting">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <img className="mobile-pt" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" src={`${process.env.PUBLIC_URL}/images/menu-01@3x.png`}/>
                    </button>
                    <div className="collapse navbar-collapse bg-navigation-mobile setting-navpos" id="navbarNavDropdown">
                        <ul className="navbar-nav">
                        <li className="nav-item active">
                            <a className="nav-link font-nav-mobile border-nav-mobile" href={`${process.env.PUBLIC_URL}/home`}>หน้าหลัก <span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link font-nav-mobile border-nav-mobile" href={`${process.env.PUBLIC_URL}/PTmaxcard`}>บัตรพีที แมกซ์</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link  font-nav-mobile border-nav-mobile" href={`${process.env.PUBLIC_URL}/privileges`}>สิทธิพิเศษและของรางวัล</a>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle font-nav-mobile border-nav-mobile" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ข่าวสารและกิจกรรม
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <div className="arrow-down"></div>
                            <a className="dropdown-item border-dropdown-it" href="#">ข่าวสาร</a>
                            <a className="dropdown-item" href="#">โปรโมชั่น</a>
                            </div>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link font-nav-mobile" href={`${process.env.PUBLIC_URL}/location`}>ค้นหาสถานีบริการ</a>
                        </li>
                        </ul>
                        <div className="bd-radius">
                            สมัครบัตร
                        </div>
                    </div>
                </nav> */}
                <div onClick={() => this.showBtn(this.state.showDropDown)}><img className="mobile-pt logo-header settitlemb" src={`${process.env.PUBLIC_URL}/images/login.png`}/></div>
                {/* <div style={{display:this.state.showDropDown}} className="mobile-pt setnavmb">
                    <div className="arrow-up"></div>
                    <button>
                        <div className="signinmb">ลงชื่อเข้าใช้</div>
                    </button>
                    <div className="regismb">สมัครบัตร</div>
                </div> */}
                <div className="div-navbarout">
                    <div className="div-navbarin">
                        <div className="nav-inline"><button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/home`} className="btn-custom">หน้าหลัก</button></div>
                        <div className="nav-inline">
                            {(localStorage.getItem('cardno') === "null" ?
                            <button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/register`} className="btn-custom">
                                <div>บัตรพีทีแมกซ์</div>
                            </button> :
                            <button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard`} className="btn-custom">
                                บัตรพีทีแมกซ์
                            </button>
                            )}
                        </div>
                        <div className="nav-inline"><button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/privileges`} className="btn-custom">สิทธิพิเศษและของรางวัล</button></div>
                        <div className="nav-inline"><button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/news`} className="btn-custom">ข่าวสารกิจกรรม</button></div>
                        <div className="nav-inline"><button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/location`} className="btn-custom">ค้นหาสถานีบริการ</button></div>
                        <div className="nav-inline">
                            {(localStorage.getItem('uid') === 'login' ?                             
                            <button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/register`} className="btn-custom border-signin">
                                <div>สมัครบัตร</div>
                            </button>
                            : 
                            <button onClick={() => this.showModalLogin()} className="btn-custom border-signin">
                                <div>สมัครบัตร</div>
                            </button>)}
                        </div>
                        {localStorage.getItem('login') === "true" ?  
                            <div className="dropdown nav-inline">
                                <button className="btn-custom btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img className="icon-login-true pr-1" src={`${process.env.PUBLIC_URL}/images/Pericon.png`} />
                                    {localStorage.getItem('user')}
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a 
                                        className="dropdown-item"
                                        href={`${process.env.PUBLIC_URL}/editprofile`}
                                    >
                                        แก้ไขข้อมูลส่วนตัว</a>
                                    <a 
                                        className="dropdown-item" 
                                        href={`${process.env.PUBLIC_URL}/newpassword`}
                                    >
                                            แก้ไขรหัสผ่าน</a>
                                    <a 
                                        className="dropdown-item" 
                                        onClick={() => this.setupLogout()}
                                    >
                                        ออกจากระบบ
                                    </a>
                                </div>
                            </div> : 
                            <div className="nav-inline">
                                <button 
                                    className="btn-custom" 
                                    type="button"
                                    onClick={() => this.showModalLogin()}
                                >
                                    <img className="icon-login" src={`${process.env.PUBLIC_URL}/images/login.png`} />
                                    ลงชื่อเข้าใช้
                                </button>
                            </div>}
                </div>
            </div>
        </div>
        {this.state.modal}
    </div>
  )}
}
