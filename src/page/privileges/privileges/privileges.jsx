import React from 'react';
import '../../privileges/privileges.css';

export class Privileges extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active:''
        };
        this.handleHover = this.handleHover.bind(this);
    }
    componentDidMount(){

    }

    addActiveClass(e){
        const clicked = e.target.id
        if(this.state.active === clicked) { 
            this.setState({active: ''});
        } else {
            this.setState({active: clicked})
       }
    }

    handleHover(){
        this.setState(prevState => ({
            isHovered: !prevState.isHovered
        }));
    }

    render(){
        console.log(this.state.isHovered)
        const btnClass = this.state.isHovered ? "block" : "none";
        return(
            <div>
                <div className="pc-pt container-fluid bg-web-pri">
                    <div className="header-privilege">สิทธิพิเศษและของรางวัล</div>
                    <div className="row set-title-pr">
                        <div className="set-content-header">
                            <ul className="nav nav-tabs">
                                <li className="nav-item nav-mt">
                                    <a className="nav-link active" data-toggle="tab" href="#home">ทั้งหมด</a>
                                </li>
                                <li className="nav-item nav-mt">
                                    <a className="nav-link" data-toggle="tab" href="#menu1">ของรางวัล PT</a>
                                </li>
                                <li className="nav-item nav-mt">
                                    <a className="nav-link" data-toggle="tab" href="#menu2">ประกัน&ท่องเที่ยว</a>
                                </li>
                                <li className="nav-item nav-mt">
                                    <a className="nav-link" data-toggle="tab" href="#menu2">ไลฟ์สไตล์&เอนเตอร์เทนเมนต์</a>
                                </li>
                                <li className="nav-item nav-mt">
                                    <a className="nav-link" data-toggle="tab" href="#menu2">อาหาร&เครื่องดื่ม</a>
                                </li>
                            </ul>
                        </div>

                        <div className="row set-mt-content">
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri" onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-pri card-img-top"  id="first" onClick={(e) => this.addActiveClass(e)} alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTbsc360x360px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            {/* <div style={this.state.active === 'first' ? {display:'block'} : {display:'none'}} className="setbtnReward"><a href="#" className="btn btn-primary">แลก</a></div> */}
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-pri card-img-top"  id="first" onClick={(e) => this.addActiveClass(e)} alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTMajor360x360(2)px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            {/* <div style={this.state.active === 'first' ? {display:'block'} : {display:'none'}} className="setbtnReward"><a href="#" className="btn btn-primary">แลก</a></div> */}
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTEE-ED 730x400& 360x360px-02.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            <div style={{display:btnClass}} className="setbtnReward"><a href="#" className="btn btn-primary">แลก</a></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTbsc360x360px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            <div style={{display:btnClass}} className="setbtnReward"><a href="#" className="btn btn-primary">แลก</a></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div className="row set-mt-content">
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri" onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTbsc360x360px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            <div style={{display:btnClass}} className="setbtnReward"><a href="#" className="btn btn-primary">แลก</a></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTMajor360x360(2)px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            <div style={{display:btnClass}} className="setbtnReward"><a href="#" className="btn btn-primary">แลก</a></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTEE-ED 730x400& 360x360px-02.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            <div style={{display:btnClass}} className="setbtnReward"><a href="#" className="btn btn-primary">แลก</a></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTbsc360x360px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            <div style={{display:btnClass}} className="setbtnReward"><a href="#" className="btn btn-primary">แลก</a></div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div className="mobile-pt container-fluid bg-web-pri">
                    <div className="header-privilege">สิทธิพิเศษและของรางวัล</div>
                    <div className="row set-title-pr">
                        {/* <div className="set-content-header tex-center">
                            <ul className="nav nav-pills np-overflow d-flex flex-nowrap">
                                <li className="li-overflow pr-2">
                                    <a className="nav-link active a-overflow" data-toggle="pill" href="#all">ทั้งหมด</a>
                                </li>
                                <li className="li-overflow pr-2">
                                    <a className="nav-link a-overflow" data-toggle="pill" href="#all">ของรางวัล PT</a>
                                </li>
                                <li className="li-overflow pr-2">
                                    <a className="nav-link a-overflow" data-toggle="pill" href="#all">ประกัน&ท่องเที่ยว</a>
                                </li>
                                <li className="li-overflow pr-2">
                                    <a className="nav-link a-overflow" data-toggle="pill" href="#all">ไลฟ์สไตล์&เอนเตอร์เทนเมนต์</a>
                                </li>
                                <li className="li-overflow pr-2">
                                    <a className="nav-link a-overflow" data-toggle="pill" href="#all">อาหาร&เครื่องดื่ม</a>
                                </li>
                            </ul>
                        </div> */}
                            {/* <div className="col-xs-2 col-md-2 col-sm-2 col-2">ทั้งหมด</div>
                            <div className="col-xs-2 col-md-2 col-sm-2 col-2">ของรางวัล PT</div>
                            <div className="col-xs-2 col-md-2 col-sm-2 col-2">ประกัน&ท่องเที่ยว</div>
                            <div className="col-xs-6 col-md-3 col-sm-3 col-lg-3">ไลฟ์สไตล์&เอนเตอร์เทนเมนต์</div>
                            <div className="col-xs-2 col-md-2 col-sm-2 col-2">อาหาร&เครื่องดื่ม</div> */}

                        <div className="row set-mt-content">
                                <div className="card-pri col-6 col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                    <button className="btn-pri">
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTbsc360x360px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            {/* <a href="#" className="btn btn-primary">Go somewhere</a> */}
                                        </div>
                                    </button>
                                </div>
                                <div className="card-pri col-6 col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                    <button className="btn-pri">
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTbsc360x360px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            {/* <a href="#" className="btn btn-primary">Go somewhere</a> */}
                                        </div>
                                    </button>
                                </div>
                        </div>

                        <div className="row set-mt-content">
                                <div className="card-pri col-6 col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                    <button className="btn-pri">
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTbsc360x360px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            {/* <a href="#" className="btn btn-primary">Go somewhere</a> */}
                                        </div>
                                    </button>
                                </div>
                                <div className="card-pri col-6 col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                    <button className="btn-pri">
                                        <img className="img-pri card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/TC-PTbsc360x360px-01.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">Pure Care By BSC Mask หน้าฟรี 1 ครั้ง</span>
                                            {/* <a href="#" className="btn btn-primary">Go somewhere</a> */}
                                        </div>
                                    </button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

