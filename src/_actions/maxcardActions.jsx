import axios from 'axios';
import { Base_API } from '../../src/_constants/matcher';

export const maxcardActions = {
    getCardList
};

function getCardList(userid){
    return axios.get(`${Base_API.apis}/PTMaxCard/CardList?userId=${userid}`).then(res => {
        return res;
    })
}