import React from 'react';
import '../../register/register.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import { registerAction } from '../../../_actions/registerActions'; 

export class changepass extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            errors: {},
            errorsFocus:{},
            fields:{
                VALUE:'',
                BIRTH_DATE:''
            },
            show: false, 
            modal: null
        };
    }

    modalAlert(msg,img){
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={img}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(false)}
              onConfirm={() => this.handleChoice(true)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(){
        this.setState({ modal : null , show : false })
    }

    handelSubmit(passfirst,passtwo){
        let { fields } = this.state;
        if(this.validateForm()){
            let formData = new FormData();
            formData.append('userId' , this.props.location.state.userId)
            formData.append('token' , this.props.location.state.token)
            formData.append('password' , passfirst)
            registerAction.forgotPassword(formData).then(e => {
                console.log(e)
                if(e.data.isSuccess === true){
                    window.location.href = `${process.env.PUBLIC_URL}/home`
                }else{
                    var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                    var msg = e.data.errMsg
                    this.modalAlert(msg,imgPopup)
                }
            })
        }else{
            console.log('formsubmit ' + false);
        }
        this.setState({ fields })
    }

    async validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(!fields["PASSONE"]){
            formIsValid = false;
            errorsFocus["PASSONE"] = 'errorFocus'
            errors["PASSONE"] = 'กรุณากรอกรหัสผ่าน'
        }
        if(!fields["PASSTWO"]){
            formIsValid = false;
            errorsFocus["PASSTWO"] = 'errorFocus'
            errors["PASSTWO"] = 'กรุณากรอกรหัสผ่านอีกครั้ง'
        }
        if(fields["PASSONE"] !== fields["PASSTWO"]){
            formIsValid = false;
            errorsFocus["PASSTWO"] = 'errorFocus'
            errors["PASSTWO"] = 'รหัสผ่านไม่ตรงกัน'
        }

        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });

        return formIsValid;
    }

    handleChange(e){
        let { errors,errorsFocus, fields } = this.state;
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
        this.setState({ errors, fields , errorsFocus });
    }

    render(){
        return(
            <div className="container-fluid my-5">
                <form>
                    <div className="label-mt">
                        <label>
                            <span className="label-validate">*</span>
                            <span className="font-header-label pl-2">รหัสผ่าน</span>
                        </label>
                        <div>
                            <input
                                className={`inputWidth form-control input_form ${this.state.errorsFocus['PASSONE']}`}
                                name="PASSONE"
                                type="password"
                                placeholder="กรุณากรอกรหัสผ่าน"
                                onChange={(e) => this.handleChange(e)}
                            />
                        </div>
                    </div>
                    <div className="errorMsg PASSONE">{this.state.errors["PASSONE"]}</div>
                    <div className="label-mt">
                        <label>
                            <span className="label-validate">*</span>
                            <span className="font-header-label pl-2">กรอกรหัสผ่านอีกครั้ง</span>
                        </label>
                        <div>
                            <input
                                className={`inputWidth form-control input_form ${this.state.errorsFocus['PASSTWO']}`}
                                placeholder="กรุณากรอกรหัสผ่านอีกครั้ง"
                                type="password"
                                name="PASSTWO"
                                onChange={(e) => this.handleChange(e)}
                            />
                        </div>
                    </div>
                    <div className="errorMsg PASSTWO">{this.state.errors["PASSTWO"]}</div>
                    <div  className="btn_nextstep">
                        <button 
                            type="button"
                            className="btn btn-secondary btn-block btn-check"
                            onClick={() => this.handelSubmit(this.state.fields['PASSONE'] , this.state.fields['PASSTWO'])}
                        >
                            ดำเนินการต่อ
                        </button>
                    </div>
                </form>
                {this.state.modal}
            </div>
        )
    }
}

