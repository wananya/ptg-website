import React from 'react';
import '../../ptmax/ptmax.css';
import { registerAction } from '../../../_actions/registerActions';
import moment from "moment-timezone";
import 'moment/locale/th';
import DatePicker from 'react-datepicker';
import DatePickers from 'react-date-picker';
import "react-datepicker/dist/react-datepicker.css";
import Img from "react-fix-image-orientation";
import EXIF from "exif-js"
import SweetAlert from "react-bootstrap-sweetalert";

sessionStorage.setItem('inputBody','{}')

let SessionData = JSON.parse(sessionStorage.getItem('inputBody'))
var genderClass = "form-control disabled";

let alert
var disabledimage = true
var disabledimage2 = true

let amphureListData = [];
let tumbonListData = [];
let postcodeListData = [];
let postcodeListDataNew = [];

var ERROR_USER_EMAIL = false;

var disabled_block = 'form-control input_form disabled';
var entertain_remark_display = 'form-control input_form disabled';
var sport_remark_display = 'form-control input_form disabled';
var outside_remark_display = 'form-control input_form disabled';
var inside_remark_display = 'form-control input_form disabled';

const monthMap = {
    '1': '01',
    '2': '02',
    '3': '03',
    '4': '04',
    '5': '05',
    '6': '06',
    '7': '07',
    '8': '08',
    '9': '09',
    '10': '10',
    '11': '11',
    '12': '12',
};
  
const dateConfig = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: 'YYYY'
    },
}
  
const dateConfigTH = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: value => moment(value).add(543,'y').format('YYYY')
    },
}


export class registemaxcard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            date : new Date(),
            isForeigner: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID != null && JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? "block" : "none" ),
            inputcard: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID != null && JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? false : true ),
            errors: {},
            errorsFocus: {
              CARD_ID: "",
              CARD_TYPE_ID: "",
              TITLE_NAME_ID: "",
              GENDER_ID: "",
              PHONE_NO: "",
              MARITAL_STATUS: "",
              FNAME_TH:"",
              LNAME_TH:""
            },
            TITLE_NAME_ID: 1,
            TITLE_NAME_REMARK: "none",
            GENDER_ID:1,
            file: '',
            agreement: false,
            imagePreviewUrl: '',
            nexbtn: 'button_finish',
            disableAgreement: false,
            disabledBtn: '',
            resultApi: '',
            errorFileSize: '',
            closeBtn: {
                imgPreview1: `none`,
                imgPreview2: `none`,
                imgPreview3: `none`,
            },
            classPreview: {
                imgPreview1: `preview-image`,
                imgPreview2: `preview-image`,
                imgPreview3: `preview-image`,
            },
            imgPreview: {
                imgPreview1: `${process.env.PUBLIC_URL}/images/mockup.png`,
                imgPreview2: `${process.env.PUBLIC_URL}/images/mockup.png`,
                imgPreview3: `${process.env.PUBLIC_URL}/images/mockup.png`,
            },
            fields: {
                CUSTOMER_IMG_INFO: [],
                CARD_ID_ENG: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_ID_ENG != null ? JSON.parse(sessionStorage.getItem('inputBody')).CARD_ID_ENG : "" ),
                TITLE_NAME_REMARK: (JSON.parse(sessionStorage.getItem('inputBody')).TITLE_NAME_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).TITLE_NAME_REMARK : "" ),
                FNAME_TH: (JSON.parse(sessionStorage.getItem('inputBody')).FNAME_TH != null ? JSON.parse(sessionStorage.getItem('inputBody')).FNAME_TH : sessionStorage.getItem("_firstname") ),
                LNAME_TH: (JSON.parse(sessionStorage.getItem('inputBody')).LNAME_TH != null ? JSON.parse(sessionStorage.getItem('inputBody')).LNAME_TH : sessionStorage.getItem("_lastname") ),
                BIRTH_DATE: (JSON.parse(sessionStorage.getItem('inputBody')).BIRTH_DATE != null ? JSON.parse(sessionStorage.getItem('inputBody')).BIRTH_DATE : sessionStorage.getItem("_birthdate") ),
                CARD_ID: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).CARD_ID : "" ),
                CARD_TYPE_ID: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID : 1 ),
                TITLE_NAME_ID: (JSON.parse(sessionStorage.getItem('inputBody')).TITLE_NAME_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).TITLE_NAME_ID : 0 ),
                GENDER_ID: (JSON.parse(sessionStorage.getItem('inputBody')).GENDER_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).GENDER_ID : 0 ),
                PHONE_NO: (JSON.parse(sessionStorage.getItem('inputBody')).PHONE_NO != null ? JSON.parse(sessionStorage.getItem('inputBody')).PHONE_NO : sessionStorage.getItem("_phid") ),
                USER_EMAIL: (JSON.parse(sessionStorage.getItem('inputBody')).USER_EMAIL != null ? JSON.parse(sessionStorage.getItem('inputBody')).USER_EMAIL : sessionStorage.getItem("_emid") ),
                MARITAL_STATUS: (JSON.parse(sessionStorage.getItem('inputBody')).MARITAL_STATUS != null ? JSON.parse(sessionStorage.getItem('inputBody')).MARITAL_STATUS : 0 ),
            },
            show: false,
            modal: null,
            activeColor2: "fade",
            activeColor3: "fade",
            activeColor1: "active",
            active1:"active",
            active2:"",
            active3:"",
            additionnal:'none',
            submitFormNoadditional:'block',
            OCCAPATION_REMARK:'none',
            CAR_BRAND_REMARK:'none',
            disabled: true,
            disabled_entertain:true,
            disabled_sport: true,
            disabled_outside: true,
            disabled_inside: true,
            disabledBtn: '',
            showLoading: 'block',
            content_page: 'none',
            chkDisplayAdd:'block'
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
      let { fields ,TITLE_NAME_REMARK , checkType , CAR_BRAND_REMARK, OCCAPATION_REMARK } = this.state;
      const params = this.props.match.params;

      this.getDropdownlist_type("TITLE").then(result => {
        if(result === undefined){
          var showLoading = '';
          var content_page = '';
            showLoading = 'block';
            content_page = 'none';
          var th = this;
          setTimeout(function(){
              th.setState({ showLoading : showLoading})
              th.setState({ content_page: content_page })
          },1000)
        }
        else if(result.status === 200){
          this.setState({ name_titles: result.data.dropdowN_INFO });
          var showLoading = '';
          var content_page = '';
            showLoading = 'none';
            content_page = 'block';
          var th = this;
          setTimeout(function(){
              th.setState({ showLoading : showLoading})
              th.setState({ content_page: content_page })
          },1000)
        }
      });
      this.getDropdownlist_type("GENDER").then(result => {
        if(result === undefined){
          var showLoading = '';
          var content_page = '';
            showLoading = 'block';
            content_page = 'none';
          var th = this;
          setTimeout(function(){
              th.setState({ showLoading : showLoading})
              th.setState({ content_page: content_page })
          },1000)
        }
        else if(result.status === 200){
          this.setState({ gender: result.data.dropdowN_INFO });
          var showLoading = '';
          var content_page = '';
            showLoading = 'none';
            content_page = 'block';
          var th = this;
          setTimeout(function(){
              th.setState({ showLoading : showLoading})
              th.setState({ content_page: content_page })
          },1000)
        }
      });
      this.getDropdownlist_type("MARITAL_STATUS").then(result => {
        if(result === undefined){
          var showLoading = '';
          var content_page = '';
            showLoading = 'block';
            content_page = 'none';
          var th = this;
          setTimeout(function(){
              th.setState({ showLoading : showLoading})
              th.setState({ content_page: content_page })
          },1000)
        }
        else if(result.status === 200){
          this.setState({ marital_status: result.data.dropdowN_INFO });
          var showLoading = '';
          var content_page = '';
            showLoading = 'none';
            content_page = 'block';
          var th = this;
          setTimeout(function(){
              th.setState({ showLoading : showLoading})
              th.setState({ content_page: content_page })
          },1000)
        }
      });
      this.getProvinceNewApi().then(result => { 
        if(result === undefined){
          var showLoading = '';
          var content_page = '';
            showLoading = 'block';
            content_page = 'none';
          var th = this;
          setTimeout(function(){
              th.setState({ showLoading : showLoading})
              th.setState({ content_page: content_page })
          },1000)
        }
        else if(result.status === 200){
          this.setState({ province: result.data.dropdowN_INFO });
          var showLoading = '';
          var content_page = '';
            showLoading = 'none';
            content_page = 'block';
          var th = this;
          setTimeout(function(){
              th.setState({ showLoading : showLoading})
              th.setState({ content_page: content_page })
          },1000)
        }
      })
      this.getAmphure(this.state.fields['ADDR_ORG_PROVINCE_ID'])

      this.getDropdownlist_type("OCCUPATION").then(result => {this.setState({ occupation: result.data.dropdowN_INFO })})
      this.getDropdownlist_type("EDUCATION").then(result => {this.setState({ education: result.data.dropdowN_INFO })})
      this.getDropdownlist_type("INCOME").then(result => {this.setState({ income: result.data.dropdowN_INFO })})
      this.getDropdownlist_type("CAR_TYPE").then(result => {this.setState({ cartype: result.data.dropdowN_INFO })})
      this.getDropdownlist_type("CAR_BRAND").then(result => {this.setState({ car_brand: result.data.dropdowN_INFO })})
          
      fields['OCCUPATION_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).OCCUPATION_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).OCCUPATION_ID : 0)
      fields['CAR_TYPE'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_TYPE != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_TYPE : 0)
      fields['CAR_BRAND'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND : 0)
      fields['EDUCATION_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).EDUCATION_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).EDUCATION_ID : 0)
      fields['INCOME_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).INCOME_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).INCOME_ID : 0)
    
      if(SessionData != null) {
        let newObject = Object.assign(SessionData , this.state.fields)
        fields = newObject
        if (fields.TITLE_NAME_ID === 4) {
          TITLE_NAME_REMARK = "block";
        }
        if (fields.CAR_BRAND === 9) {
          CAR_BRAND_REMARK = "block";
        }
        if (fields.OCCUPATION_ID === 5) {
          OCCAPATION_REMARK = "block";
        }
        this.setState({ fields, TITLE_NAME_REMARK , checkType ,CAR_BRAND_REMARK, OCCAPATION_REMARK})
      }
    }

    renderAdditional(type) {
        const { occupation, education, income, cartype, car_brand} = this.state;
        var option = []
        var name_title;
        if (type === 'type_cartype') {
          name_title = cartype 
        } else if (type === 'type_brand') {
            name_title = car_brand 
        } else if (type === 'type_occupation') {
              name_title = occupation 
        } else if (type === 'type_education'){
          name_title = education 
        } else {
          name_title = income 
        }
        for(var i in name_title) {
          option.push(<option key={i} value={name_title[i].code}>{name_title[i].values}</option>)
        }
        return option
    }

    dropdownlistCheck(e) {
      let { errors, errorsFocus ,fields, CAR_BRAND_REMARK, OCCAPATION_REMARK } = this.state;
      if(e.target.name === "CAR_BRAND") {
        if(e.target.value === '9'){
          CAR_BRAND_REMARK = 'block'
        } else {
          CAR_BRAND_REMARK = 'none'
          fields["CAR_BRAND_REMARK"] = '';
          errors["CAR_BRAND_REMARK"]  = null;
          errorsFocus["CAR_BRAND_REMARK"]  = ''
        }
        errors[e.target.name] = null;
        fields[e.target.name] = parseInt(e.target.value)
        errorsFocus[e.target.name] = ''
      }else if(e.target.name === "OCCUPATION_ID") {
        if(e.target.value === '5'){
          OCCAPATION_REMARK = 'block'
        } else {
          OCCAPATION_REMARK = 'none'
          fields["OCCAPATION_REMARK"] = '';
          errors["OCCAPATION_REMARK"]  = null;
          errorsFocus["OCCAPATION_REMARK"]  = ''
        }
        errors[e.target.name] = null;
        fields[e.target.name] = parseInt(e.target.value)
        errorsFocus[e.target.name] = ''
      } else {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      }
      this.setState({ errors, errorsFocus, fields, CAR_BRAND_REMARK, OCCAPATION_REMARK})
    }
  
    checboxClick(ev) {
      let { fields } = this.state;
      if(ev.target.name === "news") {
        fields[ev.target.id] = ev.target.checked
        if (ev.target.id === "SUBSCRIBE_NONE") {
          if(ev.target.checked === true) {
            fields['SUBSCRIBE_EMAIL'] = false
            fields['SUBSCRIBE_LETTER'] = false
            fields['SUBSCRIBE_SMS'] = false
          }
        } else {
          fields['SUBSCRIBE_NONE'] = false
        }
      }else if(ev.target.name === "activities"){
        fields[ev.target.id] = ev.target.checked
        if (ev.target.id === "ACT_HNGOUT_NO") {
          if(ev.target.checked === true) {
            fields['ACT_HNGOUT_FRIEND'] = false
            fields['ACT_HNGOUT_COUPLE'] = false
            fields['ACT_HNGOUT_FAMILY'] = false
          }
        } else {
          fields['ACT_HNGOUT_NO'] = false
        }
      }else {
        fields[ev.target.id] = ev.target.value
      }
      this.setState({ fields })
    }

    handleGameClick(ev){
      let { fields } = this.state;
      if(ev.target.name === "entertain") {
        if (ev.target.id === "ACT_EN_OTHERS") {
          if(ev.target.checked === true) {
            entertain_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            entertain_remark_display = 'form-control input_form disabled'
            fields['ACT_EN_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if(fields['ACT_EN_OTHERS'] === true) {
            entertain_remark_display = 'form-control input_form'
          } else {
            entertain_remark_display = 'form-control input_form disabled'
            fields['ACT_EN_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }
        this.setState( {disabled_entertain: (ev.target.checked === "" ? false : true)} )
      } else if(ev.target.name === "sport") {
        if (ev.target.id === "ACT_SPT_OTHERS") {
          if(ev.target.checked === true) {
            sport_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            sport_remark_display = 'form-control input_form disabled'
            fields['ACT_SPT_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if (fields['ACT_SPT_OTHERS'] === true) {
            sport_remark_display = 'form-control input_form'
          } else {
            sport_remark_display = 'form-control input_form disabled'
            fields['ACT_SPT_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }
        this.setState( {disabled_sport: (ev.target.checked === "" ? false : true)} )
      } else if(ev.target.name === "outside") {
        if (ev.target.id === "ACT_OD_OTHERS") {
          if(ev.target.checked === true) {
            outside_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            outside_remark_display = 'form-control input_form disabled'
            fields['ACT_OD_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if (fields['ACT_OD_OTHERS'] === true){
            outside_remark_display = 'form-control input_form'
          } else {
            outside_remark_display = 'form-control input_form disabled'
            fields['ACT_OD_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }  
        this.setState( {disabled_outside: (ev.target.checked === "" ? false : true)} )
      } else if(ev.target.name === "inside") {
        if (ev.target.id === "ACT_ID_OTHERS") {
          if(ev.target.checked === true) {
            inside_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            inside_remark_display = 'form-control input_form disabled'
            fields['ACT_ID_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if ( fields['ACT_ID_OTHERS'] === true) {
            inside_remark_display = 'form-control input_form'
          } else {
            inside_remark_display = 'form-control input_form disabled'
            fields['ACT_ID_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }  
        this.setState( {disabled_inside: (ev.target.checked === "" ? false : true)} )
      } else if(ev.target.name === "inside") { 
        fields['ACT_HNGOUT_FRIEND'] = false
        fields['ACT_HNGOUT_COUPLE'] = false
        fields['ACT_HNGOUT_FAMILY'] = false
        fields['ACT_HNGOUT_NO'] = false
        fields[ev.target.id] = ev.target.checked
      } else {
        fields[ev.target.id] = ev.target.value
      }
      this.setState({ fields })
    }

    handleGameClik(ev) {
      let { fields } = this.state;
      if(ev.target.name === "promotion") {
        if (ev.target.id === "PMT_OTHERS") {
          if(ev.target.checked === true) {
            disabled_block = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            disabled_block = 'form-control input_form disabled'
            fields['PMT_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if(fields['PMT_OTHERS'] === true) {
            disabled_block = 'form-control input_form'
          } else {
            disabled_block = 'form-control input_form disabled'
            fields['PMT_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }
        this.setState( {disabled: (ev.target.checked === "" ? false : true)} )
      } else {
        fields[ev.target.id] = ev.target.value
      }
        this.setState({ fields })
    } 

    getProvinceNewApi(){
        return registerAction.getProvinceNew()
    }
    
    getDropdownlist_type(title) {
      return registerAction.getDropdownlist(title);
    }
    
    getAmphure(val, type) {
        let { fields } = this.state;
        if(type) {
          fields['ADDR_ORG_AMPHURE_ID'] = ''
          fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
          fields['ADDR_ORG_POSTALCODE_ID'] = ''
        }
        this.setState({ amphure: '' })
        var value = val;
        fields['ADDR_ORG_PROVINCE_ID'] = value
        this.setState({ fields })
        registerAction.getAmphure(value).then(e => {
          this.setState({
            amphure: e.data.data
          })
          this.getTumbon(this.state.fields['ADDR_ORG_AMPHURE_ID'])
        })
    }
    
    getTumbon(val, type) {
        let { fields } = this.state;
        if(type) {
          fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
          fields['ADDR_ORG_POSTALCODE_ID'] = ''
        }
        fields['ADDR_ORG_AMPHURE_ID'] = val
        this.setState({ fields })
        var tumbon = val
        registerAction.getTumbon(tumbon).then(e => {
          this.setState({
            tumbon: e.data.data
          })
          this.getPostcode(this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'])
        })
    }
    
    getPostcode(val, type) {
        let { fields } = this.state;
        if(type) {
          fields['ADDR_ORG_POSTALCODE_ID'] = ''
        }
        fields['ADDR_ORG_SUB_DISTRICT_ID'] = val
        this.setState({ fields })
        var postcode = val
        registerAction.getPostcode(postcode).then(e => {
          if(e.data.data.length === 1){
              fields['ADDR_ORG_POSTALCODE_ID'] = e.data.data[0].codE_GUID
              this.setState({ fields })
          }
            this.setState({
              postcode: e.data.data,
            })
        })
    }


    changeHandle(e) {
        var { errorsFocus,errors, fields , additionnal , submitFormNoadditional } = this.state;
        var checkedForeign = (e.target.checked === true) ? 2 : 1
        fields["CARD_TYPE_ID"] = checkedForeign
        if(checkedForeign === 1) {
          errorsFocus['CARD_ID_ENG'] = ''
          errors['CARD_ID_ENG'] = ''
          fields['CARD_ID_ENG'] = '';
        } else {
          errorsFocus['CARD_ID'] = ''
          errors['CARD_ID'] = ''
          fields['CARD_ID'] = '';
        }

        let { nexbtn } = this.state
        if(e.target.checked === true) {
          nexbtn = 'button_nextstep'
          additionnal = 'block';
          submitFormNoadditional = 'none';
        } else {
          nexbtn = 'button_finish'
          additionnal = 'none';
          submitFormNoadditional = 'block';
        }
        this.setState({ 
          nexbtn , 
          additionnal , 
          submitFormNoadditional , 
          agreement: e.target.checked , 
          errorsFocus, 
          errors, 
          fields,
          isForeigner: e.target.checked === true ? "block" : "none", 
          inputcard: e.target.checked === true ? false : true
        })
    }

    renderContent(type) {
        const { name_titles, gender, marital_status } = this.state;
        var option = [];
        var name_title;
        if (type === "type_title_name") {
          name_title = name_titles;
        } else if (type === "type_gender") {
          name_title = gender;
        } else {
          name_title = marital_status;
        }
        for (var i in name_title) {
          option.push(
            <option key={i} value={name_title[i].code}>
              {name_title[i].values}
            </option>
          );
        }
        return option;
    }

    checkImg() {
        // console.log(this.state.imgBase);
    }
    
    handleUploadFile = (e) => {
        if(e === 1) { 
          this.inputElement_1.value = "";
          this.inputElement_1.click(); 
          disabledimage = false;
        }
        if(e === 2) { 
          this.inputElement_2.value = "";
          this.inputElement_2.click(); 
          disabledimage2 = false;
        }
        if(e === 3) { 
          this.inputElement_3.value = "";
          this.inputElement_3.click(); 
        }
    }
    
    deleteLocalStorage(target) {
        if(target == 'imgPreview1'){
          disabledimage = true;
          disabledimage2 = true;
        }else if(target == 'imgPreview2'){
          disabledimage2 = true;
        }
        let { closeBtn, imgPreview, classPreview } = this.state;
        localStorage.removeItem(target)
        closeBtn[target] = 'none'
        classPreview[target] = 'preview-image'
        imgPreview[target] = `${process.env.PUBLIC_URL}/images/mockup.png`
        this.setState({ closeBtn })
    }

    handleLoadAvatar(e) {
        let { closeBtn, imgPreview, classPreview, fields } = this.state;
        this.setState({ target_name : e.target.name })
        var system = this;
        EXIF.getData(e.target.files[0], function() {
          var orientation = EXIF.getTag(this, "Orientation");
          var can = document.createElement("canvas");
          var ctx = can.getContext("2d");
    
          var thisImage = new Image();
          thisImage.onload = function() {
            var MAX_WIDTH = 700;
            var MAX_HEIGHT = 700;
            var width = thisImage.width;
            var height = thisImage.height;
    
            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
              }
            }
            can.width = width;
            can.height = height;
            // can.width  = thisImage.width; 
            // can.height = thisImage.height;
            ctx.save();
            var width = can.width;
            var styleWidth = can.style.width;
            var height = can.height;
            var styleHeight = can.style.height;
            if (orientation) {
              if (orientation > 4) {
                can.width = height;
                can.style.width = styleHeight;
                can.height = width;
                can.style.height = styleWidth;
              }
              switch (orientation) {
                case 2:
                  ctx.translate(width, 0);
                  // ctx.scale(-1, 1);
                  break;
                case 3:
                  ctx.translate(width, height);
                  ctx.rotate(Math.PI);
                  break;
                case 4:
                  ctx.translate(0, height);
                  // ctx.scale(1, -1);
                  break;
                case 5:
                  ctx.rotate(0.5 * Math.PI);
                  // ctx.scale(1, -1);
                  break;
                case 6:
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(0, -height);
                  break;
                case 7:
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(width, -height);
                  // ctx.scale(-1, 1);
                  break;
                case 8:
                  ctx.rotate(-0.5 * Math.PI);
                  ctx.translate(-width, 0);
                  break;
              }
            }
    
            ctx.drawImage(thisImage, 0, 0, width, height);
            ctx.restore();
            var dataURL = can.toDataURL();
            var dataurlLink = can.toDataURL("image/jpeg");
            // var get64 = dataURL;
            var get64 = dataurlLink.substr(23)
            localStorage.setItem(system.state.target_name,get64)
            imgPreview[system.state.target_name] = dataURL
            classPreview[system.state.target_name] = ''
            var obj = {
              CUSTOMER_IMG: get64
            }
            fields['CUSTOMER_IMG_INFO'].push(obj)
            system.setState({ imgPreview, classPreview })
    
            closeBtn[system.state.target_name] = 'block'
            system.setState({ closeBtn })
            // img.src = e.target.result;
    
    
            imgPreview[system.state.target_name] = dataURL;
            classPreview[system.state.target_name] = ''
            // at this point you can save the image away to your back-end using 'dataURL'
            system.setState({ imgPreview, classPreview });
          };
    
          thisImage.src = URL.createObjectURL(this);
        });
    }

    renderContentProvince(type) {
        const { province } = this.state;
        var option = []
        var name_title;
        if (type === 'type_province') {
          name_title = province
        }
    
        for (var i in name_title) {
          option.push(<option key={i} value={name_title[i].codE_GUID}>{name_title[i].values}</option>)
        }
    
        return <select
          className={`form-control input_form ${this.state.errorsFocus['ADDR_ORG_PROVINCE_ID']}`}
          id='exampleFormControlSelect1'
          name="ADDR_ORG_PROVINCE_ID"
          value={this.state.fields['ADDR_ORG_PROVINCE_ID']}
          onChange={(e) => this.getAmphure(e.target.value, 'onchange')}
        >
          <option name="ADDR_ORG_PROVINCE_ID">กรุณาเลือกจังหวัด</option>
          {option}
        </select>
    }

    async handleSubmit(event) {
        var { fields } = this.state;
        event.preventDefault();
        if (await this.validateForm()) {
          if(fields['CARD_TYPE_ID'] === 2) {
            fields['CARD_ID'] = fields['CARD_ID_ENG']
            this.setState({ fields })
          }
          sessionStorage.setItem('inputBody', JSON.stringify(this.state.fields))  
        } else {
          console.log('formsubmit ' + false);
        }
    }

    checkAddress(e){
      if((e.target.value.match(/-/g) || []).length > 1){
        return false
      }else{
        return true
      }
    }
  
    checkCharacter(e){
      var check = /^\d*[a-zA-Zก-ฮ][a-zA-Zก-ฮ\d\s]{1,10}$/;
      if(check.test(e.target.value) != false){
        return true
      }else{
        return false
      }
    }
    
    async validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;
        // var firstChar = fields.ADDR_NO.charAt(0)
        var check = /^[a-zA-Zก-ฮ\b]+$/;
        var getTrue = false;
        var getSymbol = false;
        var getReplaceChar = false;
        var getReplace = /^[^0][0-9]{1,3}(\/[^0][0-9]{1,3})(\-[^0][0-9]{1,3})?$/;
        var getcheckreplace = /^[0-9]{1,5}(\-[0-9]{1,5})?$/;
        var getChecklastChar = /^[0-9]{1,4}(\/[0-9]{1,4})([a-zA-Zก-ฮ]{1,})?$/;
        var getcheck = false;

        var birthdate = moment(this.state.date).format('YYYY-MM-DD');
        fields['BIRTH_DATE'] = birthdate;

        if (!fields["ADDR_NO"]) {
          formIsValid = false;
          errors["ADDR_NO"] = 'กรุณากรอกบ้านเลขที่'
          errorsFocus["ADDR_NO"] = 'errorFocus'
        }else{
          for(var i in fields.ADDR_NO){
            if(check.test(fields.ADDR_NO[i]) == true){
              getTrue = true
              if(getChecklastChar.test(fields.ADDR_NO) == false){
                getcheck = true
              }
            }else{
              getTrue = false
            }
            if(fields.ADDR_NO[i] == '-'){
              getSymbol = true
              if(getReplace.test(fields.ADDR_NO) == false ){
                getReplaceChar = true
              }
              if(getcheckreplace.test(fields.ADDR_NO) == true){
                getReplaceChar = false
              }
            }
          }
          if(getcheck == true){
            formIsValid = false;
            errors["ADDR_NO"] = 'รูปแบบบ้านเลขที่ไม่ถูกต้อง'
            errorsFocus["ADDR_NO"] = 'errorFocus'
          } 
          if(getReplaceChar == true){
            formIsValid = false;
            errors["ADDR_NO"] = 'รูปแบบบ้านเลขที่ไม่ถูกต้อง'
            errorsFocus["ADDR_NO"] = 'errorFocus'
          }
          if(getSymbol == true && getTrue == true){
            formIsValid = false;
            errors["ADDR_NO"] = 'ไม่สามารถมีทั้งตัวอักษรและอักขระ '-' อยู่ในบ้านเลขที่ได้'
            errorsFocus["ADDR_NO"] = 'errorFocus'
          }
          // if(firstChar == 0){
          //   formIsValid = false;
          //   errors["ADDR_NO"] = 'ไม่สามารถเริ่มต้นด้วย 0'
          //   errorsFocus["ADDR_NO"] = 'errorFocus'
          // }
          var count = (fields.ADDR_NO.match(/[a-zA-Zก-ฮ]/g) || []).length;
          if(count > 1){
            formIsValid = false;
            errors["ADDR_NO"] = 'มีตัวอักษร ประกอบได้แค่ 1 ตัวอักษรและต้องเป็นตัวสุดท้ายเท่านั้น'
            errorsFocus["ADDR_NO"] = 'errorFocus'
          }
        }
    
        if (!fields["ADDR_ORG_PROVINCE_ID"]) {
          formIsValid = false;
          errors["ADDR_ORG_PROVINCE_ID"] = 'กรุณาเลือกจังหวัด'
          errorsFocus["ADDR_ORG_PROVINCE_ID"] = 'errorFocus'
        }
    
        if (!fields["ADDR_ORG_AMPHURE_ID"]) {
          formIsValid = false;
          errors["ADDR_ORG_AMPHURE_ID"] = 'กรุณาเลือกอำเภอ'
          errorsFocus["ADDR_ORG_AMPHURE_ID"] = 'errorFocus'
        }
    
        if (!fields["ADDR_ORG_SUB_DISTRICT_ID"]) {
          formIsValid = false;
          errors["ADDR_ORG_SUB_DISTRICT_ID"] = 'กรุณาเลือกตำบล'
          errorsFocus["ADDR_ORG_SUB_DISTRICT_ID"] = 'errorFocus'
        }
    
        if (!fields["ADDR_ORG_POSTALCODE_ID"] || fields["ADDR_ORG_POSTALCODE_ID"] == '') {
          formIsValid = false;
          errors["ADDR_ORG_POSTALCODE_ID"] = 'กรุณาเลือกรหัสไปรษณีย์'
          errorsFocus["ADDR_ORG_POSTALCODE_ID"] = 'errorFocus'
        }
    
        if(this.state.fields.CARD_TYPE_ID === 2){
          if(!fields["ADDR_OTHER"]){
            formIsValid = false;
            errors["ADDR_OTHER"] = 'กรุณากรอกข้อมูลที่อยู่ (เพิ่มเติม)'
            errorsFocus["ALERT_ADDR_OTHER"] = 'errorFocus'
          }
        }
        
        if(fields['CARD_TYPE_ID'] === 1) {
          if (!fields["CARD_ID"]) {
              formIsValid = false;
              errorsFocus["CARD_ID"] = 'errorFocus'
              errors["CARD_ID"] = 'กรุณาระบุเลขที่บัตรประชาชน'
          } else {
              var chkValidCard = await this.checkCard(this.state.fields["CARD_TYPE_ID"],this.state.fields["CARD_ID"]);
              if (chkValidCard.status === true) {
                formIsValid = false;
                errorsFocus["CARD_ID"] = 'errorFocus'
                errors["CARD_ID"] = (chkValidCard.msg === "" ? 'เลขที่บัตรประชาชนนี้ มีการลงทะเบียนเรียบร้อยแล้ว' : chkValidCard.msg)
              }
          }
        } else {
          if (!fields["CARD_ID_ENG"]) {
              formIsValid = false;
              errorsFocus["CARD_ID_ENG"] = 'errorFocus'
              errors["CARD_ID_ENG"] = 'กรุณาระบุเลขที่บัตรประชาชน'
          }else{
            var chkValidCard = await this.checkCard(this.state.fields["CARD_TYPE_ID"],this.state.fields["CARD_ID_ENG"]);
            if(chkValidCard.status === true){
              formIsValid = false;
              errorsFocus["CARD_ID_ENG"] = 'errorFocus'
              errors["CARD_ID_ENG"] = (chkValidCard.msg === "" ? 'เลขที่หนังสือเดินทางนี้ มีการลงทะเบียนเรียบร้อยแล้ว' : chkValidCard.msg)
            }
          }
        }
    
        if (!fields["TITLE_NAME_ID"]) {
          formIsValid = false;
          errorsFocus["TITLE_NAME_ID"] = 'errorFocus'
          errors["TITLE_NAME_ID"] = 'กรุณาระบุคำนำหน้าชื่อ';
        }
        if(fields["TITLE_NAME_ID"] === 4) {
          if (!fields["TITLE_NAME_REMARK"]) {
            formIsValid = false;
            errorsFocus["TITLE_NAME_REMARK"] = 'errorFocus'
            errors["TITLE_NAME_REMARK"] = (
              'กรุณาระบุคำนำหน้าชื่อ'
            );
          }
        }
        if (!fields["FNAME_TH"]) {
          formIsValid = false;
          errorsFocus["FNAME_TH"] = 'errorFocus'
          errors["FNAME_TH"] = 'กรุณากรอกชื่อจริง';
        }
        if (!fields["LNAME_TH"]) {
          formIsValid = false;
          errorsFocus["LNAME_TH"] = 'errorFocus'
          errors["LNAME_TH"] = 'กรุณากรอกนามสกุล';
        }
        if (!fields["GENDER_ID"]) {
          formIsValid = false;
          errorsFocus["GENDER_ID"] = 'errorFocus'
          errors["GENDER_ID"] = 'กรุณาเลือกเพศ';
        } else {
          if (fields["GENDER_ID"] === 4) {
            if (fields["GENDER_ID"]) {
              formIsValid = false;
              errorsFocus["GENDER_ID"] = 'errorFocus'
              errors["GENDER_ID"] = (
                'กรุณาเลือกเพศ'
              );
            }
          }
        }
        if (!fields["BIRTH_DATE"]) {
          formIsValid = false;
          errorsFocus["BIRTH_DATE"] = 'errorFocus'
          errors["BIRTH_DATE"] = 'กรุณาระบุ วัน/เดือน/ปี เกิด';
        } else {
          var dateBeforDiff = moment(fields["BIRTH_DATE"]).format("YYYY-MM-DD");
          var diff = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "years");
          var diffDay = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "days");
          if (diff < 7 || diffDay <= 2556) {
            formIsValid = false;
            errorsFocus["BIRTH_DATE"] = 'errorFocus'
            errors["BIRTH_DATE"] = 'อายุผู้สมัครยังไม่ถึง 7 ปีบริบูรณ์';
          }
        }
        if(!fields["MARITAL_STATUS"] || fields["MARITAL_STATUS"] === '0'){
          formIsValid = false;
          errorsFocus["MARITAL_STATUS"] = 'errorFocus'
          errors["MARITAL_STATUS"] = 'กรุณาระบุสถานภาพ';
        }
        if (!fields["USER_EMAIL"]) {
          formIsValid = false;
          errorsFocus["USER_EMAIL"] = 'errorFocus'
          errors["USER_EMAIL"] = 'กรุณากรอกอีเมล';
        } else {
          var validEmail = this.validateEmail(fields["USER_EMAIL"]);
          if (validEmail === false) {
            formIsValid = false;
            errorsFocus["USER_EMAIL"] = 'errorFocus'
            errors["USER_EMAIL"] = 'รูปแบบอีเมล์ไม่ถูกต้อง';
          } else {
            var chkValidEmail = await this.checkEmail();
            if (chkValidEmail === true) {
              formIsValid = false;
              errorsFocus["USER_EMAIL"] = 'errorFocus'
              errors["USER_EMAIL"] = 'อีเมล์นี้ มีการลงทะเบียนเรียบร้อยแล้ว';
            }
          }
        }
        if (!fields["PHONE_NO"]) {
          formIsValid = false;
          errorsFocus["PHONE_NO"] = 'errorFocus'
          errors["PHONE_NO"] = 'กรุณาระบุหมายเลขโทรศัพท์'
        } else {
          if(fields["PHONE_NO"].toString().length ) {}
          var validPhone = this.validatePhone(fields["PHONE_NO"])
          if(validPhone === false) {
            formIsValid = false;
            errorsFocus["PHONE_NO"] = 'errorFocus'
            errors["PHONE_NO"] = 'เบอร์โทรศัพท์มือถือควรขึ้นต้นด้วย 06 08 หรือ 09'
          } else {
            var chkValidPhone = await this.checkPhoneExist();
            if (chkValidPhone.status === true) {
              formIsValid = false;
              errorsFocus["PHONE_NO"] = 'errorFocus'
              errors["PHONE_NO"] = (chkValidPhone.msg === "" ? 'หมายเลขโทรศัพท์นี้ มีการลงทะเบียนเรียบร้อยแล้ว' : chkValidPhone.msg)
            }
            // var chkValidPhone = await this.checkPhoneExist();
            // if (chkValidPhone === true) {
            //   formIsValid = false;
            //   errorsFocus["PHONE_NO"] = 'errorFocus'
            //   errors["PHONE_NO"] = <FormattedMessage id="ALERT_PHONE_CANT_USE" />;
            // }
          }
        }
        this.setState({
          errors: errors,
          errorsFocus: errorsFocus
        });
    
        return formIsValid;
    }
    
    handleChange(e) {
        let { errors,errorsFocus, fields, GENDER_ID, TITLE_NAME_REMARK , date , name } = this.state;
        if(e.target.name == 'ADDR_NO'){
          var chkAddr = this.checkAddress(e)
          var chkChar = this.checkCharacter(e)
          if(chkAddr === true) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
            var firstChar = e.target.value.charAt(0)
            var lastChar = e.target.value[e.target.value.length - 1]
            if(firstChar == '-'){
              errors[e.target.name] = '- ต้องไม่เป็นอักขระตัวแรก'
              errorsFocus[e.target.name] = 'errorFocus'
            }else if(lastChar == '-'){
              errors[e.target.name] = '- ต้องไม่เป็นอักขระตัวสุดท้าย'
              errorsFocus[e.target.name] = 'errorFocus'
            }else       
              if(chkChar == true){
                errors[e.target.name] = "มีตัวอักษร ประกอบได้แค่ 1 ตัวอักษรและต้องเป็นตัวสุดท้ายเท่านั้น"
                errorsFocus[e.target.name] = 'errorFocus'
                e.preventDefault();
              }else if(chkChar == false){
                fields[e.target.name] = e.target.value;
                errors[e.target.name] = null;
                errorsFocus[e.target.name] = ''
              }
          } else {
            errors[e.target.name] = 'มี - ประกอบได้แค่ 1 ตัวเท่านั้น'
            errorsFocus[e.target.name] = 'errorFocus'
          }
        }
        else if(e.target.name === 'ADDR_MOO') {
            const re = /^[0-9\b]+$/;
            if (re.test(e.target.value) !== false) {
              fields[e.target.name] = e.target.value;
              errors[e.target.name] = null;
              errorsFocus[e.target.name] = ''
            } else {
              if(e.target.value === '') { fields[e.target.name] = ''; }
                errors[e.target.name] = 'กรุณากรอกเฉพาะตัวเลข'
                errorsFocus[e.target.name] = ''
                errorsFocus["ADDR_MOO"] = 'errorFocus'
                e.preventDefault();
            }
          } 
          else {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } 
        if (e.target.name === "TITLE_NAME_ID") {
          if (
            e.target.value === "1" ||
            e.target.value === "2" ||
            e.target.value === "3"
          ) {
            GENDER_ID = e.target.value === "1" ? 1 : 2;
            fields["GENDER_ID"] = GENDER_ID;
            genderClass = "form-control disabled";
            TITLE_NAME_REMARK = "none";
            fields["TITLE_NAME_REMARK"] = '';
            errors["GENDER_ID"]  = null;
            errorsFocus["GENDER_ID"]  = ''
    
          } else {
            fields["GENDER_ID"] = 4;
            genderClass = "form-control";
            TITLE_NAME_REMARK = "block";
          }
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
          // errors["GENDER_ID"] = null;
          // errorsFocus["GENDER_ID"] = '';
          fields[e.target.name] = parseInt(e.target.value);
        } else if (e.target.name === "CARD_ID") {
          const re = /^[0-9\b]+$/;
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            e.preventDefault();
            return false;
          }
        } else if (e.target.name === "PHONE_NO") {
          const re = /^[0-9\b]+$/;
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            errors[e.target.name] = 'กรุณากรอกเบอร์โทรศัพท์';
            errorsFocus[e.target.name] = ''
            e.preventDefault();
          }
        } else {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        this.setState({ errors, fields, GENDER_ID, TITLE_NAME_REMARK , [e.target.id]: e.target.value , date});
    }

    maxLengthCheck = (object) => {
      if (object.target.value.length > object.target.maxLength) {
          object.target.value = object.target.value.slice(0, object.target.maxLength)
      }
    }
    
    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    
    async checkEmail() {
        return registerAction.checkEmailExist(this.state.fields["USER_EMAIL"]).then(e => {
          var resp = e.data.responsE_INFO;
          if(resp != null){
            if (resp.rescode !== "000") {
              ERROR_USER_EMAIL = false
            } else {
              ERROR_USER_EMAIL = true
            }
          }else{
            console.log('null')
          }
    
          return ERROR_USER_EMAIL
        });
    }
    
    async checkPhoneExist() {
        return registerAction.checkPhoneExist(this.state.fields["PHONE_NO"]).then(e => {
          var resp = e.data.responsE_INFO;
          var response = {}
          if (resp.rescode !== "041")   {
            if(resp.rescode === "042") {
              response = {
                status: true,
                msg:'042'
              }
            } else {
              response = {
                status: true,
                msg: ""
              }
            }
          } else {
            response = {
              status: false,
              msg: ""
            }
          }
          return response
        });
    }
    
    validatePhone(phonenumber) {
        var re = /^[0]{1}[689]/;
        return re.test(String(phonenumber));
    }
    
    async checkCard(type, card) {
        return registerAction.checkexistingIdCard(type, card).then(e => {
          var resp = e.data.responsE_INFO;
          var response = {}
          if(resp != null){
            if (resp.rescode !== "031")   {
              if(resp.rescode === "032") {
                response = {
                  status: true,
                  msg: '032'
                }
              } else {
                response = {
                  status: true,
                  msg: ""
                }
              }
            } else {
              response = {
                status: false,
                msg: ""
              }
            }
          }else{
            response = {
              status: false,
              msg: ""
            }
          }
          return response
        });
    }

    async setUpActive(no){
      if(no === 2){
        if(await this.validateForm()){
          this.setState({ 
            activeColor2 : 'active' , 
            activeColor3 : 'fade' , 
            activeColor1:'fade' , 
            active1:'',
            active2:'active',
            active3:''
          })
        }
      }else if(no === 3){
        this.setState({ disabledBtn: 'disabled' })
        this.setState({ disableAgreement: true })
        if(this.state.agreement === true) {
          this.setState({ 
            activeColor2 : 'fade' , 
            activeColor3 : 'active' , 
            activeColor1:'fade' , 
            active1:'',
            active2:'',
            active3:'active'
          })
        } else {
          this.submitFinalForm()
        }
      }else{
        this.setState({ 
          activeColor3 : 'fade' , 
          activeColor2 : 'fade' , 
          activeColor1 : 'active',
          active1:'active',
          active2:'',
          active3:''
        })
      }
    }

    modalCheckSubmit(res){
      alert = (
        <SweetAlert
          custom
          confirmBtnBsStyle="success"
          closeOnClickOutside={true}
          cancelBtnBsStyle="default"
          focusConfirmBtn={false}
          title=""
          customIcon="../images/cancel.png"
          showConfirm={false}
          showCancelButton
          onCancel={() => this.handleChoice(false)}
          onConfirm={() => this.handleChoice(true)}
          onOutsideClick={() => {
            this.wasOutsideClick = true;
            this.setState({ showConfirm: false })
          }}
        >
          {res}
        </SweetAlert>
      );
  
      this.setState({ show: true , modal: alert })
    }

    async submitFormMb(){
      if(await this.validateForm()){
        this.submitFinalForm();
      }else{
         console.log(false);
      }
    }

    submitFinalForm(){
      var userid = localStorage.getItem('uid');
      registerAction.activateAndRegister(this.state.fields,userid).then(e => {
        if(e.data.isSuccess === false) {
          this.setState({ disableAgreement: false })
          this.setState({ resultApi: e.data.errMsg })
          this.setState({ disabledBtn: '' })
          var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
          this.modalCheckSubmit(this.state.resultApi,imgPopup)
        } else {
          this.setState({ resultApi: e.data.errMsg })
          this.props.history.push({
            pathname: `${process.env.PUBLIC_URL}/PTmaxcard/register/otp`,
            state: {
              data: this.state.fields
            }
          });
        }
      })
    }

    onChange = date => this.setState({ date })

    render(){
        var yearnow = new Date().getFullYear()
        var start = yearnow - parseInt(100);
        var setYear = [];
        var minDate = new Date();
        minDate.setFullYear(minDate.getFullYear() - 100);

        var { fields, amphure, tumbon, postcode } = this.state;
        amphureListData = [];
        for (var i in amphure) {
          amphureListData.push(<option name="ADDR_ORG_AMPHURE_ID" key={i} value={amphure[i].codE_GUID}>{amphure[i].values}</option>)
        }
    
        tumbonListData = [];
        for (var i in tumbon) {
          tumbonListData.push(<option name="ADDR_ORG_SUB_DISTRICT_ID" key={i} value={tumbon[i].codE_GUID}>{tumbon[i].values}</option>)
        }
    
        postcodeListData = [];
        for (var i in postcode) {
            postcodeListData.push(<option name="ADDR_ORG_POSTALCODE_ID" key={i} value={postcode[i].codE_GUID}>{postcode[i].values}</option>)
        }
        return(
          <div>
            <div id="loading" style={{display : this.state.showLoading}}>
              <div className="content_class">
                <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
              </div>
            </div>
            <div className="bg-ptmax" style={{ display: this.state.content_page}}>
              <div id="main">
                <div className="pc-pt headet-ptmaxcard">สมัครบัตรพีที แมกซ์</div>
                <div className="mobile-pt headet-ptmaxcard">สมัครบัตรพีที แมกซ์</div>
                <div className="pc-pt mt-3 mx-5 my-3">
                  <nav>
                    <div>
                      <ul className="nav nav-pills nav-justified" role="tablist">
                        <li className="nav-item">
                          <a className={`nav-item nav-link ${this.state.active1}`}>STEP 1</a>
                        </li>
                        <li className="nav-item">
                          <a className={`nav-item nav-link ${this.state.active2}`}>STEP 2</a>
                        </li>
                        <li className="nav-item">
                          <a className={`nav-item nav-link ${this.state.active3}`}>STEP 3</a>
                        </li>
                      </ul>
                    </div>
                  </nav>
                  <div className="tab-content py-3 px-3 px-sm-0">
                    <div id="step-one" className={`tab-pane ${this.state.activeColor1}`}>
                      <div className="row mx-3">
                        <div className="col-6">
                          <div className="header-profile">ข้อมูลส่วนตัว</div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">หมายเลขบัตรประชาชน/หนังสือเดินทาง</span>
                            </label>
                            <div>
                              <input disabled={!this.state.inputcard}
                                type="text"
                                className={`form-control ${this.state.errorsFocus['CARD_ID']}`}
                                maxLength="13"
                                name="CARD_ID"
                                onChange={e => this.handleChange(e)}
                                id="exampleFormControlInput1"
                                placeholder="กรอกเลขบัตรประชาชน"
                                value={this.state.fields["CARD_TYPE_ID"] == 1 ? this.state.fields["CARD_ID"] : ''}
                              />
                            </div>
                            <div className="errorMsg CARD_ID">{this.state.errors["CARD_ID"]}</div>
                          </div>
                          <label className="check_conditions_form">กรณีเป็นชาวต่างชาติ ( เลขที่หนังสือเดินทาง )
                            <input
                              type="checkbox"
                              name="aggreement"
                              checked={(this.state.fields["CARD_TYPE_ID"] === 1 ? false : true)}
                              onChange={e => this.changeHandle(e)}
                            />
                            <span className="checkmark_condition" />
                          </label>
                          <div className="form-group" style={{ display: this.state.isForeigner }}>
                            <input
                              type="text"
                              className={`form-control input_form ${this.state.errorsFocus['CARD_ID_ENG']}`}
                              name="CARD_ID_ENG"
                              maxLength="10"
                              onChange={(e) => this.handleChange(e)}
                              id="exampleFormControlInput1"
                              placeholder=""
                              value={this.state.fields['CARD_ID_ENG']}
                            />
                            <div className="errorMsg CARD_ID_ENG">{this.state.errors["CARD_ID_ENG"]}</div>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">คำนำหน้า</span>
                            </label>
                            <div>
                              <select
                                className={`form-control ${this.state.errorsFocus['TITLE_NAME_ID']}`}
                                name="TITLE_NAME_ID"
                                id="exampleFormControlSelect1"
                                onChange={e => this.handleChange(e)}
                                value={this.state.fields.TITLE_NAME_ID == 0 ? this.state.fields.TITLE_NAME_ID = '' : this.state.fields.TITLE_NAME_ID}
                              >
                                <option value="0">กรุณาเลือกคำนำหน้าชื่อ</option>
                                {this.renderContent("type_title_name")}
                              </select>
                            </div>
                            <div className="form-group" style={{ display: this.state.TITLE_NAME_REMARK }}>
                              <input
                                type="text"
                                className={`form-control ${this.state.errorsFocus['TITLE_NAME_REMARK']} input_form`}
                                name="TITLE_NAME_REMARK"
                                onChange={e => this.handleChange(e)}
                                id="exampleFormControlInput1"
                                placeholder="คำนำหน้า"
                                value={this.state.fields['TITLE_NAME_REMARK']}
                              />
                              <div className="errorMsg TITLE_NAME_REMARK">{this.state.errors["TITLE_NAME_REMARK"]}</div>
                            </div>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">ชื่อ</span>
                            </label>
                            <input
                              type="text"
                              className={`form-control ${this.state.errorsFocus['FNAME_TH']} input_form`}
                              name="FNAME_TH"
                              onChange={e => this.handleChange(e)}
                              id="exampleFormControlInput1"
                              placeholder="กรอกชื่อ"
                              value={this.state.fields.FNAME_TH}
                            />
                            <div className="errorMsg FNAME_TH">{this.state.errors["FNAME_TH"]}</div>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">นามสกุล</span>
                            </label>
                            <input
                              type="text"
                              className={`form-control ${this.state.errorsFocus['FNAME_TH']} input_form`}
                              name="LNAME_TH"
                              onChange={e => this.handleChange(e)}
                              id="exampleFormControlInput1"
                              placeholder="กรอกนามสกุล"
                              value={this.state.fields.LNAME_TH}
                            />
                            <div className="errorMsg LNAME_TH">{this.state.errors["FNAME_TH"]}</div>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">เพศ</span>
                            </label>
                            <select
                              className={`${genderClass} form-control ${this.state.errorsFocus['GENDER_ID']} input_form`}
                              name="GENDER_ID"
                              id="exampleFormControlSelect1"
                              onChange={e => this.handleChange(e)}
                              value={this.state.fields["GENDER_ID"]}
                            >
                              <option value="0">กรุณาเลือกเพศ </option> {this.renderContent("type_gender")}
                            </select>
                            <div className="errorMsg GENDER_ID"> {this.state.errors["GENDER_ID"]}</div>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">วัน เดือน ปีเกิด</span>
                            </label>
                            <div>
                              <DatePickers 
                                onChange={this.onChange}
                                name="BIRTH_DATE"
                                value={this.state.date} 
                                className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`} 
                                format="dd-MM-yyyy"
                              />
                            </div>
                            <div className="errorMsg BIRTH_DATE">{this.state.errors["BIRTH_DATE"]}</div>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">สถานภาพ</span>
                            </label>
                            <select
                              className={`form-control ${this.state.errorsFocus['MARITAL_STATUS']} input_form`}
                              name="MARITAL_STATUS"
                              id="exampleFormControlSelect1"
                              onChange={e => this.handleChange(e)}
                              value={this.state.fields['MARITAL_STATUS']}
                            >
                              <option value="0">กรุณาเลือกสถานภาพ </option>{this.renderContent("type_status")}
                            </select>
                            <div className="errorMsg MARITAL_STATUS"> {this.state.errors["MARITAL_STATUS"]}</div>
                          </div>
                          <div className="header-profile">ข้อมูลติดต่อ</div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">อีเมล</span>
                            </label>
                            <input
                              type="text"
                              className={`form-control ${this.state.errorsFocus['USER_EMAIL']} input_form`}
                              name="USER_EMAIL"
                              onChange={e => this.handleChange(e)}
                              id="exampleFormControlInput1"
                              placeholder="กรอกอีเมล"
                              value={this.state.fields['USER_EMAIL']}
                            />
                            <div className="errorMsg USER_EMAIL">{this.state.errors["USER_EMAIL"]}</div>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2">เบอร์โทรศัพท์</span>
                            </label>
                            <input
                              type="number"
                              maxLength="10"
                              className={`form-control ${this.state.errorsFocus['PHONE_NO']} input_form ${(this.state.fields["PHONE_NO"] ? `` : '')}`}
                              name="PHONE_NO"
                              onInput={this.maxLengthCheck}
                              onChange={e => this.handleChange(e)}
                              id="exampleFormControlInput1"
                              placeholder="กรอกเบอร์โทรศัพท์"
                              value={this.state.fields["PHONE_NO"]}
                            />
                            <div className="errorMsg PHONE_NO"> {this.state.errors["PHONE_NO"]}</div>
                          </div>
                        </div>          
                        <div className="col-6">
                          <div className="header-profile">ข้อมูลที่อยู่</div>
                          <div style={{display:this.state.chkDisplayAdd}}>        
                            <div className="form-group">
                              <span className="label-validate">* </span>
                              <label className="label font-header-label pl-2">บ้านเลขที่</label>
                              <input 
                                type="text" 
                                className={`form-control input_form ${this.state.errorsFocus['ADDR_NO']}`} 
                                id="exampleFormControlInput1" 
                                maxLength="10" 
                                name="ADDR_NO" 
                                onChange={(e) => this.handleChange(e)} 
                                value={this.state.fields['ADDR_NO']}
                                placeholder="กรอกบ้านเลขที่"
                              />
                              <div className="errorMsg">{this.state.errors['ADDR_NO']}</div>
                            </div>
                            <div className="form-group">
                              <label>หมู่บ้าน</label>
                              <input
                                type="text"
                                className={`form-control input_form ${this.state.errorsFocus['ADDR_MOO']}`}
                                maxLength="3"
                                id="exampleFormControlInput1"
                                name="ADDR_MOO"
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.fields['ADDR_MOO']}
                                placeholder="กรอกหมู่"
                              />
                              <div className="errorMsg">{this.state.errors['ADDR_MOO']}</div>
                            </div>
                            <div className="form-group">
                              <label>หมู่บ้าน/อาคาร</label>
                              <input 
                                type="text" 
                                className="form-control input_form" 
                                id="exampleFormControlInput1" 
                                name="ADDR_VILLEGE" 
                                onChange={(e) => this.handleChange(e)} 
                                value={this.state.fields['ADDR_VILLEGE']} 
                                placeholder="กรุณากรอกหมู่บ้าน/อาคาร"
                              />
                            </div>
                            <div className="form-group">
                              <label>ตรอก/ซอย</label>
                              <input 
                                type="text" 
                                className="form-control input_form" 
                                name="ADDR_SOI" 
                                onChange={(e) => this.handleChange(e)} 
                                value={this.state.fields.ADDR_SOI} 
                                id="exampleFormControlInput1" 
                                placeholder="กรุณากรอกตรอก/ซอย"
                              />
                            </div>
                            <div className="form-group">
                              <label>ถนน</label>
                              <input 
                                type="text" 
                                className="form-control input_form" 
                                name="ADDR_STREET" 
                                onChange={(e) => this.handleChange(e)} 
                                value={this.state.fields.ADDR_STREET} 
                                id="exampleFormControlInput1" 
                                placeholder="กรุณากรอกถนน"
                              />
                            </div>
                            <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2">จังหวัด</span>
                              </label>
                              {this.renderContentProvince('type_province')}
                            </div>
                            <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2">อำเภอ</span>
                              </label>
                              <select
                                className={`form-control ${this.state.errorsFocus['ADDR_ORG_AMPHURE_ID']}`}
                                id='exampleFormControlSelect1'
                                name="ADDR_ORG_AMPHURE_ID"
                                onChange={(e) => this.getTumbon(e.target.value, 'onchange')}
                                value={this.state.fields['ADDR_ORG_AMPHURE_ID'] == 0 ? this.state.fields['ADDR_ORG_AMPHURE_ID'] = '' : this.state.fields['ADDR_ORG_AMPHURE_ID']}
                              >
                                <option value="0">กรุณาเลือกอำเภอ</option>{amphureListData}
                              </select>
                              <div className="errorMsg">{this.state.errors['ADDR_ORG_AMPHURE_ID']}</div>
                            </div>
                            <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2">ตำบล</span>
                              </label>
                              <select
                                className={`form-control ${this.state.errorsFocus['ADDR_ORG_SUB_DISTRICT_ID']}`}
                                id='exampleFormControlSelect1'
                                name="ADDR_ORG_SUB_DISTRICT_ID"
                                onChange={(e) => this.getPostcode(e.target.value, 'onchange')}
                                value={this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] == 0 ? this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] = '' : this.state.fields['ADDR_ORG_SUB_DISTRICT_ID']}
                              >
                                <option value="0">กรุณาเลือกตำบล</option>{tumbonListData}
                              </select>
                              <div className="errorMsg">  {this.state.errors['ADDR_ORG_SUB_DISTRICT_ID']} </div>
                            </div>
                            <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2">รหัสไปรษณีย์</span>
                              </label>
                              <select 
                                className={`form-control ${this.state.errorsFocus['ADDR_ORG_POSTALCODE_ID']}`}
                                id='exampleFormControlSelect1' 

                                name="ADDR_ORG_POSTALCODE_ID" 
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.fields['ADDR_ORG_POSTALCODE_ID'] == 0 ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] = '' : (this.state.fields['ADDR_ORG_POSTALCODE_ID'] !== '' ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] : '')}
                              >
                                <option value="0">กรุณาเลือกรหัสไปรษณีย์</option>{postcodeListData}
                              </select>
                              <div className="errorMsg"> {this.state.errors['ADDR_ORG_POSTALCODE_ID']} </div>
                            </div>
                          <div className="form-group">
                            <label>ข้อมูลที่อยู่ (เพิ่มเติม )</label>
                            <textarea 
                              className="form-control input_form" 
                              id="exampleFormControlTextarea1" 
                              rows="3" 
                              name="ADDR_OTHER" 
                              value={this.state.fields.ADDR_OTHER} 
                              onChange={(e) => this.handleChange(e)} 
                              placeholder="กรุณากรอกข้อมูลเพิ่มเติม"
                            >
                            </textarea>
                          </div>
                          </div>
                          <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}><div className="bg_full">
  <div className="container-fluid"><div className="content_page"><div className="head_title_agree"></div><div className="form-group"><label><span className="required">* </span></label>
    <textarea className="form-control input_form" id="exampleFormControlTextarea1" rows="3" name="ADDR_OTHER" value={this.state.fields.ADDR_OTHER} onChange={(e) => this.handleChange(e)} placeholder=""></textarea>
    <div className="errorMsg">{this.state.errors['ADDR_OTHER']}</div>
  </div>
  <div className="btn_nextstep">
  <button type="submit" className="btn btn-secondary btn-block btn_agreement"></button>
  </div></div></div></div></form>
                          <div className="btn_nextstep nav nav-tabs nav-fill my-2" id="nav-tab" role="tablist">
                            <a className="btn btn-secondary btn-block" onClick={() => this.setUpActive(2)}>คลิกเพื่อดำเนินการต่อ</a>
                          </div>
                        </div>
                      </div>
                    </div>             
                    
                    <div id="step-two" className={`tab-pane ${this.state.activeColor2}`}>
                      <div className="bg_fullall">
                        <div className="container-fluid">
                          <div className="content_page">
                            <div className="header-profile">ข้อมูลเพิ่มเติม (รูปภาพ)</div>
                            <div className="errorImageSize">{this.state.errorFileSize}</div>
                              <div className="">
                                <div className="row row_image">
                                  <div className="col-4 col-sm-3 review_image_before">
                                    <div className="upload_preview" disabled={false}>
                                      <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview1')} style={{ display: this.state.closeBtn['imgPreview1'] }}>
                                        <i className="fa fa-times-circle" aria-hidden="true"></i>
                                      </div>
                                      <div onClick={() => this.handleUploadFile(1)}>
                                        <input type="file" name="imgPreview1" style={{ display: "none" }} ref={input => this.inputElement_1 = input} onChange={(e) => this.handleLoadAvatar(e)} />
                                        <img src={this.state.imgPreview['imgPreview1']} alt="" className={this.state.classPreview['imgPreview1']} />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-4 col-sm-3 review_image_before">
                                    <div className="upload_preview">
                                      <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview2')} style={{ display: this.state.closeBtn['imgPreview2'] }}><i className="fa fa-times-circle" aria-hidden="true"></i></div>
                                      <div onClick={() => this.handleUploadFile(2)}>
                                        <input type="file" name="imgPreview2" style={{ display: "none" }} ref={input => this.inputElement_2 = input} onChange={(e) => this.handleLoadAvatar(e)} disabled={disabledimage}/>
                                        <img src={this.state.imgPreview['imgPreview2']} alt="" className={this.state.classPreview['imgPreview2']} />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-4 col-sm-3 review_image_before">
                                    <div className="upload_preview">
                                      <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview3')} style={{ display: this.state.closeBtn['imgPreview3'] }}><i className="fa fa-times-circle" aria-hidden="true"></i></div>
                                      <div onClick={() => this.handleUploadFile(3)}>
                                          <input type="file" name="imgPreview3" style={{ display: "none" }} ref={input => this.inputElement_3 = input} onChange={(e) => this.handleLoadAvatar(e)} disabled={disabledimage2}/>
                                          <img src={this.state.imgPreview['imgPreview3']} alt="" className={this.state.classPreview['imgPreview3']} />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <label className="check_conditions my-3">
                                ต้องการให้ข้อมูลเพิ่มเติม เพื่อสิทธิประโยชน์ที่ตรงกับความต้องการของท่าน
                                <input type="checkbox" onChange={(e) => this.changeHandle(e)} name="aggreement" disabled={this.state.disableAgreement}/>
                                <span className="checkmark_condition"></span>
                              </label>
                              <div className="container-fluid btn btn-secondary btn-block btn_agreement mb-3" style={{display:this.state.submitFormNoadditional}}>
                                <button onClick={() => this.submitFormMb()} className="send-font">ส่งข้อมูลเพื่อสมัครสมาชิก</button>
                              </div>
                              <div className="btn_nextstep mb-3" id="nav-tab" role="tablist" style={{display:this.state.additionnal}} >
                                <a 
                                  className={`btn btn-secondary btn-block btn_agreement ${this.state.disabledBtn}`} 
                                  id="nav-contact-tab" 
                                  data-toggle="tab" 
                                  href="#step-three" 
                                  role="tab" 
                                  aria-controls="step-three" 
                                  aria-selected="false"
                                  onClick={() => this.setUpActive(3)}
                                >
                                  คลิกเพื่อให้ข้อมูลเพิ่มเติม
                                </a>
                              </div>
                              {this.state.modal}
                            </div>
                          </div>
                        </div>
                      </div>
                    
                    <div id="step-three" className={`tab-pane ${this.state.activeColor3}`}>
                      <div className="bg_full">
                        <div className="row container-fluid">
                          <div className="col-6 mt-3 content_page">
                            <div className="form-group">
                              <label className="addtional">ประเภทรถ</label>
                              <select className={`form-control ${this.state.errorsFocus['CAR_TYPE']}`}
                                name="CAR_TYPE" 
                                id='exampleFormControlSelect1'
                                value={this.state.fields['CAR_TYPE']}
                                onChange={(e) => this.dropdownlistCheck(e)}>
                                <option value="0">กรุณาเลือกประเภทรถ</option>
                                {this.renderAdditional('type_cartype')}
                              </select>
                              <div className="errorMsg CAR_TYPE">{this.state.errors["CAR_TYPE"]}</div>
                            </div>
                            <div className="form-group">
                              <label className="addtional">ยี่ห้อรถ</label>
                              <select 
                                className={`form-control ${this.state.errorsFocus['CAR_BRAND']}`} 
                                name="CAR_BRAND" 
                                value={this.state.fields['CAR_BRAND']}
                                id='exampleFormControlSelect1' 
                                onChange={(e) => this.dropdownlistCheck(e)}>
                                <option value="0">
                                กรุณาเลือกยี่ห้อรถ
                                </option>
                                {this.renderAdditional('type_brand')}
                              </select>
                              <div className="errorMsg CAR_BRAND">{this.state.errors['CAR_BRAND']}</div>
                              <div className="form-group" style={{ display: this.state.CAR_BRAND_REMARK}}>
                                <input
                                  type="text"
                                  className={`form-control ${this.state.errorsFocus['CAR_BRAND_REMARK']} input_form`}
                                  name="CAR_BRAND_REMARK"
                                  value={this.state.fields['CAR_BRAND_REMARK']}
                                  onChange={e => this.dropdownlistCheck(e)}
                                  id="exampleFormControlInput1"
                                  placeholder="หมายเหตุประเภทรถยนต์"
                                />
                                <div className="errorMsg CAR_BRAND_REMARK">{this.state.errors['CAR_BRAND_REMARK']}</div>
                              </div>
                            </div>
                            <div className="form-group">
                              <label className="addtional">อาชีพ</label>
                              <select 
                                className={`form-control ${this.state.errorsFocus['OCCUPATION_ID']}`}
                                name="OCCUPATION_ID" 
                                value={this.state.fields['OCCUPATION_ID']}
                                id='exampleFormControlSelect1' 
                                onChange={(e) => this.dropdownlistCheck(e)}>
                                <option value="0" >กรุณาเลือกอาชีพ</option>
                                {this.renderAdditional('type_occupation')}
                              </select>
                              <div className="errorMsg OCCUPATION_ID">{this.state.errors['OCCUPATION_ID']} </div>
                              <div className="form-group" style={{ display: this.state.OCCAPATION_REMARK}}>
                                <input
                                  type="text"
                                  className={`form-control ${this.state.errorsFocus['OCCAPATION_REMARK']} input_form`}
                                  name="OCCAPATION_REMARK"
                                  value={this.state.fields['OCCAPATION_REMARK']}
                                  onChange={e => this.dropdownlistCheck(e)}
                                  id="exampleFormControlInput1"
                                  placeholder="โปรดระบุรายละเอียดอาชีพอื่นๆ"
                                />
                                <div className="errorMsg OCCAPATION_REMARK">{this.state.errors['OCCAPATION_REMARK']}</div> 
                              </div>
                            </div>
                            <div className="form-group">
                              <label className="addtional">การศึกษา</label>
                              <select 
                                className={`form-control ${this.state.errorsFocus['EDUCATION_ID']}`}
                                name="EDUCATION_ID" 
                                value={this.state.fields['EDUCATION_ID']}
                                id='exampleFormControlSelect1' 
                                onChange={(e) => this.dropdownlistCheck(e)}>
                                <option value="0">กรุณาเลือกระดับการศึกษา</option>
                                {this.renderAdditional('type_education')}
                              </select>
                              <div className="errorMsg EDUCATION_ID">{this.state.errors["EDUCATION_ID"]} </div>
                            </div>
                            <div className="form-group">
                              <label className="addtional">รายได้ต่อเดือน</label>
                              <select  
                                className={`form-control ${this.state.errorsFocus['INCOME_ID']}`}
                                name="INCOME_ID" 
                                value={this.state.fields['INCOME_ID']}
                                id='exampleFormControlSelect1' 
                                onChange={(e) => this.dropdownlistCheck(e)}>
                                <option value="0">กรุณาเลือกรายได้ต่อเดือน</option>
                                {this.renderAdditional('type_income')}
                              </select>
                              <div className="errorMsg INCOME_ID">{this.state.errors["INCOME_ID"]}</div>
                            </div>
                            <label className="recommend">ท่านยินดีรับข่าวสาร/โปรโมชั่นจากทางใด(เลือกได้มากกว่า 1 ข้อ)</label>
                            <div className="row">
                              <div className="col">
                                <label className="check_conditions_select">จดหมาย
                                  <input type="checkbox" id="SUBSCRIBE_LETTER" name="news"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_LETTER']) ? (this.state.fields['SUBSCRIBE_LETTER'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                                  <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="check_conditions_select">อีเมล
                                  <input type="checkbox" id="SUBSCRIBE_EMAIL" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_EMAIL']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                                  <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="check_conditions_select">ข้อความทางโทรศัพท์มือถือ (SMS)
                                  <input type="checkbox" id="SUBSCRIBE_SMS" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_SMS']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                                  <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="check_conditions_select">ไม่ประสงค์รับข่าวสาร
                                  <input type="checkbox" id="SUBSCRIBE_NONE" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_NONE']) ? (this.state.fields['SUBSCRIBE_NONE'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false } />
                                  <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                            </div>
                            <label className="addtional">โปรโมชั่นที่น่าสนใจ</label>
                            <div className="row">
                              <div className="col">
                                <label className="selectoneonly">อาหาร
                                  <input type="checkbox" id="PMT_FOOD" checked={this.state.fields['PMT_FOOD']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ท่องเที่ยว
                                  <input type="checkbox" id="PMT_TRAVEL" checked={this.state.fields['PMT_TRAVEL']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">ช้อปปิ้ง
                                  <input type="checkbox" id="PMT_SHOPPING" checked={this.state.fields['PMT_SHOPPING']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ไลฟ์สไตล์และความงาม
                                  <input type="checkbox" id="PMT_BEAUTY" checked={this.state.fields['PMT_BEAUTY']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">กีฬาและรถยนต์
                                  <input type="checkbox" id="PMT_SPORT_AND_CAR" checked={this.state.fields['PMT_SPORT_AND_CAR']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อื่นๆโปรดระบุ
                                  <input type="checkbox" id="PMT_OTHERS" checked={this.state.fields['PMT_OTHERS']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                                <div className="form-group">
                                  <input 
                                    type="text" 
                                    id="PMT_OTHERS_REMARK" 
                                    value={this.state.fields['PMT_OTHERS_REMARK']} 
                                    className={disabled_block} 
                                    placeholder="อื่นๆโปรดระบุข้อมูล"
                                    value={this.state.fields['PMT_OTHERS_REMARK']}  
                                    onChange={(e) => this.handleGameClik(e)} 
                                  />
                                </div>
                              </div>
                            </div>
                            <label className="addtional">กิจกรรมที่ท่านสนใจ</label>
                            <div><label>เอ็นเตอร์เทนเมนท์</label></div>
                            <div className="row">
                              <div className="col">
                                <label className="selectoneonly">ฟังเพลง
                                  <input type="checkbox" id="ACT_EN_MUSIC" checked={this.state.fields['ACT_EN_MUSIC']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">คอนเสิร์ต
                                  <input type="checkbox" id="ACT_EN_CONCERT" checked={this.state.fields['ACT_EN_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">คอนเสิร์ตลูกทุ่ง
                                  <input type="checkbox" id="ACT_EN_COUNTRY_CONCERT" checked={this.state.fields['ACT_EN_COUNTRY_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ภาพยนตร์
                                  <input type="checkbox" id="ACT_EN_MOVIES" checked={this.state.fields['ACT_EN_MOVIES']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">ละครเวที
                                  <input type="checkbox" id="ACT_EN_THEATER" checked={this.state.fields['ACT_EN_THEATER']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อื่นๆโปรดระบุ
                                  <input type="checkbox" id="ACT_EN_OTHERS" checked={this.state.fields['ACT_EN_OTHERS']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                                <div className="form-group">
                                  <input 
                                    type="text" 
                                    id="ACT_EN_OTHERS_REMARK" 
                                    className={entertain_remark_display} 
                                    placeholder="อื่นๆโปรดระบุข้อมูล"
                                    value={this.state.fields['ACT_EN_OTHERS_REMARK']}  
                                    onChange={(e) => this.handleGameClick(e)} 
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-6 mt-5 content_page">
                            <label>ด้านกีฬา</label>
                            <div className="row">
                              <div className="col">
                                <label className="selectoneonly">ฟิตเนส
                                  <input type="checkbox" id="ACT_SPT_FITNESS" checked={this.state.fields['ACT_SPT_FITNESS']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">วิ่ง
                                  <input type="checkbox" id="ACT_SPT_RUN" checked={this.state.fields['ACT_SPT_RUN']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">ปั่นจักรยาน
                                  <input type="checkbox" id="ACT_SPT_CYCLING" checked={this.state.fields['ACT_SPT_CYCLING']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ฟุตบอล
                                  <input type="checkbox" id="ACT_SPT_FOOTBALL" checked={this.state.fields['ACT_SPT_FOOTBALL']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อื่นๆโปรดระบุ
                                  <input type="checkbox" id="ACT_SPT_OTHERS" checked={this.state.fields['ACT_SPT_OTHERS']} name="sport"  onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                                <div className="form-group">
                                  <input 
                                    type="text" 
                                    id="ACT_SPT_OTHERS_REMARK" 
                                    className={sport_remark_display} 
                                    placeholder="อื่นๆโปรดระบุข้อมูล"
                                    value={this.state.fields['ACT_SPT_OTHERS_REMARK']}  
                                    onChange={(e) => this.handleGameClick(e)} 
                                  />
                                </div>
                              </div>
                            </div>
                            <label className="addtional">ด้านกิจกรรมนอกบ้าน</label>
                            <div className="row">
                              <div className="col">
                                <label className="selectoneonly">ช้อปปิ้ง
                                  <input type="checkbox" id="ACT_OD_SHOPPING" checked={this.state.fields['ACT_OD_SHOPPING']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ความงาม
                                  <input type="checkbox" id="ACT_OD_BEAUTY" checked={this.state.fields['ACT_OD_BEAUTY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">ร้านอาหาร
                                  <input type="checkbox" id="ACT_OD_RESTAURANT" checked={this.state.fields['ACT_OD_RESTAURANT']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">สถานบันเทิง
                                  <input type="checkbox" id="ACT_OD_CLUB" checked={this.state.fields['ACT_OD_CLUB']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">กิจกรรมแม่และเด็ก
                                  <input type="checkbox" id="ACT_OD_MOMCHILD" checked={this.state.fields['ACT_OD_MOMCHILD']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">แรลลี่
                                  <input type="checkbox" id="ACT_OD_RALLY" checked={this.state.fields['ACT_OD_RALLY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">การผจญภัย
                                  <input type="checkbox" id="ACT_OD_ADVANTURE" checked={this.state.fields['ACT_OD_ADVANTURE']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ถ่ายภาพ
                                  <input type="checkbox" id="ACT_OD_PHOTO" checked={this.state.fields['ACT_OD_PHOTO']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">ไหว้พระและการทำบุญ
                                  <input type="checkbox" id="ACT_OD_PHILANTHROPY" checked={this.state.fields['ACT_OD_PHILANTHROPY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ดูดวงและฮวงจุ้ย
                                  <input type="checkbox" id="ACT_OD_HOROSCOPE" checked={this.state.fields['ACT_OD_HOROSCOPE']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">ท่องเที่ยวต่างประเทศ
                                  <input type="checkbox" id="ACT_OD_ABROAD" checked={this.state.fields['ACT_OD_ABROAD']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ท่องเที่ยวในประเทศ
                                  <input type="checkbox" id="ACT_OD_UPCOUNTRY" checked={this.state.fields['ACT_OD_UPCOUNTRY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อื่นๆโปรดระบุ
                                  <input type="checkbox" id="ACT_OD_OTHERS" checked={this.state.fields['ACT_OD_OTHERS']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                                <div className="form-group">
                                  <input type="text" id="ACT_OD_OTHERS_REMARK" className={outside_remark_display} placeholder="อื่นๆโปรดระบุข้อมูล" value={this.state.fields['ACT_OD_OTHERS_REMARK']}  onChange={(e) => this.handleGameClick(e)} />
                                </div>
                              </div>
                            </div>
                            <label className="addtional">ด้านกิจกรรมในบ้าน</label>
                            <div className="row">
                              <div className="col">
                                <label className="selectoneonly">ทำอาหาร
                                  <input type="checkbox"  id="ACT_ID_COOKING" checked={this.state.fields['ACT_ID_COOKING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ทำขนม
                                  <input type="checkbox"  id="ACT_ID_BAKING" checked={this.state.fields['ACT_ID_BAKING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">ตกแต่งบ้าน
                                  <input type="checkbox"  id="ACT_ID_DECORATE" checked={this.state.fields['ACT_ID_DECORATE']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ทำสวน
                                  <input type="checkbox"  id="ACT_ID_GARDENING" checked={this.state.fields['ACT_ID_GARDENING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อ่านหนังสือ
                                  <input type="checkbox"  id="ACT_ID_READING" checked={this.state.fields['ACT_ID_READING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">เล่นเกมส์
                                  <input type="checkbox"  id="ACT_ID_GAMING" checked={this.state.fields['ACT_ID_GAMING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อินเทอร์เน็ต
                                  <input type="checkbox"  id="ACT_ID_INTERNET" checked={this.state.fields['ACT_ID_INTERNET']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อื่นๆโปรดระบุ
                                  <input type="checkbox"  id="ACT_ID_OTHERS" checked={this.state.fields['ACT_ID_OTHERS']} name="inside" value="inside_other" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                                <div className="form-group">
                                  <input type="text" id="ACT_ID_OTHERS_REMARK" className={inside_remark_display}placeholder="อื่นๆโปรดระบุข้อมูล" value={this.state.fields['ACT_ID_OTHERS_REMARK']}  onChange={(e) => this.handleGameClick(e)} />
                                </div>
                              </div>
                            </div>
                            <label className="addtional">ลักษณะกิจกรรมที่ท่านสนใจ</label>
                            <div className="row">
                              <div className="col">
                                <label className="check_conditions_select">กิจกรรมสำหรับเพื่อนฝูง
                                  <input type="checkbox"  id="ACT_HNGOUT_FRIEND" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FRIEND']) ? (this.state.fields['ACT_HNGOUT_FRIEND'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false}  />
                                    <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="check_conditions_select">กิจกรรมสำหรับคู่รัก
                                  <input type="checkbox"  id="ACT_HNGOUT_COUPLE" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_COUPLE']) ? (this.state.fields['ACT_HNGOUT_COUPLE'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                                    <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="check_conditions_select">กิจกรรมสำหรับครอบครัว
                                  <input type="checkbox"  id="ACT_HNGOUT_FAMILY" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FAMILY']) ? (this.state.fields['ACT_HNGOUT_FAMILY'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                                  <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="check_conditions_select">ไม่สนใจทำกิจกรรม
                                  <input type="checkbox"  id="ACT_HNGOUT_NO" name="activities"   onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_NO'] === true ? true : false)} />
                                  <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                            </div>
                            <div className="container-fluid btn btn-secondary btn-block btn_agreement mb-3">
                              <button type="button" className={`${this.state.disabledBtn}`} onClick={(e) => this.submitFinalForm(e)}>ส่งข้อมูลเพื่อสมัครสมาชิก</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="mobile-pt mt-3 my-3 box-history">
                  <div className="row mx-1">
                    <div className="col-12">
                      <div className="header-profile">ข้อมูลส่วนตัว</div>
                      <div className="form-group">
                        <label>
                          <span className="label-validate">* </span>
                          <span className="font-header-label pl-2">หมายเลขบัตรประชาชน/หนังสือเดินทาง</span>
                        </label>
                        <div>
                          <input 
                            disabled={!this.state.inputcard}
                            type="text"
                            className={`form-control ${this.state.errorsFocus['CARD_ID']}`}
                            maxLength="13"
                            name="CARD_ID"
                            onChange={e => this.handleChange(e)}
                            id="exampleFormControlInput1"
                            placeholder="กรอกเลขบัตรประชาชน"
                            value={this.state.fields["CARD_TYPE_ID"] == 1 ? this.state.fields["CARD_ID"] : ''}
                          />
                        </div>
                        <div className="errorMsg CARD_ID">{this.state.errors["CARD_ID"]}</div>
                      </div>
                      <label className="check_conditions_form">กรณีเป็นชาวต่างชาติ ( เลขที่หนังสือเดินทาง )
                        <input
                          type="checkbox"
                          name="aggreement"
                          checked={(this.state.fields["CARD_TYPE_ID"] === 1 ? false : true)}
                          onChange={e => this.changeHandle(e)}
                        />
                        <span className="checkmark_condition" />
                      </label>
                      <div className="form-group" style={{ display: this.state.isForeigner }}>
                        <input
                          type="text"
                          className={`form-control input_form ${this.state.errorsFocus['CARD_ID_ENG']}`}
                          name="CARD_ID_ENG"
                          maxLength="10"
                          onChange={(e) => this.handleChange(e)}
                          id="exampleFormControlInput1"
                          placeholder=""
                          value={this.state.fields['CARD_ID_ENG']}
                        />
                        <div className="errorMsg CARD_ID_ENG">{this.state.errors["CARD_ID_ENG"]}</div>
                      </div>
                      <div className="form-group">
                        <label>
                          <span className="label-validate">* </span>
                          <span className="font-header-label pl-2">คำนำหน้า</span>
                        </label>
                        <div>
                          <select
                            className={`form-control ${this.state.errorsFocus['TITLE_NAME_ID']}`}
                            name="TITLE_NAME_ID"
                            id="exampleFormControlSelect1"
                            onChange={e => this.handleChange(e)}
                            value={this.state.fields.TITLE_NAME_ID == 0 ? this.state.fields.TITLE_NAME_ID = '' : this.state.fields.TITLE_NAME_ID}
                          >
                            <option value="0">
                              กรุณาเลือกคำนำหน้าชื่อ
                            </option>
                            {this.renderContent("type_title_name")}
                          </select>
                        </div>
                        <div className="form-group" style={{ display: this.state.TITLE_NAME_REMARK }}>
                          <input
                            type="text"
                            className={`form-control ${this.state.errorsFocus['TITLE_NAME_REMARK']} input_form`}
                            name="TITLE_NAME_REMARK"
                            onChange={e => this.handleChange(e)}
                            id="exampleFormControlInput1"
                            placeholder="คำนำหน้า"
                            value={this.state.fields['TITLE_NAME_REMARK']}
                          />
                          <div className="errorMsg TITLE_NAME_REMARK">{this.state.errors["TITLE_NAME_REMARK"]}</div>
                        </div>
                      </div>
                  <div className="form-group">
                    <label>
                      <span className="label-validate">* </span>
                      <span className="font-header-label pl-2">ชื่อ</span>
                    </label>
                    <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['FNAME_TH']} input_form`}
                      name="FNAME_TH"
                      onChange={e => this.handleChange(e)}
                      id="exampleFormControlInput1"
                      placeholder="กรอกชื่อ"
                      value={this.state.fields.FNAME_TH}
                    />
                    <div className="errorMsg FNAME_TH">{this.state.errors["FNAME_TH"]}</div>
                  </div>
                  <div className="form-group">
                    <label>
                      <span className="label-validate">* </span>
                      <span className="font-header-label pl-2">นามสกุล</span>
                    </label>
                    <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['FNAME_TH']} input_form`}
                      name="LNAME_TH"
                      onChange={e => this.handleChange(e)}
                      id="exampleFormControlInput1"
                      placeholder="กรอกนามสกุล"
                      value={this.state.fields.LNAME_TH}
                    />
                    <div className="errorMsg LNAME_TH">{/* {this.state.errors["FNAME_TH"]} */}</div>
                  </div>
                  <div className="form-group">
                    <label>
                      <span className="label-validate">* </span>
                      <span className="font-header-label pl-2">เพศ</span>
                    </label>
                    <select
                      className={`${genderClass} form-control ${this.state.errorsFocus['GENDER_ID']} input_form`}
                      name="GENDER_ID"
                      id="exampleFormControlSelect1"
                      onChange={e => this.handleChange(e)}
                      value={this.state.fields["GENDER_ID"]}
                    >
                      <option value="0">กรุณาเลือกเพศ</option>
                      {this.renderContent("type_gender")}
                    </select>
                    <div className="errorMsg GENDER_ID">{this.state.errors["GENDER_ID"]}</div>
                  </div>
                  <div className="form-group">
                    <label>
                      <span className="label-validate">* </span>
                      <span className="font-header-label pl-2">วัน เดือน ปีเกิด</span>  
                    </label>
                    {/* <div className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`} onClick={this.renderDatepickerMobile}>
                      {this.state.fields['BIRTH_DATE'] == "" ? "กรุณาเลือกวันเดือนปีเกิด" :  moment(this.state.fields['BIRTH_DATE']).add(543,'y').format('DD/MM/YYYY') }
                    </div> */}
                    {/* <div>
                      <DatePicker
                        className="form-control inpot_form"
                        selected={this.state.startDate}
                        onChange={this.handChange}
                      />
                    </div> */}
                    <input
                      type="date"
                      // max={maxDate}
                      name="BIRTH_DATE"
                      onChange={e => this.handleChange(e)}
                      className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`}
                      id="date"
                      value={this.state.fields['BIRTH_DATE']}
                    /> 
                    <div className="errorMsg BIRTH_DATE">{this.state.errors["BIRTH_DATE"]}</div>
                  </div>
                  <div className="form-group">
                    <label>
                      <span className="label-validate">* </span>
                      <span className="font-header-label pl-2">สถานภาพ</span>  
                    </label>
                    <select
                      className={`form-control ${this.state.errorsFocus['MARITAL_STATUS']} input_form`}
                      name="MARITAL_STATUS"
                      id="exampleFormControlSelect1"
                      onChange={e => this.handleChange(e)}
                      value={this.state.fields['MARITAL_STATUS']}
                    >
                      <option value="0">กรุณาเลือกสถานภาพ</option>
                      {this.renderContent("type_status")}
                    </select>
                    <div className="errorMsg MARITAL_STATUS">{this.state.errors["MARITAL_STATUS"]}</div>
                  </div>
                  <div className="header-profile">ข้อมูลติดต่อ</div>
                  <div className="form-group">
                    <label>
                      <span className="label-validate">* </span>
                      <span className="font-header-label pl-2">อีเมล</span>  
                    </label>
                    <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['USER_EMAIL']} input_form`}
                      name="USER_EMAIL"
                      onChange={e => this.handleChange(e)}
                      id="exampleFormControlInput1"
                      placeholder="กรอกอีเมล"
                      value={this.state.fields['USER_EMAIL']}
                    />
                    <div className="errorMsg USER_EMAIL">{this.state.errors["USER_EMAIL"]}</div>
                  </div>
                  <div className="form-group">
                    <label>
                      <span className="label-validate">* </span>
                      <span className="font-header-label pl-2">เบอร์โทรศัพท์</span> 
                    </label>
                      <input
                        type="number"
                        maxLength="10"
                        className={`form-control ${this.state.errorsFocus['PHONE_NO']} input_form ${(this.state.fields["PHONE_NO"] ? `` : '')}`}
                        name="PHONE_NO"
                        onChange={e => this.handleChange(e)}
                        id="exampleFormControlInput1"
                        placeholder="กรอกเบอร์โทรศัพท์"
                        value={this.state.fields["PHONE_NO"]}
                      />
                      <div className="errorMsg PHONE_NO"> {this.state.errors["PHONE_NO"]} </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="header-profile">ข้อมูลที่อยู่</div>
                      <div className="form-group">
                        <label>
                          <span className="label-validate">* </span>
                          <span className="font-header-label pl-2">บ้านเลขที่</span> 
                        </label>
                        <input 
                            type="text" 
                            className={`form-control input_form ${this.state.errorsFocus['ADDR_NO']}`} 
                            id="exampleFormControlInput1" 
                            maxLength="10" 
                            name="ADDR_NO" 
                            onChange={(e) => this.handleChange(e)} 
                            value={this.state.fields['ADDR_NO']}
                            placeholder="กรอกบ้านเลขที่"
                        />
                        <div className="errorMsg">{this.state.errors['ADDR_NO']} </div>
                      </div>
                      <div className="form-group">
                          <label>
                            <span className="font-header-label pl-2">หมู่</span>
                          </label>
                          <input
                            type="text"
                            className={`form-control input_form ${this.state.errorsFocus['ADDR_MOO']}`}
                            maxLength="3"
                            id="exampleFormControlInput1"
                            name="ADDR_MOO"
                            onChange={(e) => this.handleChange(e)}
                            value={this.state.fields['ADDR_MOO']}
                            placeholder="กรอกหมู่"
                          />
                          <div className="errorMsg"> {this.state.errors['ADDR_MOO']}</div>
                        </div>
                        <div className="form-group">
                          <label>
                            <span className="font-header-label pl-2">หมู่บ้าน/อาคาร</span>
                          </label>
                          <input 
                            type="text" 
                            className="form-control input_form" 
                            id="exampleFormControlInput1" 
                            name="ADDR_VILLEGE" 
                            onChange={(e) => this.handleChange(e)} 
                            value={this.state.fields['ADDR_VILLEGE']} 
                            placeholder="กรุณากรอกหมู่บ้าน/อาคาร"
                          />
                        </div>
                        <div className="form-group"> 
                          <label>
                            <span className="font-header-label pl-2">ตรอก/ซอย</span>
                          </label>
                          <input 
                            type="text" 
                            className="form-control input_form" 
                            name="ADDR_SOI" 
                            onChange={(e) => this.handleChange(e)} 
                            value={this.state.fields.ADDR_SOI} 
                            id="exampleFormControlInput1" 
                            placeholder="กรุณากรอกตรอก/ซอย"
                          />
                        </div>
                        <div className="form-group">
                          <span className="font-header-label pl-2">ถนน</span>
                          <input 
                            type="text" 
                            className="form-control input_form" 
                            name="ADDR_STREET" 
                            onChange={(e) => this.handleChange(e)} 
                            value={this.state.fields.ADDR_STREET} 
                            id="exampleFormControlInput1" 
                            placeholder="กรุณากรอกถนน"
                          />
                        </div>
                        <div className="form-group">
                          <label>
                            <span className="label-validate">* </span>
                            <span className="font-header-label pl-2">จังหวัด</span> 
                          </label>
                          {this.renderContentProvince('type_province')}
                        </div>
                        <div className="form-group">
                          <label>
                            <span className="label-validate">* </span>
                            <span className="font-header-label pl-2">อำเภอ</span> 
                          </label>
                          <select
                            className='form-control'
                            id='exampleFormControlSelect1'
                            name="ADDR_ORG_AMPHURE_ID"
                            onChange={(e) => this.getTumbon(e.target.value, 'onchange')}
                            value={this.state.fields['ADDR_ORG_AMPHURE_ID'] == 0 ? this.state.fields['ADDR_ORG_AMPHURE_ID'] = '' : this.state.fields['ADDR_ORG_AMPHURE_ID']}
                          >
                            <option value="0"> กรุณาเลือกอำเภอ</option>
                            {amphureListData}
                          </select>
                          <div className="errorMsg">{this.state.errors['ADDR_ORG_AMPHURE_ID']} </div>
                        </div>
                        <div className="form-group">
                          <label>
                            <span className="label-validate">* </span>
                            <span className="font-header-label pl-2">ตำบล</span> 
                          </label>
                          <select
                            className={`form-control ${this.state.errorsFocus['ADDR_ORG_SUB_DISTRICT_ID']}`}
                            id='exampleFormControlSelect1'
                            name="ADDR_ORG_SUB_DISTRICT_ID"
                            onChange={(e) => this.getPostcode(e.target.value, 'onchange')}
                            value={this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] == 0 ? this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] = '' : this.state.fields['ADDR_ORG_SUB_DISTRICT_ID']}
                          >
                          <option value="0">กรุณาเลือกตำบล</option>
                          {tumbonListData}
                        </select>
                        <div className="errorMsg">{this.state.errors['ADDR_ORG_SUB_DISTRICT_ID']}</div>
                      </div>
                      <div className="form-group">
                        <label>
                          <span className="label-validate">* </span>
                          <span className="font-header-label pl-2">รหัสไปรษณีย์</span> 
                        </label>
                        <select 
                          className='form-control' 
                          id={`exampleFormControlSelect1 ${this.state.errorsFocus['ADDR_ORG_POSTALCODE_ID']}`} 
                          name="ADDR_ORG_POSTALCODE_ID" 
                          onChange={(e) => this.handleChange(e)}
                          value={this.state.fields['ADDR_ORG_POSTALCODE_ID'] == 0 ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] = '' : (this.state.fields['ADDR_ORG_POSTALCODE_ID'] !== '' ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] : '')}
                        >
                          <option value="0">กรุณาเลือกรหัสไปรษณีย์</option> 
                          {postcodeListData}
                        </select>
                        <div className="errorMsg"> {this.state.errors['ADDR_ORG_POSTALCODE_ID']} </div>
                      </div>
                      <div className="form-group">
                        <label>ข้อมูลที่อยู่ (เพิ่มเติม )</label>
                        <textarea 
                            className="form-control input_form" 
                            id="exampleFormControlTextarea1" 
                            rows="3" 
                            name="ADDR_OTHER" 
                            value={this.state.fields.ADDR_OTHER} 
                            onChange={(e) => this.handleChange(e)} 
                            placeholder="กรุณากรอกข้อมูลเพิ่มเติม"
                        >
                        </textarea>
                      </div>
                      <div className="header-profile mt-5">ข้อมูลเพิ่มเติม (รูปภาพ)</div>
                      <div className="errorImageSize">{this.state.errorFileSize}</div>
                         <div className="row row_image">
                          <div className="col-4 col-sm-3 review_image_before">
                            <div className="upload_preview" disabled={false}>
                              <div 
                                className="close-btn" 
                                onClick={() => this.deleteLocalStorage('imgPreview1')} 
                                style={{ display: this.state.closeBtn['imgPreview1'] }}
                              >
                                <i className="fa fa-times-circle" aria-hidden="true"></i>
                              </div>
                              <div onClick={() => this.handleUploadFile(1)}>
                                <input type="file" name="imgPreview1" style={{ display: "none" }} ref={input => this.inputElement_1 = input} onChange={(e) => this.handleLoadAvatar(e)} />
                                <img src={this.state.imgPreview['imgPreview1']} alt="" className={this.state.classPreview['imgPreview1']} />
                              </div>
                            </div>
                          </div>
                          <div className="col-4 col-sm-3 review_image_before">
                            <div className="upload_preview">
                              <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview2')} style={{ display: this.state.closeBtn['imgPreview2'] }}><i className="fa fa-times-circle" aria-hidden="true"></i></div>
                                <div onClick={() => this.handleUploadFile(2)}>
                                  <input type="file" name="imgPreview2" style={{ display: "none" }} ref={input => this.inputElement_2 = input} onChange={(e) => this.handleLoadAvatar(e)} disabled={disabledimage}/>
                                  <img src={this.state.imgPreview['imgPreview2']} alt="" className={this.state.classPreview['imgPreview2']} />
                                </div>
                            </div>
                          </div>
                          <div className="col-4 col-sm-3 review_image_before">
                            <div className="upload_preview">
                              <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview3')} style={{ display: this.state.closeBtn['imgPreview3'] }}><i className="fa fa-times-circle" aria-hidden="true"></i></div>
                                <div onClick={() => this.handleUploadFile(3)}>
                                  <input type="file" name="imgPreview3" style={{ display: "none" }} ref={input => this.inputElement_3 = input} onChange={(e) => this.handleLoadAvatar(e)} disabled={disabledimage2}/>
                                  <img src={this.state.imgPreview['imgPreview3']} alt="" className={this.state.classPreview['imgPreview3']} />
                                </div>
                              </div>
                            </div>
                          </div>
                          <label className="check_conditions my-3">
                            ต้องการให้ข้อมูลเพิ่มเติม เพื่อสิทธิประโยชน์ที่ตรงกับความต้องการของท่าน
                            <input type="checkbox" onChange={(e) => this.changeHandle(e)} name="aggreement" disabled={this.state.disableAgreement}/>
                            <span className="checkmark_condition"></span>
                          </label>
                          <div className="container-fluid btn btn-secondary btn-block btn_agreement" style={{display:this.state.submitFormNoadditional}}>
                            <button onClick={() => this.submitFormMb()} className="send-font">ส่งข้อมูลเพื่อสมัครสมาชิก</button>
                          </div>
                        </div>
                      </div>

                      <div className="row" style={{display:this.state.additionnal}}>
                        <div className="col-12 content_page">
                          <div className="form-group">
                            <label className="addtional"> ประเภทรถ </label>
                            <select 
                              className={`form-control ${this.state.errorsFocus['CAR_TYPE']}`}
                              name="CAR_TYPE" 
                              id='exampleFormControlSelect1'
                              value={this.state.fields['CAR_TYPE']}
                              onChange={(e) => this.dropdownlistCheck(e)}>
                              <option value="0">กรุณาเลือกประเภทรถ</option>
                              {this.renderAdditional('type_cartype')}
                            </select>
                              <div className="errorMsg CAR_TYPE">{this.state.errors["CAR_TYPE"]}</div>
                          </div>
                          <div className="form-group">
                            <label className="addtional">ยี่ห้อรถ</label>
                              <select 
                                className={`form-control ${this.state.errorsFocus['CAR_BRAND']}`}
                                name="CAR_BRAND" 
                                value={this.state.fields['CAR_BRAND']}
                                id='exampleFormControlSelect1' 
                                onChange={(e) => this.dropdownlistCheck(e)}
                              >
                                <option value="0">กรุณาเลือกยี่ห้อรถ</option>
                                {this.renderAdditional('type_brand')}
                              </select>
                              <div className="errorMsg CAR_BRAND">{this.state.errors['CAR_BRAND']}</div>
                              <div className="form-group" style={{ display: this.state.CAR_BRAND_REMARK}}>
                                <input
                                  type="text"
                                  className={`form-control ${this.state.errorsFocus['CAR_BRAND_REMARK']} input_form`}
                                  name="CAR_BRAND_REMARK"
                                  value={this.state.fields['CAR_BRAND_REMARK']}
                                  onChange={e => this.dropdownlistCheck(e)}
                                  id="exampleFormControlInput1"
                                  placeholder="หมายเหตุประเภทรถยนต์"
                                />
                                <div className="errorMsg CAR_BRAND_REMARK">{this.state.errors['CAR_BRAND_REMARK']} </div>
                              </div>
                            </div>
                             <div className="form-group">
                                <label className="addtional">อาชีพ</label>
                                      <select className={`form-control ${this.state.errorsFocus['OCCUPATION_ID']}`}
                                        name="OCCUPATION_ID" 
                                        value={this.state.fields['OCCUPATION_ID']}
                                        id='exampleFormControlSelect1' 
                                        onChange={(e) => this.dropdownlistCheck(e)}>
                                        <option value="0" >
                                          กรุณาเลือกอาชีพ
                                        </option>
                                        {this.renderAdditional('type_occupation')}
                                      </select>
                                      <div className="errorMsg OCCUPATION_ID">
                                        {this.state.errors['OCCUPATION_ID']}
                                      </div>
                                      <div className="form-group" style={{ display: this.state.OCCAPATION_REMARK}}>
                                        <input
                                          type="text"
                                          className={`form-control ${this.state.errorsFocus['OCCAPATION_REMARK']} input_form`}
                                          name="OCCAPATION_REMARK"
                                          value={this.state.fields['OCCAPATION_REMARK']}
                                          onChange={e => this.dropdownlistCheck(e)}
                                          id="exampleFormControlInput1"
                                          placeholder="โปรดระบุรายละเอียดอาชีพอื่นๆ"
                                        />
                                        <div className="errorMsg OCCAPATION_REMARK">
                                          {this.state.errors['OCCAPATION_REMARK']}
                                        </div> 
                                      </div>
                                    </div>
                                    <div className="form-group">
                                      <label className="addtional">การศึกษา</label>
                                      <select className={`form-control ${this.state.errorsFocus['EDUCATION_ID']}`}
                                        ame="EDUCATION_ID" 
                                        value={this.state.fields['EDUCATION_ID']}
                                        id='exampleFormControlSelect1' 
                                        onChange={(e) => this.dropdownlistCheck(e)}>
                                        <option value="0">กรุณาเลือกระดับการศึกษา</option>
                                        {this.renderAdditional('type_education')}
                                      </select>
                                      <div className="errorMsg EDUCATION_ID">
                                        {this.state.errors["EDUCATION_ID"]}
                                      </div>
                                    </div>
                                    <div className="form-group">
                                      <label className="addtional">รายได้ต่อเดือน</label>
                                      <select  className={`form-control ${this.state.errorsFocus['INCOME_ID']}`}
                                          name="INCOME_ID" 
                                          value={this.state.fields['INCOME_ID']}
                                          id='exampleFormControlSelect1' 
                                          onChange={(e) => this.dropdownlistCheck(e)}>
                                        <option value="0">กรุณาเลือกรายได้ต่อเดือน</option>
                                        {this.renderAdditional('type_income')}
                                      </select>
                                      <div className="errorMsg INCOME_ID">
                                        {this.state.errors["INCOME_ID"]}
                                      </div>
                                    </div>
                                    <label className="recommend">ท่านยินดีรับข่าวสาร/โปรโมชั่นจากทางใด(เลือกได้มากกว่า 1 ข้อ)</label>
                                    <div className="row">
                                      <div className="col">
                                        <label className="check_conditions_select">จดหมาย
                                          <input type="checkbox" id="SUBSCRIBE_LETTER" name="news"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_LETTER']) ? (this.state.fields['SUBSCRIBE_LETTER'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                                          <span className="checkmark_condition_select"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="check_conditions_select">อีเมล
                                          <input type="checkbox" id="SUBSCRIBE_EMAIL" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_EMAIL']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                                          <span className="checkmark_condition_select"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="check_conditions_select">ข้อความทางโทรศัพท์มือถือ (SMS)
                                          <input type="checkbox" id="SUBSCRIBE_SMS" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_SMS'] === true ? true : false )} />
                                          <span className="checkmark_condition_select"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="check_conditions_select">ไม่ประสงค์รับข่าวสาร
                                          <input type="checkbox" id="SUBSCRIBE_NONE" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_NONE']) ? (this.state.fields['SUBSCRIBE_NONE'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false } />
                                          <span className="checkmark_condition_select"></span>
                                        </label>
                                      </div>
                                    </div>
                                    <label className="addtional">โปรโมชั่นที่น่าสนใจ</label>
                                    <div className="row">
                                      <div className="col">
                                        <label className="selectoneonly">อาหาร
                                          <input type="checkbox" id="PMT_FOOD" checked={this.state.fields['PMT_FOOD']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="selectoneonly">ท่องเที่ยว
                                          <input type="checkbox" id="PMT_TRAVEL" checked={this.state.fields['PMT_TRAVEL']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">ช้อปปิ้ง
                                          <input type="checkbox" id="PMT_SHOPPING" checked={this.state.fields['PMT_SHOPPING']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="selectoneonly">ไลฟ์สไตล์และความงาม
                                          <input type="checkbox" id="PMT_BEAUTY" checked={this.state.fields['PMT_BEAUTY']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">กีฬาและรถยนต์
                                          <input type="checkbox" id="PMT_SPORT_AND_CAR" checked={this.state.fields['PMT_SPORT_AND_CAR']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">อื่นๆโปรดระบุ
                                          <input type="checkbox" id="PMT_OTHERS" checked={this.state.fields['PMT_OTHERS']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                        <div className="form-group">
                                          <input 
                                            type="text" 
                                            id="PMT_OTHERS_REMARK" 
                                            value={this.state.fields['PMT_OTHERS_REMARK']} 
                                            // className={disabled_block} 
                                            className="form-control input_form disabled" 
                                            placeholder="อื่นๆโปรดระบุข้อมูล"
                                            value={this.state.fields['PMT_OTHERS_REMARK']}  
                                            onChange={(e) => this.handleGameClik(e)} 
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <label className="addtional">กิจกรรมที่ท่านสนใจ</label>
                                    <div><label>เอ็นเตอร์เทนเมนท์</label></div>
                                    <div className="row">
                                      <div className="col">
                                        <label className="selectoneonly">ฟังเพลง
                                          <input type="checkbox" id="ACT_EN_MUSIC" checked={this.state.fields['ACT_EN_MUSIC']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="selectoneonly">คอนเสิร์ต
                                          <input type="checkbox" id="ACT_EN_CONCERT" checked={this.state.fields['ACT_EN_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">คอนเสิร์ตลูกทุ่ง
                                          <input type="checkbox" id="ACT_EN_COUNTRY_CONCERT" checked={this.state.fields['ACT_EN_COUNTRY_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="selectoneonly">ภาพยนตร์
                                          <input type="checkbox" id="ACT_EN_MOVIES" checked={this.state.fields['ACT_EN_MOVIES']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">ละครเวที
                                          <input type="checkbox" id="ACT_EN_THEATER" checked={this.state.fields['ACT_EN_THEATER']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">อื่นๆโปรดระบุ
                                          <input type="checkbox" id="ACT_EN_OTHERS" checked={this.state.fields['ACT_EN_OTHERS']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                        <div className="form-group">
                                          <input 
                                            type="text" 
                                            id="ACT_EN_OTHERS_REMARK" 
                                            className={entertain_remark_display} 
                                            placeholder="อื่นๆโปรดระบุข้อมูล"
                                            value={this.state.fields['ACT_EN_OTHERS_REMARK']}  
                                            onChange={(e) => this.handleGameClick(e)} 
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-12 content_page">
                                    <label>ด้านกีฬา</label>
                                    <div className="row">
                                      <div className="col">
                                        <label className="selectoneonly">ฟิตเนส
                                          <input type="checkbox" id="ACT_SPT_FITNESS" checked={this.state.fields['ACT_SPT_FITNESS']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="selectoneonly">วิ่ง
                                          <input type="checkbox" id="ACT_SPT_RUN" checked={this.state.fields['ACT_SPT_RUN']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">ปั่นจักรยาน
                                          <input type="checkbox" id="ACT_SPT_CYCLING" checked={this.state.fields['ACT_SPT_CYCLING']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="selectoneonly">ฟุตบอล
                                          <input type="checkbox" id="ACT_SPT_FOOTBALL" checked={this.state.fields['ACT_SPT_FOOTBALL']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">อื่นๆโปรดระบุ
                                          <input type="checkbox" id="ACT_SPT_OTHERS" checked={this.state.fields['ACT_SPT_OTHERS']} name="sport"  onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                        <div className="form-group">
                                          <input 
                                            type="text" 
                                            id="ACT_SPT_OTHERS_REMARK" 
                                            className={sport_remark_display} 
                                            placeholder="อื่นๆโปรดระบุข้อมูล"
                                            value={this.state.fields['ACT_SPT_OTHERS_REMARK']}  
                                            onChange={(e) => this.handleGameClick(e)} 
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <label className="addtional">ด้านกิจกรรมนอกบ้าน</label>
                                    <div className="row">
                                      <div className="col">
                                        <label className="selectoneonly">ช้อปปิ้ง
                                          <input type="checkbox" id="ACT_OD_SHOPPING" checked={this.state.fields['ACT_OD_SHOPPING']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="selectoneonly">ความงาม
                                          <input type="checkbox" id="ACT_OD_BEAUTY" checked={this.state.fields['ACT_OD_BEAUTY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">ร้านอาหาร
                                          <input type="checkbox" id="ACT_OD_RESTAURANT" checked={this.state.fields['ACT_OD_RESTAURANT']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                        <label className="selectoneonly">สถานบันเทิง
                                          <input type="checkbox" id="ACT_OD_CLUB" checked={this.state.fields['ACT_OD_CLUB']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="w-100"></div>
                                      <div className="col">
                                        <label className="selectoneonly">กิจกรรมแม่และเด็ก
                                          <input type="checkbox" id="ACT_OD_MOMCHILD" checked={this.state.fields['ACT_OD_MOMCHILD']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                          <span className="checkmark_selectoneonly"></span>
                                        </label>
                                      </div>
                                      <div className="col">
                                      <label className="selectoneonly">แรลลี่
                                        <input type="checkbox" id="ACT_OD_RALLY" checked={this.state.fields['ACT_OD_RALLY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                        <span className="checkmark_selectoneonly"></span>
                                      </label>
                                    </div>
                                    <div className="w-100"></div>
                                    <div className="col">
                                      <label className="selectoneonly">การผจญภัย
                                        <input type="checkbox" id="ACT_OD_ADVANTURE" checked={this.state.fields['ACT_OD_ADVANTURE']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                        <span className="checkmark_selectoneonly"></span>
                                      </label>
                                    </div>
                                    <div className="col">
                                      <label className="selectoneonly">ถ่ายภาพ
                                        <input type="checkbox" id="ACT_OD_PHOTO" checked={this.state.fields['ACT_OD_PHOTO']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                        <span className="checkmark_selectoneonly"></span>
                                      </label>
                                    </div>
                                    <div className="w-100"></div>
                                    <div className="col">
                                      <label className="selectoneonly">ไหว้พระและการทำบุญ
                                        <input type="checkbox" id="ACT_OD_PHILANTHROPY" checked={this.state.fields['ACT_OD_PHILANTHROPY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                        <span className="checkmark_selectoneonly"></span>
                                      </label>
                                    </div>
                                    <div className="col">
                                      <label className="selectoneonly">ดูดวงและฮวงจุ้ย
                                        <input type="checkbox" id="ACT_OD_HOROSCOPE" checked={this.state.fields['ACT_OD_HOROSCOPE']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                        <span className="checkmark_selectoneonly"></span>
                                      </label>
                                    </div>
                                    <div className="w-100"></div>
                                    <div className="col">
                                      <label className="selectoneonly">ท่องเที่ยวต่างประเทศ
                                      <input type="checkbox" id="ACT_OD_ABROAD" checked={this.state.fields['ACT_OD_ABROAD']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                      <span className="checkmark_selectoneonly"></span>
                                    </label>
                                  </div>
                                  <div className="col">
                                    <label className="selectoneonly">ท่องเที่ยวในประเทศ
                                      <input type="checkbox" id="ACT_OD_UPCOUNTRY" checked={this.state.fields['ACT_OD_UPCOUNTRY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                      <span className="checkmark_selectoneonly"></span>
                                    </label>
                                  </div>
                                  <div className="w-100"></div>
                                  <div className="col">
                                    <label className="selectoneonly">อื่นๆโปรดระบุ
                                      <input type="checkbox" id="ACT_OD_OTHERS" checked={this.state.fields['ACT_OD_OTHERS']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                      <span className="checkmark_selectoneonly"></span>
                                    </label>
                                    <div className="form-group">
                                      <input type="text" id="ACT_OD_OTHERS_REMARK" className={outside_remark_display} 
                                      placeholder="อื่นๆโปรดระบุข้อมูล" value={this.state.fields['ACT_OD_OTHERS_REMARK']}  onChange={(e) => this.handleGameClick(e)} />
                                    </div>
                                  </div>
                                </div>
                              <label className="addtional">ด้านกิจกรรมในบ้าน</label>
                              <div className="row">
                                <div className="col">
                                  <label className="selectoneonly">ทำอาหาร
                                    <input type="checkbox"  id="ACT_ID_COOKING" checked={this.state.fields['ACT_ID_COOKING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                    <span className="checkmark_selectoneonly"></span>
                                  </label>
                                </div>
                                <div className="col">
                                  <label className="selectoneonly">ทำขนม
                                  <input type="checkbox"  id="ACT_ID_BAKING" checked={this.state.fields['ACT_ID_BAKING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">ตกแต่งบ้าน
                                  <input type="checkbox"  id="ACT_ID_DECORATE" checked={this.state.fields['ACT_ID_DECORATE']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">ทำสวน
                                  <input type="checkbox"  id="ACT_ID_GARDENING" checked={this.state.fields['ACT_ID_GARDENING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อ่านหนังสือ
                                  <input type="checkbox"  id="ACT_ID_READING" checked={this.state.fields['ACT_ID_READING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="selectoneonly">เล่นเกมส์
                                  <input type="checkbox"  id="ACT_ID_GAMING" checked={this.state.fields['ACT_ID_GAMING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อินเทอร์เน็ต
                                  <input type="checkbox"  id="ACT_ID_INTERNET" checked={this.state.fields['ACT_ID_INTERNET']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                              </div>
                              <div className="w-100"></div>
                              <div className="col">
                                <label className="selectoneonly">อื่นๆโปรดระบุ
                                  <input type="checkbox"  id="ACT_ID_OTHERS" checked={this.state.fields['ACT_ID_OTHERS']} name="inside" value="inside_other" onClick = {(e) => this.handleGameClick(e)} />
                                  <span className="checkmark_selectoneonly"></span>
                                </label>
                                <div className="form-group">
                                  <input 
                                    type="text" 
                                    id="ACT_ID_OTHERS_REMARK" 
                                    className={inside_remark_display} 
                                    placeholder="อื่นๆโปรดระบุข้อมูล" 
                                    value={this.state.fields['ACT_ID_OTHERS_REMARK']}  
                                    onChange={(e) => this.handleGameClick(e)} 
                                  />
                                </div>
                              </div>
                            </div>
                            <label className="addtional">ลักษณะกิจกรรมที่ท่านสนใจ</label>
                            <div className="row">
                              <div className="col">
                                <label className="check_conditions_select">กิจกรรมสำหรับเพื่อนฝูง
                                  <input type="checkbox"  id="ACT_HNGOUT_FRIEND" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FRIEND']) ? (this.state.fields['ACT_HNGOUT_FRIEND'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false}  />
                                  <span className="checkmark_condition_select"></span>
                                </label>
                              </div>
                              <div className="col">
                                <label className="check_conditions_select">กิจกรรมสำหรับคู่รัก
                                <input type="checkbox"  id="ACT_HNGOUT_COUPLE" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_COUPLE']) ? (this.state.fields['ACT_HNGOUT_COUPLE'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                                <span className="checkmark_condition_select"></span>
                              </label>
                            </div>
                            <div className="w-100"></div>
                            <div className="col">
                              <label className="check_conditions_select">กิจกรรมสำหรับครอบครัว
                                <input type="checkbox"  id="ACT_HNGOUT_FAMILY" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FAMILY']) ? (this.state.fields['ACT_HNGOUT_FAMILY'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                                <span className="checkmark_condition_select"></span>
                              </label>
                            </div>
                            <div className="col">
                              <label className="check_conditions_select">ไม่สนใจทำกิจกรรม
                                <input type="checkbox"  id="ACT_HNGOUT_NO" name="activities"   onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_NO'] === true ? true : false)} />
                                <span className="checkmark_condition_select"></span>
                              </label>
                            </div>
                          </div>
                          <div className="">
                            <button 
                              type="button" 
                              className={`btn btn-secondary btn-block btn_agreement ${this.state.disabledBtn}`} 
                              onClick={() => this.submitFormMb()}>{(this.state.disabledBtn === 'disabled' ? <i className="fa fa-circle-o-notch fa-spin" style={{ marginRight: "10px",marginTop: "5px" }}/> : '')}
                              ส่งข้อมูลเพื่อสมัครสมาชิก
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  {this.state.modal}
                </div>
              </div>
            </div>
    )
  }
}


