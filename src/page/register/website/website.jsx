import React from 'react';
import '../../register/register.css';
import { registerAction } from '../../../_actions/registerActions'; 
import SweetAlert from "react-bootstrap-sweetalert"
const publicIp = require('public-ip');
export class Regisweb extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            errors: {},
            errorsFocus:{},
            setActive: "active",
            fields : {},
            show:false,
            modal:null
        };
    }

    componentDidMount(){
        (async () => {
            this.setState({ ipaddress : await publicIp.v4() })
            console.log(await publicIp.v4());
        })();
    }

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    handleChange(e,type){
        let { fields , errors , errorsFocus } = this.state;
        if(e.target.name == "MAIL"){
            fields['MAIL'] = e.target.value
        }
        if(e.target.name == "TEL"){
            const re = /^[0-9\b]+$/;
            if(e.target.value == "" || re.test(e.target.value)){
                fields[e.target.name] = e.target.value;
                errors[e.target.name] = null;
            }else{
                errors[e.target.name] = "กรอกตัวเลข";
                errorsFocus[e.target.name] = ''
                e.preventDefault();
            }
        }else{
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
        }
        this.setState({ 
            setValue : e.target.value,
            setType : type ,
            errors, 
            fields
        })
    }

    btnActive(){
        this.setState({ setActive: ""})
    }

    submitData(value,type,lang){
        if(this.validateForm(type)){
            console.log(true)
            this.checkPhoneorEmail(value,type,lang);
        }else{
            console.log(false)
        }
    }

    validateForm(type){
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(type === 0){
            if(!fields['EMAIL']){
                formIsValid = false;
                errors["EMAIL"] = 'กรุณากรอกข้อมูล'
                errorsFocus["EMAIL"] = 'errorFocus'
            }else{
                var validEmail = this.validateEmail(fields["EMAIL"]);
                if (validEmail === false) {
                  formIsValid = false;
                  errorsFocus["EMAIL"] = 'errorFocus'
                  errors["EMAIL"] = 'รูปแบบอีเมล์ไม่ถูกต้อง'
                }
            }
        }else{
            if(!fields['TEL']){
                formIsValid = false;
                errors["TEL"] = 'กรุณากรอกข้อมูล'
                errorsFocus["TEL"] = 'errorFocus'
            }
        }
        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
          });
      
          return formIsValid;
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    checkPhoneorEmail(value,type,lang){
        registerAction.checkEmailorPhone(type,value).then(e => {
            console.log(e)
            if(e.data.isSuccess === true && type === 1){
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/register/otp`,
                    state: {
                        data: value,
                        socialid : '',
                        socialType: '',
                        udId:this.state.ipaddress,
                        playerId : 'WEBSITE',
                        email:'',
                        phone:'',
                    }
                })
            }else if(e.data.isSuccess === true && type === 0){
                this.props.history.push({
                    pathname:`${process.env.PUBLIC_URL}/register/web`,
                    state:{
                        type:0,
                        data:value,
                        socialid : '',
                        socialType: '',
                        udId:this.state.ipaddress,
                        playerId : 'WEBSITE',
                        email:'',
                        phone:'',
                    }
                })
            }else{
                if(lang === 'th'){
                    this.modalAlert(e.data.errMsg);
                }else{
                    this.modalAlert(e.data.errMsg_en);
                }
            }
        })
    }

    modalAlert(msg){
        alert = (
            <SweetAlert
                custom
                showCloseButton
                closeOnClickOutside={false}
                focusConfirmBtn={false}
                title=""
                showConfirm={false}
                showCancelButton
                onCancel={() => this.handleChoice(false)}
                onConfirm={() => this.handleChoice(true)}

            >
                <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i>
                    <img style={{width:"25%"}} src={`${process.env.PUBLIC_URL}/images/cancel.png`} />
                    <div className="my-3 alert-dismiss">
                        {msg}
                    </div>
                </div>
            </SweetAlert>
        )
        this.setState({ show : true , modal : alert })
    }

    handleChoice(choice) {
        if(choice === false){
            this.setState({ show: false , modal: null })
        }else{
            this.setState({ show:true , modal:alert })
        }
    }

    render(){
        return(
            <div>
                <div className="bg-body py-5">
                    <div className="content-white">
                        <div className="container-fluid">
                            <ul className="nav nav-box text-center">
                                    <li className={this.state.setActive}>
                                        <button 
                                            data-toggle="tab" 
                                            href="#tel"
                                        >
                                            เบอร์โทรศัพท์
                                        </button>
                                    </li>
                                    <li className={this.state.mailActive}>
                                        <button 
                                            data-toggle="tab" 
                                            href="#mail"
                                            onClick={() => this.btnActive()}
                                        >
                                            อีเมล
                                        </button>
                                    </li>
                            </ul>
                            <div className="tab-content clear-border clearfix text-center" style={{marginTop:'15px'}}>
                                <div id="tel" className={`tab-pane ${this.state.setActive}`}>
                                    <div className="header-title">เบอร์โทรศัพท์</div>
                                    <input 
                                        maxLength="10"
                                        type="number"
                                        name="TEL"
                                        id="phoneno"
                                        onInput={this.maxLengthCheck}
                                        onChange={(e) => this.handleChange(e,1)}
                                        className={`inputTel ${this.state.errorsFocus['TEL']}`}
                                        value={this.state.fields['TEL']}
                                    />
                                    <div className="errorMsg TEL">{this.state.errors["TEL"]}</div>
                                </div>
                    
                                <div id="mail" className="tab-pane">
                                    <div className="header-title">อีเมล</div>
                                    <input 
                                        type="email"
                                        name="EMAIL"
                                        onChange={(e) => this.handleChange(e,0)}
                                        pattern=".+@globex.com" 
                                        size="30" 
                                        required
                                        className={`inputTel ${this.state.errorsFocus['EMAIL']}`}
                                        value={this.state.fields['EMAIL']}
                                    />
                                    <div className="errorMsg EMAIL">{this.state.errors["EMAIL"]}</div>
                                </div>
                                <div className="mt-5 text-center">
                                    <button className="btnNext" onClick={() => this.submitData(this.state.setValue,this.state.setType,this.state.language)}>ดำเนินการต่อ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {this.state.modal}
        </div>
        )
    }
}

