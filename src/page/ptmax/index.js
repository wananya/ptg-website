export * from '../ptmax/problem';
export * from '../ptmax/card';
export * from '../ptmax/history';
export * from '../ptmax/editprofile';
export * from '../ptmax/register';
export * from '../ptmax/register/success';