import React from 'react';
import '../../register/register.css';
import moment from "moment-timezone";
import 'moment/locale/th';
import DatePicker from 'react-datepicker';
import { registerAction } from '../../../_actions/registerActions'; 

const monthMap = {
    '1': '01',
    '2': '02',
    '3': '03',
    '4': '04',
    '5': '05',
    '6': '06',
    '7': '07',
    '8': '08',
    '9': '09',
    '10': '10',
    '11': '11',
    '12': '12',
  };
  
  const dateConfig = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: 'YYYY'
    },
  }
  
  const dateConfigTH = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: value => moment(value).add(543,'y').format('YYYY')
    },
  }

export class Edit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            errors: {},
            errorsFocus:{},
            setActive: "active",
            fields : {
                PHONE_NO:'',
                EMAIL:'',
                PASSWORD:'',
                CHECKPASSWORD:'',
                FIRSTNAME:'',
                LASTNAME:'',
                BIRTHDATE:'',
                GENDER:''
            },
            show:false,
            modal:null,
            fields:{},
            time: new Date(),
            isOpenDatepickerMobile: false,
            errors: {},
            errorsFocus: {},
            email:'',
            dp:false
        };
        // this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        this.getCustomerprofile();
    }

    getCustomerprofile(){
        let { fields } = this.state;
        var userid = localStorage.getItem('uid');
        var tkmb = localStorage.getItem('tkmb');
        registerAction.getUserById(userid,tkmb).then(e => {
            console.log(e)
            if(e.data.isSuccess === true){
                fields = e.data.data
                this.setState({ fields })
            }else{

            }
        })
    }

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    handChange(e){
        let { errors,errorsFocus, fields } = this.state;
        if (e.target.name === "PHONE_NO") {
          const re = /^[0-9\b]+$/;
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            errors[e.target.name] = 'กรุณากรอกเฉพาะตัวเลข'
            errorsFocus[e.target.name] = ''
            e.preventDefault();
          }
        } else {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        this.setState({ errors, fields , errorsFocus });
    }

    async handleSubmit(event) {
        console.log(event)
        var { fields } = this.state;
        event.preventDefault();
        if (await this.validateForm()) {
            this.props.history.push({
                pathname: `${process.env.PUBLIC_URL}/edit/otp`,
                state: {
                    data: this.state.fields['phoneNo'],
                    firstname : this.state.fields['firstName'],
                    lastname : this.state.fields['lastName'],
                    birthday : this.state.fields['birthDay'],
                    gender : this.state.fields['gender']
                }
            })
        } else {
          console.log('formsubmit ' + false);
        }
        this.setState({ fields })
      }
    
      async validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(!fields["phoneNo"]){
            formIsValid = false;
            errorsFocus["phoneNo"] = 'errorFocus'
            errors["phoneNo"] = 'กรุณากรอกเบอร์โทรศัพท์'
        }
        if(!fields["email"]){
            formIsValid = false;
            errorsFocus["email"] = 'errorFocus'
            errors["email"] = 'กรุณากรอกอีเมล'
        }
        if(!fields["firstName"]){
            formIsValid = false;
            errorsFocus["firstName"] = 'errorFocus'
            errors["firstName"] = 'กรุณากรอกชื่อ'
        }
        if(!fields["lastName"]){
            formIsValid = false;
            errorsFocus["lastName"] = 'errorFocus'
            errors["lastName"] = 'กรุณากรอกนามสกุล'
        }
        if(!fields["birthDay"]){
            formIsValid = false;
            errorsFocus["birthDay"] = 'errorFocus'
            errors["birthDay"] = 'กรุณาเลือกวัน/เดือน/ปีเกิด'
        }else{
            var dateBeforDiff = moment(fields["birthDay"]).format("YYYY-MM-DD");
            var diff = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "years");
            var diffDay = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "days");
            if (diff < 7 || diffDay <= 2556) {
                formIsValid = false;
                errorsFocus["birthDay"] = 'errorFocus'
                errors["birthDay"] = 'อายุผู้สมัครยังไม่ถึง 7 ปีบริบูรณ์'
            }
        }
        if(!fields["gender"]){
            formIsValid = false;
            errorsFocus["gender"] = 'errorFocus'
            errors["gender"] = 'กรุณาเลือกเพศ'
        }
        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });

        return formIsValid;
      }


    render(){
        var yearnow = new Date().getFullYear()
        var start = yearnow - parseInt(100);
        var setYear = [];
        var selectTime = new Date()
        var minDate = new Date();
        minDate.setFullYear(minDate.getFullYear() - 100);
        var date_Config = dateConfigTH;
        return(
            <div>
                <div className="bg-body-regis py-3">
                    <div className="">
                        <div className="container-fluid">
                            <form onSubmit={e => this.handleSubmit(e)}>
                                <div>
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">เบอร์โทรศัพท์</span>
                                    </label>
                                    <div>
                                        <div className="form-control disabled" disabled>
                                            {this.state.fields['phoneNo']}
                                        </div>
                                        <input 
                                            maxLength="10"
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['phoneNo']}`}
                                            onInput={this.maxLengthCheck}
                                            placeholder="กรอกเบอร์โทรศัพท์"
                                            type="hidden"
                                            name="phoneNo"
                                            value={this.state.fields['phoneNo']}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg phoneNo">{this.state.errors["phoneNo"]}</div>
                                </div>

                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">อีเมล</span>
                                    </label>
                                    <div>
                                        <div className="form-control disabled" disabled>
                                            {(this.state.fields['email'])}
                                        </div>
                                        <input 
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['email']}`}
                                            placeholder="กรอกอีเมล"
                                            name="email"
                                            type="hidden"
                                            disabled={this.state.dp}
                                            value={(this.state.fields['email'])}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg EMAIL">{this.state.errors["EMAIL"]}</div>
                                </div>
                                <div className="header-label">ข้อมูลผู้ใช้งาน</div>
                                <div className="label-mt">
                                    <label className="font-header-label"><span className="label-validate">*</span> ชื่อ</label>
                                    <div>
                                        <input 
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['firstName']}`}
                                            placeholder="กรอกชื่อ"
                                            name="firstName"
                                            value={this.state.fields['firstName']}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg firstName">{this.state.errors["firstName"]}</div>
                                </div>
                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">นามสกุล</span>
                                    </label>
                                    <div>
                                        <input 
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['lastName']}`}
                                            placeholder="กรอกนามสกุล"
                                            name="lastName"
                                            value={this.state.fields['lastName']}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg LASTNAME">{this.state.errors["lastName"]}</div>
                                </div>
                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">วัน/เดือน/ปีเกิด</span>
                                    </label>
                                    <div>
                                        <input
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['birthDay']}`}
                                            placeholder="กรุณาเลือกวัน เดือน ปีเกิด"
                                            type="date" 
                                            id="date"
                                            name="birthDay"
                                            value={this.state.fields['birthDay']}
                                            selected={this.state.startDate}
                                            onChange={(e) => this.handChange(e)}
                                        />
                                    </div>
                                    <div className="errorMsg birthDay">{this.state.errors["birthDay"]}</div>
                                </div>
                                <div className="label-mt">
                                    <label>
                                        <span className="label-validate">*</span> 
                                        <span className="font-header-label pl-2">เพศ</span>
                                    </label>
                                    <div>
                                        <select 
                                            className={`inputWidth form-control input_form ${this.state.errorsFocus['gender']}`}
                                            placeholder="เลือกเพศ"
                                            name="gender"
                                            value={this.state.fields['gender']}
                                            onChange={(e) => this.handChange(e)}
                                        >
                                            <option value="4">เลือกเพศ</option>
                                            <option value="1">ชาย</option>
                                            <option value="2">หญิง</option>
                                            <option value="3">อื่น ๆ</option>
                                        </select>
                                    </div>
                                    <div className="errorMsg gender">{this.state.errors["gender"]}</div>
                                </div>
                                <div className="btn_nextstep">
                                    <button 
                                        type="submit"
                                        className="btn btn-secondary btn-block btn-check"
                                    >
                                        ยืนยัน
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

