import React from 'react';
import { Switch , Route } from 'react-router-dom';
import { home , adsdetail } from '../page/home';
import { News } from '../page/news';
import { Privileges , Detail} from '../page/privileges';
import { Location , DetailMap } from '../page/location'; 
import {Regisweb , Regishome , otp , Register , Edit , forgot , changepass , forgotp ,newpass,newpassotp ,otpedit} from '../page/register';
import NotFoundPages from '../page/404';
import Header from "../_pagebuilder/Header";
import Footer from "../_pagebuilder/Footer";
import { Problem , myMaxCard , History , editprofile , registemaxcard , Success , regisotp} from '../page/ptmax';

// const chLogin = (localStorage.getItem('login') === null ? LoginPage : Home);

export default () => (
    <Route>
        <div>
            <Header />
                <Switch>
                    <Route exact path="/website/home" component={home}/>
                    <Route exact path="/website/adsdetail" component={adsdetail} />

                    <Route exact path="/website/news" component={News} />

                    <Route exact path="/website/register" component={Regisweb} />
                    <Route exact path="/website/register/home" component={Regishome} />
                    <Route exact path="/website/register/otp" component={otp} />
                    <Route exact path="/website/register/web" component={Register} />
                    <Route exact path="/website/editprofile" component={Edit} />
                    <Route exact path="/website/forgotpassword" component={forgot} />
                    <Route exact path="/website/forgotpassword/changpassword" component={changepass} />
                    <Route exact path="/website/forgotpassword/otp" component={forgotp} />
                    <Route exact path="/website/newpassword" component={newpass} />
                    <Route exact path="/website/newpassotp" component={newpassotp}/>
                    <Route exact path="/website/edit/otp" component={otpedit} />

                    <Route exact path="/website/privileges" component={Privileges} />
                    <Route exact path="/website/privileges/detail" component={Detail} />

                    <Route exact path="/website/location" component={Location} />
                    <Route exact path="/website/location/detail/:shopId/:lat/:lng" component={DetailMap} />

                    <Route exact path="/website/maxcard/report" component={Problem} /> 
                    <Route exact path="/website/PTmaxcard" component={myMaxCard} />
                    <Route exact path="/website/PTmaxcard/history" component={History} />
                    <Route exact path="/website/PTmaxcard/editprofile" component={editprofile} />
                    <Route exact path="/website/PTmaxcard/register" component={registemaxcard} />
                    <Route exact path="/website/PTmaxcard/success" component={Success} />
                    <Route exact path="/website/PTmaxcard/register/otp" component={regisotp} />

                    <Route component={NotFoundPages} />
                </Switch>
            <Footer />
        </div>
    </Route>
)


