import React from 'react';
import '../../ptmax/ptmax.css';

export class editprofile extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            fields:{},
            TITLE_NAME_REMARK: "none",
            errors: {},
            errorsFocus: {
              CARD_ID: "",
              CARD_TYPE_ID: "",
              TITLE_NAME_ID: "",
              GENDER_ID: "",
              PHONE_NO: "",
              MARITAL_STATUS: "",
              FNAME_TH:"",
              LNAME_TH:""
            },
        };
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount(){
        this.closeNav();
        this.closeNa();
    }

    handleChange(e) {

    }

    openNa(){
        document.getElementById("myNav").style.width = "100%";
    }

    closeNa(){
        document.getElementById("myNav").style.width = "0%";
    }

    openNav() {
        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("main").style.marginLeft = "300px";
    }
      
    closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
    }

    render(){
        return(
            <div>
                <div className="bg-ptmax">
                    <div id="mySidenav" className="sidenav">
                        <div className="font-user-profile">
                            <img className="icon-width" src={`${process.env.PUBLIC_URL}/images/iconlogin-01.png`} />
                            <span className="pl-2">{localStorage.getItem('user')}</span>
                        </div>
                        <button onClick={() => this.closeNav()}>
                            <div className="closebtn">&times;</div>
                        </button>
                        <div className="block-navbar">
                            <a href={`${process.env.PUBLIC_URL}/ptmaxcard`}>บัตรพีที แมกซ์ ของฉัน</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">แก้ไขข้อมูลส่วนตัว</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">ประวัติการใช้งาน</a>
                        </div>
                        <div className="block-navbar">
                            <a href={`${process.env.PUBLIC_URL}/maxcard/report`}>แจ้งปัญหาการใช้งาน</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">เงื่อนไขและการบริการ</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">คำถามที่พบบ่อย</a>
                        </div>
                        <div className="block-navbar">
                            <a className="signout-font" href="#">ออกจากระบบ</a>
                        </div>
                    </div>
                    <div id="myNav" className="overlay">
                        <div>
                            <button className="closebtn" onClick={() => this.closeNa()}>&times;</button>
                        </div>
                        <div className="font-user-profile mt-5">
                            <img className="icon-width" src={`${process.env.PUBLIC_URL}/images/iconlogin-01.png`} />
                            <span className="pl-2">{localStorage.getItem('user')}</span>
                        </div>
                        <div className="overlay-content">
                            <a href={`${process.env.PUBLIC_URL}/ptmaxcard`}>บัตรพีที แมกซ์ ของฉัน</a>
                            <a href="#">แก้ไขข้อมูลส่วนตัว</a>
                            <a href="#">ประวัติการใช้งาน</a>
                            <a href={`${process.env.PUBLIC_URL}/maxcard/report`}>แจ้งปัญหาการใช้งาน</a>
                            <a href="#">เงื่อนไขและการบริการ</a>
                            <a href="#">คำถามที่พบบ่อย</a>
                            <a className="signout-font" href="#">ออกจากระบบ</a>
                        </div>
                    </div>
                    <div id="main">
                       <div className="headet-ptmaxcard">แก้ไขข้อมูลส่วนตัว</div>
                       <span className="mobile-pt span-heading" style={{cursor:'pointer'}} onClick={() => this.openNa()}><i className="fas fa-chevron-left"></i>&nbsp;&nbsp; บัตรพีที แมกซ์ ของฉัน</span>
                       <span className="span-heading" style={{cursor:'pointer'}} onClick={() => this.openNav()}><i className="fas fa-chevron-left"></i>&nbsp;&nbsp; บัตรพีที แมกซ์ ของฉัน</span>
                       <form>
                            <div className="row box-content">
                                <div className="col-6">
                                    <div className="header-profile">ข้อมูลส่วนตัว</div>
                                    <div className="form-group">
                                        <label>* เลขที่บัตรประชาชน</label>
                                        <div>
                                            <input
                                                type="number"
                                                className={`form-control input_form ${this.state.errorsFocus['CARD_ID']}`}
                                                maxLength="13"
                                                name="CARD_ID"
                                                onChange={e => this.handleChange(e)}
                                                id="exampleFormControlInput1"
                                                placeholder="เลขบัตรประชาชน"
                                                // value={this.state.fields["CARD_ID"]}
                                            />
                                        </div>
                                    </div>
                                    <input
                                        type="checkbox"
                                        name="aggreement"
                                        // checked={(this.state.fields["CARD_TYPE_ID"] === 1 ? false : true)}
                                        // onChange={e => this.changeHandle(e)}
                                    />
                                    <span className="pl-3 foreign-remind">
                                        กรณีเป็นชาวต่างชาติ (เลขที่หนังสือเดินทาง)
                                    </span>
                                    <div className="form-group">
                                        <label>
                                            <span className="required">* </span>คำนำหน้า
                                        </label>
                                        <div>
                                            <select
                                                className="form-control"
                                                // className={`form-control ${this.state.errorsFocus['TITLE_NAME_ID']}`}
                                                name="TITLE_NAME_ID"
                                                id="exampleFormControlSelect1"
                                                // onChange={e => this.handleChange(e)}
                                                // value={this.state.fields.TITLE_NAME_ID == 0 ? this.state.fields.TITLE_NAME_ID = '' : this.state.fields.TITLE_NAME_ID}
                                            >
                                                <option value="0">
                                                    กรุณาเลือกคำนำหน้าชื่อ
                                                </option>
                                                {/* {this.renderContent("type_title_name")} */}
                                            </select>
                                            {/* <div className="errorMsg TITLE_NAME_ID">
                                                {this.state.errors["TITLE_NAME_ID"]}
                                            </div> */}
                                        </div>
                                        <div
                                            className="form-group"
                                            style={{ display: this.state.TITLE_NAME_REMARK }}
                                        >
                                            <input
                                                type="text"
                                                className="form-control"
                                                // className={`form-control ${this.state.errorsFocus['TITLE_NAME_REMARK']} input_form`}
                                                name="TITLE_NAME_REMARK"
                                                onChange={e => this.handleChange(e)}
                                                id="exampleFormControlInput1"
                                                placeholder="คำนำหน้า"
                                                // value={this.state.fields['TITLE_NAME_REMARK']}
                                            />
                                            <div className="errorMsg TITLE_NAME_REMARK">
                                                {/* {this.state.errors["TITLE_NAME_REMARK"]} */}
                                            </div>
                                        </div>
                                        {/* <FormattedHTMLMessage id="title_nametitle_list" /> */}
                                    </div>
                                    <div className="form-group">
                                        <label>
                                            ชื่อ
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control input_form"
                                            // className={`form-control ${this.state.errorsFocus['FNAME_TH']} input_form`}
                                            name="FNAME_TH"
                                            // onChange={e => this.handleChange(e)}
                                            id="exampleFormControlInput1"
                                            placeholder="กรอกชื่อ"
                                            // value={this.state.fields.FNAME_TH}
                                        />
                                        <div className="errorMsg FNAME_TH">
                                            {/* {this.state.errors["FNAME_TH"]} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>
                                            <span className="required">* </span>นามสกุล
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control input-form"
                                            // className={`form-control ${this.state.errorsFocus['LNAME_TH']} input_form`}
                                            name="LNAME_TH"
                                            // onChange={e => this.handleChange(e)}
                                            id="exampleFormControlInput1"
                                            placeholder="กรอกนามสกุล"
                                            // value={this.state.fields['LNAME_TH']}
                                        />
                                        <div className="errorMsg LNAME_TH">
                                            {/* {this.state.errors["LNAME_TH"]} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>
                                            <span className="required">* </span>เพศ
                                        </label>
                                        <select
                                            className="form-control input_form"
                                            //className={`${genderClass} form-control ${this.state.errorsFocus['GENDER_ID']} input_form`}
                                            name="GENDER_ID"
                                            id="exampleFormControlSelect1"
                                            // onChange={e => this.handleChange(e)}
                                            // value={this.state.fields["GENDER_ID"]}
                                        >
                                            <option value="0">
                                                กรุณาเลือกเพศ
                                            </option>
                                            {/* {this.renderContent("type_gender")} */}
                                        </select>
                                        <div className="errorMsg GENDER_ID">
                                            {/* {this.state.errors["GENDER_ID"]} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>
                                            <span className="required">* </span>วัน เดือน ปีเกิด
                                        </label>
                                        {/* <div className="form-control input_form disabled" disabled={true}>
                                            {(this.state.language == 'en' ? moment(this.state.fields['BIRTH_DATE']).format('DD/MM/YYYY') : moment(this.state.fields['BIRTH_DATE']).add(543,'y').format('DD/MM/YYYY'))}
                                        </div> */}
                                        <input
                                            type="datetime"
                                            // max={maxDate}
                                            name="BIRTH_DATE"
                                            // onChange={e => this.handleChange(e)}
                                            className="form-control input_form"
                                            // className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`}
                                            id="date"
                                            // value={this.state.fields['BIRTH_DATE']}
                                        /> 
                                        <div className="errorMsg BIRTH_DATE">
                                            {/* {this.state.errors["BIRTH_DATE"]} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>
                                            <span className="required">* </span>สถานภาพ
                                        </label>
                                        <select
                                            className="form-control input_form" 
                                            // className={`form-control ${this.state.errorsFocus['MARITAL_STATUS']} input_form`}
                                            name="MARITAL_STATUS"
                                            id="exampleFormControlSelect1"
                                            // onChange={e => this.handleChange(e)}
                                            // value={this.state.fields['MARITAL_STATUS']}
                                        >
                                            <option value="0">
                                                กรุณาเลือกสถานภาพ
                                            </option>
                                            {/* {this.renderContent("type_status")} */}
                                        </select>
                                        <div className="errorMsg MARITAL_STATUS">
                                            {/* {this.state.errors["MARITAL_STATUS"]} */}
                                        </div>
                                    </div>
                                    <div className="header-profile">ข้อมูลติดต่อ</div>
                                    <div className="form-group">
                                        <label>
                                            <span className="required">* </span>อีเมล
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control input_form"
                                            // className={`form-control ${this.state.errorsFocus['USER_EMAIL']} input_form`}
                                            name="USER_EMAIL"
                                            // onChange={e => this.handleChange(e)}
                                            id="exampleFormControlInput1"
                                            placeholder="กรอกอีเมล"
                                            // value={this.state.fields['USER_EMAIL']}
                                        />
                                        <div className="errorMsg USER_EMAIL">
                                            {/* {this.state.errors["USER_EMAIL"]} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>
                                            <span className="required">* </span>เบอร์โทรศัพท์
                                        </label>
                                        {/* <div className="form-control input_form disabled" disabled={true}>
                                            {this.state.fields['PHONE_NO']}
                                        </div> */}
                                        <input
                                            type="number"
                                            maxLength="10"
                                            // className={`form-control ${this.state.errorsFocus['PHONE_NO']} input_form ${(this.state.fields["PHONE_NO"] ? `` : '')}`}
                                            className="form-control input_form"
                                            name="PHONE_NO"
                                            // onChange={e => this.handleChange(e)}
                                            id="exampleFormControlInput1"
                                            placeholder="กรอกเบอร์โทรศัพท์"
                                            // value={this.state.fields["PHONE_NO"]}
                                        />
                                        <div className="errorMsg PHONE_NO">
                                            {/* {this.state.errors["PHONE_NO"]} */}
                                        </div>
                                    </div>
                                </div>

                                <div className="col-6">
                                    <div className="header-profile">ข้อมูลที่อยู่</div>
                                    <div className="form-group">
                                        <span className="required">* </span><label className="label">บ้านเลขที่</label>
                                        <input 
                                            type="text" 
                                            className="form-control input_form"
                                            // className={`form-control input_form ${this.state.errorsFocus['ADDR_ADDRESS']}`} 
                                            id="exampleFormControlInput1" 
                                            maxLength="10" 
                                            name="ADDR_ADDRESS" 
                                            // onChange={(e) => this.handleChange(e)} 
                                            // value={this.state.fields['ADDR_ADDRESS']}
                                            placeholder="กรอกบ้านเลขที่"
                                        />
                                        <div className="errorMsg">
                                            {/* {this.state.errors['ADDR_ADDRESS']} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>หมู่บ้าน/อาคาร</label>
                                        <input
                                            type="text"
                                            className="form-control input_form"
                                            // className={`form-control input_form ${this.state.errorsFocus['ADDR_MOO']}`}
                                            maxLength="3"
                                            id="exampleFormControlInput1"
                                            name="ADDR_MOO"
                                            // onChange={(e) => this.handleChange(e)}
                                            // value={this.state.fields['ADDR_MOO']}
                                            placeholder="กรอกหมู่"
                                        />
                                        <div className="errorMsg">
                                            {/* {this.state.errors['ADDR_MOO']} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>หมู่บ้าน/อาคาร</label>
                                        <input 
                                            type="text" 
                                            className="form-control input_form" 
                                            id="exampleFormControlInput1" 
                                            name="ADDR_VILLEGE" 
                                            // onChange={(e) => this.handleChange(e)} 
                                            // value={this.state.fields['ADDR_VILLEGE']} 
                                            placeholder="กรุณากรอกหมู่บ้าน/อาคาร"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>ตรอก/ซอย</label>
                                        <input 
                                            type="text" 
                                            className="form-control input_form" 
                                            name="ADDR_SOI" 
                                            // onChange={(e) => this.handleChange(e)} 
                                            // value={this.state.fields.ADDR_SOI} 
                                            id="exampleFormControlInput1" 
                                            placeholder="กรุณากรอกตรอก/ซอย"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>ถนน</label>
                                        <input 
                                            type="text" 
                                            className="form-control input_form bd-field" 
                                            name="ADDR_STREET" 
                                            // onChange={(e) => this.handleChange(e)} 
                                            // value={this.state.fields.ADDR_STREET} 
                                            id="exampleFormControlInput1" 
                                            placeholder="กรุณากรอกถนน"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label><span className="required">* </span>จังหวัด</label>
                                        <select
                                            className='form-control bd-field'
                                            id='exampleFormControlSelect1'
                                            name="ADDR_ORG_PROVINCE_ID"
                                            // onChange={(e) => this.getAmphure(e.target.value, 'onchange')}
                                            // value={this.state.fields['ADDR_ORG_PROVINCE_ID'] == 0 ? this.state.fields['ADDR_ORG_PROVINCE_ID'] = '' : this.state.fields['ADDR_ORG_PROVINCE_ID']}
                                        >
                                            <option value="0">กรุณาเลือกจังหวัด</option>
                                                {/* {provinceListData} */}
                                        </select>
                                        {/* {this.renderContent('type_province')} */}
                                        <div className="errorMsg">
                                            {/* {this.state.errors['ADDR_ORG_PROVINCE_ID']} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label><span className="required">* </span>อำเภอ</label>
                                        <select
                                            className='form-control bd-field'
                                            id='exampleFormControlSelect1'
                                            name="ADDR_ORG_AMPHURE_ID"
                                            // onChange={(e) => this.getTumbon(e.target.value, 'onchange')}
                                            // value={this.state.fields['ADDR_ORG_AMPHURE_ID'] == 0 ? this.state.fields['ADDR_ORG_AMPHURE_ID'] = '' : this.state.fields['ADDR_ORG_AMPHURE_ID']}
                                        >
                                            <option value="0">
                                                กรุณาเลือกอำเภอ
                                            </option>
                                            {/* {amphureListData} */}
                                        </select>
                                        <div className="errorMsg">
                                            {/* {this.state.errors['ADDR_ORG_AMPHURE_ID']} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label><span className="required">* </span>ตำบล</label>
                                        <select
                                            className='form-control bd-field'
                                            id='exampleFormControlSelect1'
                                            name="ADDR_ORG_SUB_DISTRICT_ID"
                                            // onChange={(e) => this.getPostcode(e.target.value, 'onchange')}
                                            // value={this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] == 0 ? this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] = '' : this.state.fields['ADDR_ORG_SUB_DISTRICT_ID']}
                                        >
                                            <option value="0">
                                                กรุณาเลือกตำบล
                                            </option>
                                            {/* {tumbonListData} */}
                                        </select>
                                        <div className="errorMsg">
                                            {/* {this.state.errors['ADDR_ORG_SUB_DISTRICT_ID']} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label><span className="required">* </span>รหัสไปรษณีย์</label>
                                        <select 
                                            className='form-control bd-field' 
                                            id='exampleFormControlSelect1' 
                                            name="ADDR_ORG_POSTALCODE_ID" 
                                            // onChange={(e) => this.handleChange(e)}
                                            // value={this.state.fields['ADDR_ORG_POSTALCODE_ID'] == 0 ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] = '' : (this.state.fields['ADDR_ORG_POSTALCODE_ID'] !== '' ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] : '')}
                                        >
                                            <option value="0">กรุณาเลือกรหัสไปรษณีย์</option>
                                            {/* {postcodeListData} */}
                                        </select>
                                        <div className="errorMsg">
                                            {/* {this.state.errors['ADDR_ORG_POSTALCODE_ID']} */}
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label>ข้อมูลที่อยู่ (เพิ่มเติม )</label>
                                        <textarea 
                                            className="form-control input_form bd-field" 
                                            id="exampleFormControlTextarea1" 
                                            rows="3" 
                                            name="ADDR_OTHER" 
                                            // value={this.state.fields.ADDR_OTHER} 
                                            // onChange={(e) => this.handleChange(e)} 
                                            placeholder="กรุณากรอกข้อมูลเพิ่มเติม"
                                        >
                                        </textarea>
                                    </div>
                                </div>
                                <div className="btn_nextstep">
                                    <button 
                                        type="submit" 
                                        className="btn btn-secondary btn-block btn_agreement" 
                                        // onClick={() => this.submitFinalForm()}
                                    >
                                        บันทึกข้อมูล
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}


