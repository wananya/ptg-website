let config;
const urlPath = window.location.hostname;

if(urlPath === 'localhost'){
    config = {
        apis : `https://asv-mobileapp-dev.azurewebsites.net/api`,
    }
}else{
    config = {
        apis : `https://asv-mobileapp-dev.azurewebsites.net/api`,
    }
}

export const Base_API = config;