import React from "react";
import '../Footer/footer.css';
function Footer() {
  return (
    <div>
                <div className="set-zindex">
                    <div className="pc-pt container-fluid footer font-footer">
                        {/* <img style={{width:'100%'}} src={`${process.env.PUBLIC_URL}/images/footer-01.jpg`} /> */}
                        <div className="row mx-5">
                            <div className="col-3 col-xs-3 col-sm-3 col-md-3 text-left">
                                <div className="row">
                                    <div className="col-3 col-xs-3 col-sm-3 col-md-3">
                                        <div className="text-left"><img style={{width:'100%'}}src={`${process.env.PUBLIC_URL}/images/icon.png`} /></div>
                                        <div className="nav-bar">

                                        </div>
                                    </div>
                                    <div className="col-9 col-xs-9 col-sm-9 col-md-9">
                                        <div className="ptg-energy-font">PTG ENERGY</div>
                                        บริษัทพีทีจี เอ็นเนอยี จำกัด
                                    </div>
                                </div>
                                <div style={{marginLeft:'27%'}}>
                                    PTG Energy Public Company Limited <br />
                                    90 อาคารซีดับเบิ้ลยู ทาวเวอร์ เอ ชั้นที่ 33 <br />
                                    ถนน รัชดาภิเษก แขวงห้วยขวาง กรุงเทพฯ <br />
                                    10310 
                                </div>
                            </div>
                            <div className="col-3 col-xs-3 col-sm-3 col-md-3 text-left">
                                <div className="row">
                                    <div className="col-2 col-xs-2 col-sm-2 col-md-2"></div>
                                    <div className="col-10 col-xs-10 col-sm-10 col-md-10">
                                        <div className="head-footer-font">บัตรพีที แมกซ์</div>
                                        เกี่ยวกับบัตร <br/>
                                        สมัครบัตรพีที แมกซ์ <br/>
                                        ขั้นตอนการสมัครบัตร <br/>
                                        คิดแต้มจากค่าน้ำมัน <br/>
                                        เงื่อนไขการใช้งานบัตร 
                                    </div>
                                </div>
                            </div>
                            <div className="col-3 col-xs-3 col-sm-3 col-md-3 text-left">
                                <div className="head-footer-font">กิจกรรมและสิทธิพิเศษ</div>
                                ข่าวสารและกิจกรรม <br/>
                                โปรโมชั่นแนะนำ <br/>
                                ของรางวัล <br/>
                                E-Stamp <br/>
                                โอนคะแนนสะสม
                            </div>
                            <div className="col-3 col-xs-3 col-sm-3 col-md-3 text-left">
                                <div className="head-footer-font">การใช้บริการ </div>
                                ค้นหาสถานีคำถามที่พบบ่อย <br />
                                แจ้งปัญหาการใช้งาน <br />
                                เงื่อนไขและการบริการ
                            </div>
                        </div>
                    </div>
                </div>

                <div className="set-zindex">
                    <div className="mobile-pt footer font-footer">
                        <div className="text-left ml-3">
                            <img style={{width:'20%'}}src={`${process.env.PUBLIC_URL}/images/icon.png`} />
                            <span>
                                <div className="ptg-energy-font setalign">PTG ENERGY</div>
                                <div className="setalign-two">
                                    บริษัทพีทีจี เอ็นเนอยี จำกัด
                                </div>
                            </span>
                        </div>
                        <div className="text-left mt-3" style={{marginLeft:'12%'}}>
                            PTG Energy Public Company Limited <br />
                            90 อาคารซีดับเบิ้ลยู ทาวเวอร์ เอ ชั้นที่ 33 <br />
                            ถนน รัชดาภิเษก แขวงห้วยขวาง กรุงเทพฯ <br />
                            10310 
                            <div className="my-3">
                                <img style={{width:'30%'}} src={`${process.env.PUBLIC_URL}/images/logo001@3x.png`}/>
                            </div>
                        </div>
                        <div className="row container-fluid text-left">
                            <div className="col-6 col-sm-6 col-md-6 col-xs-6" style={{paddingLeft: '45px'}}>
                                <div className="head-footer-font">บัตรพีที แมกซ์</div>
                                เกี่ยวกับบัตร <br/>
                                สมัครบัตรพีที แมกซ์ <br/>
                                ขั้นตอนการสมัครบัตร <br/>
                                คิดแต้มจากค่าน้ำมัน <br/>
                                เงื่อนไขการใช้งานบัตร 
                                <div>
                                    <div className="head-footer-font">การใช้บริการ </div>
                                    ค้นหาสถานีคำถามที่พบบ่อย <br />
                                    แจ้งปัญหาการใช้งาน <br />
                                    เงื่อนไขและการบริการ
                                </div>
                            </div>
                            <div className="col-6 col-sm-6 col-md-6 col-xs-6">
                                <div className="head-footer-font">กิจกรรมและสิทธิพิเศษ</div>
                                ข่าวสารและกิจกรรม <br/>
                                โปรโมชั่นแนะนำ <br/>
                                ของรางวัล <br/>
                                E-Stamp <br/>
                                โอนคะแนนสะสม
                            </div>
                        </div>
                    </div>
                </div>
    </div>
  );
}

export default Footer;