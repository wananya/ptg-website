import React from 'react';
import '../../news/news.css';

export class News extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isHovered:true
        };
        this.handleHover = this.handleHover.bind(this);
    }
    componentDidMount(){

    }

    handleHover(){
        this.setState(prevState => ({
            isHovered: !prevState.isHovered
        }));
    }

    render(){
        const btnClass = this.state.isHovered ? "block" : "none";
        return(
            <div>
                <div className="pc-pt container-fluid bg-web-pri">
                    <div className="header-privilege">ข่าวสารกิจกรรม</div>
                    <div className="row set-title-pr">
                        <div className="set-content-header">
                            <ul className="nav nav-tabs">
                                <li className="nav-item nav-mt">
                                    <a className="nav-link active" data-toggle="tab" href="#home">ข่าวสาร</a>
                                </li>
                                <li className="nav-item nav-mt">
                                    <a className="nav-link" data-toggle="tab" href="#menu1">โปรโมชัน</a>
                                </li>
                            </ul>
                        </div>

                        <div className="row set-mt-content">
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri" onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-news card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/news3.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">
                                                <div className="sub-head-title text-left">สะสม E-Stamp แลกของรางวัล</div>
                                                <div className="content-sub text-left">สามารถรับบัตรสมาชิกได้ทันที ที่สถานีบริการน้ำมันพีทีที่ร่วมรายการทั่วประเทศ</div>
                                            </span>
                                        </div>
                                        <div className="row">
                                            <div className="col-8 datetime-sub">1 ต.ค. - 8 ธ.ค. 62</div>
                                            <div className="col-4 readmore-sub">อ่านต่อ</div>
                                        </div> 
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-news card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/news3.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">
                                                <div className="sub-head-title text-left">สะสม E-Stamp แลกของรางวัล</div>
                                                <div className="content-sub text-left">สามารถรับบัตรสมาชิกได้ทันที ที่สถานีบริการน้ำมันพีทีที่ร่วมรายการทั่วประเทศ</div>
                                            </span>
                                        </div>
                                        <div className="row">
                                            <div className="col-8 datetime-sub">1 ต.ค. - 8 ธ.ค. 62</div>
                                            <div className="col-4 readmore-sub">อ่านต่อ</div>
                                        </div> 
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-news card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/news3.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">
                                                <div className="sub-head-title text-left">สะสม E-Stamp แลกของรางวัล</div>
                                                <div className="content-sub text-left">สามารถรับบัตรสมาชิกได้ทันที ที่สถานีบริการน้ำมันพีทีที่ร่วมรายการทั่วประเทศ</div>
                                            </span>
                                        </div>
                                        <div className="row">
                                            <div className="col-8 datetime-sub">1 ต.ค. - 8 ธ.ค. 62</div>
                                            <div className="col-4 readmore-sub">อ่านต่อ</div>
                                        </div> 
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-news card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/news3.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">
                                                <div className="sub-head-title text-left">สะสม E-Stamp แลกของรางวัล</div>
                                                <div className="content-sub text-left">สามารถรับบัตรสมาชิกได้ทันที ที่สถานีบริการน้ำมันพีทีที่ร่วมรายการทั่วประเทศ</div>
                                            </span>
                                        </div>
                                    </button>
                                    <div className="row">
                                        <div className="col-8 datetime-sub">1 ต.ค. - 8 ธ.ค. 62</div>
                                        <div className="col-4 readmore-sub">อ่านต่อ</div>
                                    </div> 
                                </div>
                            </div>
                        </div>

                        <div className="row set-mt-content">
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-news card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/news3.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">
                                                <div className="sub-head-title text-left">สะสม E-Stamp แลกของรางวัล</div>
                                                <div className="content-sub text-left">สามารถรับบัตรสมาชิกได้ทันที ที่สถานีบริการน้ำมันพีทีที่ร่วมรายการทั่วประเทศ</div>
                                            </span>
                                        </div>
                                        <div className="row">
                                            <div className="col-8 datetime-sub">1 ต.ค. - 8 ธ.ค. 62</div>
                                            <div className="col-4 readmore-sub">อ่านต่อ</div>
                                        </div> 
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-news card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/news3.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">
                                                <div className="sub-head-title text-left">สะสม E-Stamp แลกของรางวัล</div>
                                                <div className="content-sub text-left">สามารถรับบัตรสมาชิกได้ทันที ที่สถานีบริการน้ำมันพีทีที่ร่วมรายการทั่วประเทศ</div>
                                            </span>
                                        </div>
                                        <div className="row">
                                            <div className="col-8 datetime-sub">1 ต.ค. - 8 ธ.ค. 62</div>
                                            <div className="col-4 readmore-sub">อ่านต่อ</div>
                                        </div> 
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-news card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/news3.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">
                                                <div className="sub-head-title text-left">สะสม E-Stamp แลกของรางวัล</div>
                                                <div className="content-sub text-left">สามารถรับบัตรสมาชิกได้ทันที ที่สถานีบริการน้ำมันพีทีที่ร่วมรายการทั่วประเทศ</div>
                                            </span>
                                        </div>
                                        <div className="row">
                                            <div className="col-8 datetime-sub">1 ต.ค. - 8 ธ.ค. 62</div>
                                            <div className="col-4 readmore-sub">อ่านต่อ</div>
                                        </div> 
                                    </button>
                                </div>
                            </div>
                            <div className="col-xs-6 col-md-6 col-sm-6 col-lg-3 text-center">
                                <div className="card-pri">
                                    <button className="btn-pri">
                                    {/* <button className="btn-pri"  onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}> */}
                                        <img className="img-news card-img-top"  alt="Card image cap" src={`${process.env.PUBLIC_URL}/images/news3.jpg`} />
                                        <div className="my-3 set-detail-col">
                                            <span className="setdetailpri">
                                                <div className="sub-head-title text-left">สะสม E-Stamp แลกของรางวัล</div>
                                                <div className="content-sub text-left">สามารถรับบัตรสมาชิกได้ทันที ที่สถานีบริการน้ำมันพีทีที่ร่วมรายการทั่วประเทศ</div>
                                            </span>
                                        </div>
                                        <div className="row">
                                            <div className="col-8 datetime-sub">1 ต.ค. - 8 ธ.ค. 62</div>
                                            <div className="col-4 readmore-sub">อ่านต่อ</div>
                                        </div> 
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> 
            </div>
        )
    }
}
