import React from 'react';
import '../../location/location.css';
import { ShopAction } from '../../../_actions/locationAction';
import FilterResults from 'react-filter-search';


export class Location extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showLoading: 'block',
            getShopProduct: {},
            productPetrol: {},
            productOther: {},
            modalFilter: false,
            where: {lat:null, lng:null},  
            pageNo: "1",
            lang: "th",
            textsearch: "",
            distancePT: "10000",
            getPTLocation: [],
            coords:{},
            isMap: false,
            tablist: "fade",
            tabmap: "active",
            tabfilter: "fade",
            bgtabs: "",
            on: false,
            FAQList: [],
            value: "",
            GetProvince:{},
            GetAmphure:{},
            valueinputrange: 10 ,
            distance:"10",
            province:"",
            amphure:"",
            productlist1: {},
            productlist2: {},
            productlist3: {},
            productlist4: {},
            productPetrolFilter: [],
            productOtherFilter: [],
        };
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/logo.jpg`);
    }

    componentDidMount() {
        if(sessionStorage.getItem("checkData") === "checkData"){
            this.checkDataDone()
        } else {
            this.checkDataNone()
        }
        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 2000, //20 second  
          };  
          this.setState({ready:false, error: null });  
            navigator.geolocation.getCurrentPosition( this.geoSuccess,  
              this.geoFailure,  
              geoOptions
          ); 

    }

    checkDataDone(){
        // sessionStorage.setItem("checkProductpt", "")
        sessionStorage.setItem("checkData", "checkData")
        // this.handlelatlong()
    }

    checkDataNone(){
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            sessionStorage.setItem("province", "")
            sessionStorage.setItem("amphure", "")
            sessionStorage.setItem("productPetrolFilter", "")
            sessionStorage.setItem("productOtherFilter", "")
            sessionStorage.setItem("distance", "10000")
            sessionStorage.setItem("checkDataFilter", "")
            sessionStorage.setItem("checkDatacity", "")
            sessionStorage.setItem("checkData", "checkData")
            // this.handlelatlong()
            // window.location.href="/map/home"
            this.setState({ showLoading: 'none' })
        }, 3500);
    }

    geoSuccess = (position) => {  
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({  
                ready:true,  
                where: {lat: position.coords.latitude,lng:position.coords.longitude }  
            })  
            sessionStorage.setItem("lat", this.state.where.lat)
            sessionStorage.setItem("lng", this.state.where.lng)
            this.handlegetShopProduct()
            this.handlegetgetPTLocation()
            this.handleGetProvince()
            this.setState({ showLoading: 'none' })
            

        }, 3500);
    }  

    geoFailure = (err) => {  
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({
                error: err.message
            });  
            sessionStorage.setItem("lat", "13.7758095")
            sessionStorage.setItem("lng", "100.57419329999999")
            this.handlegetShopProduct()
            this.handlegetgetPTLocation()
            this.handleGetProvince()
            this.setState({ showLoading: 'none' })
        }, 3500);
    }  

    handlegetShopProduct(){
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            ShopAction.getShopProduct().then(e => {

                if (e.isSuccess === true) {
                    this.setState({ getShopProduct: e.data })
                    this.setState({ productPetrol: e.data.productPetrol })
                    this.setState({ productOther: e.data.productOther })
                    this.setState({ showLoading: 'none' })
                } else {
                    this.setState({ showError: true });
                }
            });
        }, 2500);
    }

    handlegetgetPTLocation(){
        var lat = sessionStorage.getItem('lat');
		var lng = sessionStorage.getItem('lng');
		var province = sessionStorage.getItem('province');
		var amphure = sessionStorage.getItem('amphure');
        var distance = sessionStorage.getItem('distance');
        if(sessionStorage.getItem('checkDataFilter') === ""){
            var productlist = "";
          } else {
            var productlist = sessionStorage.getItem('productPetrolFilter') + "," + sessionStorage.getItem('productOtherFilter');
          }
        var isMap = true;

        const { pageNo, lang, textsearch} = this.state;
        
        ShopAction.getPTLocation(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist).then(e => {
            if (e.isSuccess === true) {
                console.log(e)
                this.setState({ getPTLocation: e.data })
            } else {
                this.setState({ showError: true });
            }

        });
    }

    handleGetProvince(){
        ShopAction.GetProvince().then(e => {
            if (e.isSuccess === true) {
                this.setState({ GetProvince: e.data })
            } else {
                this.setState({ showError: true });
            }
        });
    }

    handleChangeSelect(e) {
        var value = e.target.value;
        this.setState({ codE_GUID: value });
        this.setState({ province: value });
        var formData = new FormData();
        formData.append("codE_GUID", value);
        ShopAction.GetAmphure(formData).then(e => {
            this.setState({ GetAmphure: e.dropdowN_INFO })
        });
    }

    actioneArrProduct(value){
        var arrProduct = [...this.state.productPetrolFilter];
        var index = arrProduct.indexOf(value)
        if (index !== -1) {
            arrProduct.splice(index, 1);
        }else{
            arrProduct.push(value);
        }
        this.setState({productPetrolFilter: arrProduct});
        sessionStorage.setItem('productPetrolFilter' , arrProduct)
        sessionStorage.setItem("checkDataFilter", "checkDataFilter")
    }

    actioneArrService(value){
        var arrProduct = [...this.state.productOtherFilter];
        var index = arrProduct.indexOf(value)
        if (index !== -1) {
            arrProduct.splice(index, 1);
        }else{
            arrProduct.push(value);
        }
        this.setState({productOtherFilter: arrProduct});
        sessionStorage.setItem('productOtherFilter' , arrProduct)
        sessionStorage.setItem("checkDataFilter", "checkDataFilter")
    }

    onHandleproductPetrol(e){
        console.log(e.target.value)
        this.actioneArrProduct(e.target.value)
    }

    onHandleproductOther(e){
        console.log(e)
        this.actioneArrService(e.target.value)
    }

    handleChangesearch = event => {
        const { value } = event.target;
        this.setState({ value });
    };

    handleChange = (e) => {
        const input = e.target;
        const value = input.value;
        this.setState({ [input.name]: value });
    };

    handleFormSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        this.handleFilter();
    }

    handleFilter(){
        sessionStorage.setItem("province", this.state.province)
        sessionStorage.setItem("amphure", this.state.amphure)
        sessionStorage.setItem("checkDatacity", "checkDatacity")
        window.location.href = "/website/location";
    }

    render(){
        const { getPTLocation, value } = this.state;

        let { GetProvince } = this.state
        var GetProvinceCards = [];
        for (var x in GetProvince) {
            GetProvinceCards.push(
                <option value={GetProvince[x].codE_GUID}>{GetProvince[x].values}</option>
            );
        }

        let { GetAmphure } = this.state
        var GetAmphureCards = [];
        for (var x in GetAmphure) {
            GetAmphureCards.push(
                <option value={GetAmphure[x].codE_GUID}>{GetAmphure[x].values}</option>
            );
        }

        let { productOther } = this.state
        var productOtherCards = [];
        for (var x in productOther) {
            productOtherCards.push(
                <span className="p-1">
                    <input 
                        id={productOther[x].id} 
                        type="checkbox" 
                        name={`productOther${productOther[x].id}`}
                        value={productOther[x].producT_ID} 
                        onClick={(e) => this.onHandleproductOther(e)}
                    />
                    <label for={productOther[x].id}>
                    <img 
                        src={productOther[x].producT_IMAGE}
                        className="img-fluid"
                        width="35"
                    />
                    </label>
                </span>
            );
        }

        let { productPetrol } = this.state
        var productPetrolCards = [];
        for (var x in productPetrol) {
            productPetrolCards.push(
                <span className="p-1 py-1">
                    <input
                        id={productPetrol[x].id} 
                        type="checkbox" 
                        name={`productPetrol${productPetrol[x].id}`}
                        value={productPetrol[x].producT_ID} 
                        onClick={(e) => this.onHandleproductPetrol(e)}
                    />
                    <label for={productPetrol[x].id}>
                        <img 
                            src={productPetrol[x].producT_IMAGE}
                            className="img-fluid checkbox-img"
                            width="35"
                        />
                    </label> 
                </span>
            );
        }
        return(
            <div>
                <div className="container-fluid bg-web-pri">
                    <div className="header-location-font">ค้นหาสถานีบริการ</div>
                    <div className="row mb-5">
                        <div className="col-sm-12 col-xs-12 col-lg-7">
                            <div id="map-container">
                                <div id="map" data-map-zoom="9" data-map-scroll="true"></div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-xs-12 col-lg-5">
                            {/* <div className={`tab-pane ${this.state.tabfilter}`}> */}
                            <div className="tab-pane">
                                <div className="container content-top">
                                    <div className="loaderdetail mx-auto" style={{ display: this.state.showLoading}}></div>
                                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                                        <form onSubmit={(e) => this.handleFormSubmit(e)}> 
                                            <div className="">
                                                <div className="mt-4">
                                                    <div className="t-18 t-bold subheader-location">ผลิตภัณฑ์</div>
                                                </div>
                                                <div className="">
                                                    {productPetrolCards}
                                                </div>
                                            </div>
                                            <div className="pt-3">
                                                <div className="">
                                                    <div className="t-18 t-bold subheader-location">บริการ</div>
                                                </div>
                                                <div className="">
                                                    {productOtherCards}
                                                </div>
                                            </div>
                                            <hr/>
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="t-18 t-bold subheader-location">ค้นหา</div>
                                                </div>
                                                <div className="col-12">
                                                    <div className="input-group form-phone mb-3">
                                                        <select 
                                                            className="form-control select-pro arrow-down-black set-font-selected" 
                                                            id="selected"
                                                            name="province"
                                                            onChange={(e) => this.handleChangeSelect(e)}
                                                            value={this.state.province} 
                                                        >
                                                            <option selected hidden value="" className="set-font-selected">ค้นหา</option>
                                                            {GetProvinceCards}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div className="col-12">
                                                    <div className="input-group form-phone mb-3">
                                                        <select 
                                                            className="form-control select-pro arrow-down-black set-font-selected" 
                                                            id="selected"
                                                            name="amphure"
                                                            value={this.state.amphure} 
                                                            onChange={this.handleChange}
                                                        >
                                                            <option selected hidden value="">ค้นหาอำเภอ</option>
                                                            {GetAmphureCards}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div className="row">
                                                <div className="col-12 d-flex t-18 t-bold">
                                                    <div className="mr-auto subheader-location">ระยะการแสดงผล (จากตำแหน่งปัจจุบัน)</div>
                                                    <div className="kmdis" id="p1">{this.state.distance} กม.</div>
                                                </div>
                                                <div className="col-12 d-flex t-18 t-bold">
                                                    <input className="distance-radius" type="range" min="1" max="100" step="1" value={this.state.distance}/> 
                                                </div>
                                            </div>
                                            <div className="heigh-footer"></div>
                                            <div className="row fix-bottom py-3 text-center">
                                                <div className="col-12">
                                                    <button type="submit" className="t-20 btn btn-block btn-green shadow-none localtion-btn-nextstep">
                                                        ค้นหาร้านค้า
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mb-5">
                        <div className="text-center">
                            <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place01.jpg`} /></div>
                            <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place02.jpg`} /></div>
                            <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place03.jpg`} /></div>
                            <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place04.jpg`} /></div>
                            <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place04.jpg`} /></div>
                            <br/>
                        </div>
                    </div> 
                </div>
            </div>
        )
    }
}