import React from 'react';
import '../../../ptmax/ptmax.css';

export class Success extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            
        };
    }


    componentDidMount(){

    }


    render(){

        return(
            <div>
                <div className="bg-ptmax">
                    <div id="main">
                        <div className="box-content">
                            <div className="text-center my-4">
                                <img style={{width:'20%'}} src={`${process.env.PUBLIC_URL}/images/01.png`} />
                            </div>
                            <div className="text-center my-4">
                                <img style={{width:'5%'}} src={`${process.env.PUBLIC_URL}/images/02.png`} />
                            </div>
                            <div className="text-center sent-data-success my-3">
                                ส่งข้อมูลเรียบร้อย
                            </div>
                            <div className="text-center">
                                <button className="btn-secondary">กลับหน้าหลัก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


