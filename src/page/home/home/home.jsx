import React from 'react';
import { homeAction } from '../../../_actions/homeAction'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import '../../home/home.css';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import LazyLoad from 'react-lazy-load';
import SweetAlert from 'react-bootstrap-sweetalert';
export class home extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            dataBanner: [],
            iconDisplay: [],
            showDropDown:'none',
            modal: null,
            show:false
        };
    }
    componentDidMount(){
        this.getBannerInHome();
    }
    getBannerInHome(){
        var lang = 'TH';
        var cardType = '!';
        homeAction.getBanner(lang,cardType).then(e => {
            console.log(e)
            if(e.data.isSuccess === true){
                this.setState({ 
                    dataBanner : e.data.data.adslist , 
                    dataIcon : e.data.data.bannerIconDetail ,
                    dataPromotion : e.data.data.promotion_Not_Promote
                })
            }
            if(e.data.data.promotion_Not_Promote.length < 4){
                this.setState({ infinite : false })
            }else{
                this.setState({ infinite : true })
            }
        })
    }
    showBtn(showBtn){
        if(showBtn === 'none'){
            this.setState({ showDropDown : 'block' })
        }else{
            this.setState({ showDropDown : 'none' })
        }
    }
    getNoBanner(bannerDetail){

    }
    showModalLogin(){
        alert = (
            <SweetAlert
                custom
                closeOnClickOutside = {true}
                showConfirm = {false}
            >
                <div className="row">
                    <div className="col-6">
                        <img style={{width:'100%'}} src={`${process.env.PUBLIC_URL}/images/Untitled-1-02.jpg`} />
                    </div>
                    <div className="col-6">
                        <div className="header-sigin">ลงชื่อเข้าใช้</div>
                        <div className="my-3 label-header">
                            <label className="label-text-pc">เบอร์โทรศัพท์ / อีเมล</label>
                            <input 
                                className="form-control-set"
                            />
                        </div>
                        <div className="my-3 label-header">
                            <label className="label-text-pc">รหัสผ่าน</label>
                            <input 
                                className="form-control-set"
                            />
                        </div>
                        <div className="label-forgot">ลืมรหัสผ่าน ?</div>
                        <div className="loginandregis-btn text-center">
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบ</div>
                        </div>
                        <div className="loginregis-fb-btn" onClick={() => this.regisFacebook()}>
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบด้วย Facebook</div>
                        </div>
                        <div className="loginregis-gm-btn">
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบด้วย Gmail</div>
                        </div>
                        <div className="loginandregis-btn mt-3">
                            <div style={{marginTop: '5px'}}><a href="">สมัครสมาชิกใหม่</a></div>
                        </div>
                    </div>
                </div>
            </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }
    render(){
        var settings = {
            infinite: this.state.infinite,
            speed: 200,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    infinite: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    rows:2
                  }
                }
            ]
        };
        var settingsNews = {
            infinite: this.state.infinite,
            speed: 200,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    rows:1
                  }
                }
            ]
        };
        var disPlayBanner = [];
        var disPlayIcon = [];
        var displayPromotion = [];
        var bannerSlide = this.state.dataBanner;
        var iconDisplay = this.state.dataIcon;
        var promotionDisPlay = this.state.dataPromotion;
        for(var i in bannerSlide){
            disPlayBanner.push(
                <LazyLoad>
                    <img src={bannerSlide[i].banner_Cover} />
                </LazyLoad>
            )
        }
        for(var i in iconDisplay){
            disPlayIcon.push(
                <div className="col-6 col-xs-6 col-sm-6 col-md-6 mt-block LazyLoad is-visible">
                    <LazyLoad>
                        <img style={{width:'100%'}} src={iconDisplay[i].image}/>
                    </LazyLoad>
                </div>
            )
        }
        for(var i in promotionDisPlay){
            displayPromotion.push(
                <div className="card set-margin LazyLoad is-visible" style={{width:'18rem'}}>
                    <LazyLoad>
                        <img src={promotionDisPlay[i].imG_URL_NORMAL} className="card-img-top" alt="..." />     
                    </LazyLoad>
                    <div className="card-body">
                        <p className="card-text line-clamp">{promotionDisPlay[i].redeeM_NAME}</p>
                    </div>
                </div> 
            )
        }
        return(
            <div className="">
                <div className="LazyLoad is-visible">
                    <Carousel 
                        autoPlay={true}
                        showThumbs={false}
                    >
                        {disPlayBanner}
                    </Carousel>
                </div>
                <div className="container-content">
                <div className="font-PT">บัตรพีที แมกซ์</div>
                    <div className="pc-pt">
                        <div className="row">
                            <div className="col-2 col-xs-2 col-sm-2 col-md-2 px-0 text-center">
                                <img className="fix-icon" src={`${process.env.PUBLIC_URL}/images/icon-02.jpg`} />
                                <div className="font-body">แลกคะแนน</div>
                            </div>
                            <div className="col-2 col-xs-2 col-sm-2 col-md-2 px-0 text-center">
                                <img className="fix-icon" src={`${process.env.PUBLIC_URL}/images/icon-01.jpg`} />
                                <div className="font-body">สะสมคะแนน</div>
                            </div>
                            <div className="col-2 col-xs-2 col-sm-2 col-md-2 px-0 text-center">
                                <img className="fix-icon" src={`${process.env.PUBLIC_URL}/images/icon-03.jpg`} />
                                <div className="font-body">รายการคูปอง</div>
                            </div>
                            <div className="col-2 col-xs-2 col-sm-2 col-md-2 px-0 text-center">
                                <img className="fix-icon" src={`${process.env.PUBLIC_URL}/images/icon-04.jpg`} />
                                <div className="font-body">แสตมป์สะสม</div>
                            </div>
                            <div className="col-2 col-xs-2 col-sm-2 col-md-2 px-0 text-center">
                                <img className="fix-icon" src={`${process.env.PUBLIC_URL}/images/icon-05.jpg`} />
                                <div className="font-body">โอนคะแนนสะสม</div>
                            </div>
                            <div className="col-2 col-xs-2 col-sm-2 col-md-2 px-0 text-center">
                                <img className="fix-icon" src={`${process.env.PUBLIC_URL}/images/icon-06.jpg`} />
                                <div className="font-body">ค้นหาสถานี</div>
                            </div>
                        </div>
                    </div>

                    <div className="mobile-pt">
                        <div className="row">
                            <div className="col-4 col-xs-4 col-sm-4 col-md-4 px-0 text-center">
                                <img className="fix-icon" src="images/icon-02.jpg" />
                                <div className="font-body">แลกคะแนน</div>
                            </div>
                            <div className="col-4 col-xs-4 col-sm-4 col-md-4 px-0 text-center">
                                <img className="fix-icon" src="images/icon-01.jpg" />
                                <div className="font-body">สะสมคะแนน</div>
                            </div>
                            <div className="col-4 col-xs-4 col-sm-4 col-md-4 px-0 text-center">
                                <img className="fix-icon" src="images/icon-03.jpg" />
                                <div className="font-body">รายการคูปอง</div>
                            </div>
                            <div className="col-4 col-xs-4 col-sm-4 col-md-4 px-0 text-center">
                                <img className="fix-icon" src="images/icon-04.jpg" />
                                <div className="font-body">แสตมป์สะสม</div>
                            </div>
                            <div className="col-4 col-xs-4 col-sm-4 col-md-4 px-0 text-center">
                                <img className="fix-icon" src="images/icon-05.jpg" />
                                <div className="font-body">โอนคะแนนสะสม</div>
                            </div>
                            <div className="col-4 col-xs-4 col-sm-4 col-md-4 px-0 text-center">
                                <img className="fix-icon" src="images/icon-06.jpg" />
                                <div className="font-body">ค้นหาสถานี</div>
                            </div>
                        </div>
                    </div>

                    <div className="pc-pt row mt-box">
                        <div className="col-6 col-xs-6 col-sm-6 col-md-6">
                            <div className="row">
                                {disPlayIcon}
                            </div>
                        </div>
                        <div className="col-6 col-xs-6 col-sm-6 col-md-6 vertical-box">
                            <div className="set-content-detail">
                                <div className="header-body-pt">บัตรพีที แมกซ์</div>
                                <div className="header-body-pt">เชื่อมได้หลายที่ดีลดี ๆ ก็มีเยอะ</div>
                                <div className="content-body-pt">
                                    บัตรเดียวที่เชื่อมดีลดี ๆ จากหลายแบรนด์ดัง และยังสามารถโอนคะแนนไปเป็นคะแนนบัตรอื่น และสนุกครบทุกไลฟ์สไตล์
                                    สมัครสมาชิก PT Max วันนี้ สะสมคะแนนเพื่อแลกของรางวัลและรับดีลดี ๆ อีกมากมาย
                                </div>
                                <div className="my-3">
                                    <button className="btn-detail"><div className="mt-1">สมัครบัตร</div></button>
                                    <button className="btn-detail register"><div className="mt-1">ดูรายละเอียด</div></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="mobile-pt">
                        <div className="row">
                            {disPlayIcon}
                        </div>
                        <div className="set-content-detail">
                            <div className="header-body-pt mt-3">บัตรพีที แมกซ์</div>
                            <div className="header-body-pt">เชื่อมได้หลายที่ดีลดี ๆ ก็มีเยอะ</div>
                            <div className="content-body-pt">
                                บัตรเดียวที่เชื่อมดีลดี ๆ จากหลายแบรนด์ดัง และยังสามารถโอนคะแนนไปเป็นคะแนนบัตรอื่น และสนุกครบทุกลฟ์สไตล์
                                สมัครสมาชิก PT Max วันนี้ สะสมคะแนนเพื่อแลกของรางวัลและรับดีลดี ๆ อีกมากมาย
                            </div>
                            <div className="text-center mt-3">
                                <button className="btn-detail"><div className="mt-1">สมัครบัตร</div></button>
                                <button className="btn-detail register"><div className="mt-1">ดูรายละเอียด</div></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bg-content">
                    <div className="pc-pt pt-5">
                        <div className="title-body seeAllInline">รับดีลดี ๆ จากบัตรพีที แมกซ์</div>
                        <div className="seeAllInline seeAllright"><button className="btn-custom border-rds"><div>ดูทั้งหมด</div></button></div>
                    </div>
                    <div className="mobile-pt pt-5 set-pdmobile">
                            <div className="title-body seeAllInline">
                                รับดีลดี ๆ <br />จากบัตรพีที แมกซ์
                            </div>
                            <div className="set-seeall">
                                <button className="btn-custom border-rds"><div>ดูทั้งหมด</div></button>
                            </div>
                    </div>
                    <div>
                        <div className="fix-slider">
                            <Slider {...settings}>                           
                                {displayPromotion}
                            </Slider>
                        </div>
                    </div>
                </div>
                <div className="pc-pt container-fluid bg-subcontent marginbt-subcontent">
                    <div className="pc-pt title-subcontent">สะดวกยิ่งขึ้นผ่านแอปพีที แมกซ์ รีวอร์ด</div>
                    <div className="row">
                        <div className="col-6 col-sm-6 col-md-6 col-xs-6 text-center LazyLoad is-visible">
                            <LazyLoad>
                                <div>
                                    <img style={{width:'60%'}} src={`${process.env.PUBLIC_URL}/images/mobile-02.png`}/>
                                </div>
                            </LazyLoad>
                        </div>
                        <div className="col-6 col-sm-6 col-md-6 col-xs-6">
                            <div className="row sub-contentbt">
                                <div className="col-2 col-sm-2 col-md-2 col-xs-2">
                                    <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-01.png`}/>
                                </div>
                                <div className="col-10 col-xs-10 col-sm-10 col-md-10 textinsubcontent">     
                                    <div className="text-sub-content">เติมน้ำมันสะสมคะแนนทันที</div>
                                    <div className="text-sub-contentbody">
                                        สมัครที่สถานีบริการน้ำมันพีทีทั่วประเทศ <br />
                                        พร้อมเปิดใช้งานได้ทันที
                                    </div>
                                </div>
                                <div className="col-2 col-sm-2 col-md-2 col-xs-2">
                                    <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-02.png`}/>
                                </div>
                                <div className="col-10 col-xs-10 col-sm-10 col-md-10 textinsubcontent">
                                    <div className="text-sub-content">สะสม E-Stamp แลกของรางวัล</div>
                                    <div className="text-sub-contentbody">
                                        สามารถรับบัตรสมาชิกได้ทันทีที่สถานีบริการ <br />
                                        น้ำมันพีทีที่ร่วมบริการทั่วประเทศ
                                    </div>
                                </div>
                                <div className="col-2 col-sm-2 col-md-2 col-xs-2">
                                    <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-03.png`}/>
                                </div>
                                <div className="col-10 col-xs-10 col-sm-10 col-md-10 textinsubcontent">
                                    <div className="text-sub-content">ค้นหา ร้านค้า และปั๊มน้ำมันได้ทุกที่</div>
                                    <div className="text-sub-contentbody">
                                        วิธีนับอายุแต้ม นับจากเดือนที่สมาชิกได้รับแต้ม <br />
                                        โดยสรุปและตัดยอดแต้มในวันสุดท้ายของเดือน <br />
                                        เดียวกันในอีก 3 ปีข้างหน้า
                                    </div>
                                </div>
                                <div className="col-2 col-sm-2 col-md-2 col-xs-2">
                                    <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/icon_app-04.png`}/>
                                </div>
                                <div className="col-10 col-xs-10 col-sm-10 col-md-10 textinsubcontent">
                                    <div className="text-sub-content">สะสมแลกสินค้ามากกว่า 100 รายการ</div>
                                    <div className="text-sub-contentbody">
                                        ทุกรายการยกเว้นส่วนลดเติมน้ำมันสามารถแลกรับ <br/>
                                        และใช้ได้เลยที่สถานีบริการน้ำมันพีที
                                    </div>
                                </div>
                            </div> 
                            <div className="my-3 mx-3">
                                <button className="btn-detail mr-2"><div className="mt-1">โหลดแอปพลิเคชั่น</div></button>
                                <button className="btn-detail register"><div className="mt-1">ขั้นตอนการใช้งาน</div></button>
                            </div>
                        </div> 
                    </div> 
                </div>

                <div className="mobile-pt container-fluid bg-subcontent marginbt-subcontent">
                    <div className="title-subcontent pt-4">สะดวกยิ่งขึ้นผ่านแอป <br/>พีที แมกซ์ รีวอร์ด</div>
                    <LazyLoad>
                        <div className="text-center my-3">
                            <img style={{width:'60%'}} src={`${process.env.PUBLIC_URL}/images/mobile-02.png`}/>
                        </div>
                    </LazyLoad>
                    <div className="row">
                        <div className="col-3 col-xs-3 col-md-3 col-sm-3">
                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-01.png`}/>
                        </div>
                        <div className="col-9 col-xs-9 col-md-9 col-sm-9 content-sub">
                            <div className="text-sub-content">เติมน้ำมันสะสมคะแนนทันที</div>
                            <div className="text-sub-contentbody">
                                สมัครที่สถานีบริการน้ำมันพีทีทั่วประเทศ <br />
                                พร้อมเปิดใช้งานได้ทันที
                            </div>
                        </div>
                        <div className="col-3 col-xs-3 col-md-3 col-sm-3">
                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-02.png`}/>
                        </div>
                        <div className="col-9 col-xs-9 col-md-9 col-sm-9 content-sub">
                            <div className="text-sub-content">สะสม E-Stamp แลกของรางวัล</div>
                            <div className="text-sub-contentbody">
                                สามารถรับบัตรสมาชิกได้ทันทีที่สถานีบริการ <br />
                                น้ำมันพีทีที่ร่วมบริการทั่วประเทศ
                            </div>
                        </div>
                        <div className="col-3 col-xs-3 col-md-3 col-sm-3">
                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-03.png`}/>
                        </div>
                        <div className="col-9 col-xs-9 col-md-9 col-sm-9 content-sub"> 
                            <div className="text-sub-content">ค้นหา ร้านค้า และปั๊มน้ำมันได้ทุกที่</div>
                            <div className="text-sub-contentbody">
                                วิธีนับอายุแต้ม นับจากเดือนที่สมาชิกได้รับแต้ม <br />
                                โดยสรุปและตัดยอดแต้มในวันสุดท้ายของเดือน <br />
                                เดียวกันในอีก 3 ปีข้างหน้า 
                            </div>
                        </div>
                        <div className="col-3 col-xs-3 col-md-3 col-sm-3">
                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/icon_app-04.png`}/>
                        </div>
                        <div className="col-9 col-xs-9 col-md-9 col-sm-9 content-sub">
                            <div className="text-sub-content">สะสมแลกสินค้ามากกว่า 100 รายการ</div>
                            <div className="text-sub-contentbody">
                                ทุกรายการยกเว้นส่วนลดเติมน้ำมันสามารถแลกรับ <br/>
                                และใช้ได้เลยที่สถานีบริการน้ำมันพีที
                            </div>
                        </div>
                    </div>
                    <div className="my-3 mx-3">
                        <button className="btn-detail mr-2"><div className="mt-1">โหลดแอปพลิเคชั่น</div></button>
                        <button className="btn-detail register"><div className="mt-1">ขั้นตอนการใช้งาน</div></button>
                    </div>
                </div>

                <div className="bg-content pc-pt">
                    <div className="pt-5">
                        <div className="title-body seeAllInline">ข่าวสารโปรโมชั่น</div>
                        <div className="seeAllInline seeAllright"><button className="btn-custom border-rds"><div>ดูทั้งหมด</div></button></div>
                    </div>
                    <div className="mx-5 pt-3 pb-5">
                        <div className="fix-slider">
                        <Slider {...settingsNews}>
                            {displayPromotion}
                        </Slider>
                        </div>
                    </div>
                </div>
                <div className="bg-content mobile-pt">
                    <div className="mobile-pt pt-5 set-pdmobile">
                        <div className="title-body seeAllInline">
                            ข่าวสารโปรโมชั่น
                        </div>
                        <div className="set-seeall">
                            <button className="btn-custom border-rds"><div>ดูทั้งหมด</div></button>
                        </div>
                    </div>
                    <div className="mx-5 pt-3 pb-5">
                        <div className="fix-slider">
                            <Slider {...settingsNews}>
                                {displayPromotion}
                            </Slider>
                        </div>
                    </div>
                </div>

                <div className="pc-pt mb-5">
                    <div className="title-green text-station my-5">ค้นหาสถานีบริการ</div>
                    <img className="picture-station" src={`${process.env.PUBLIC_URL}/images/s-location.jpg`}/>
                    <div className="btn-station">
                        <button className="btnstation mr-2"><div className="mt-1">สถานีปั้มน้ำมัน</div></button>
                        <button className="btnstation detail text-station"><div className="mt-1">บริการอื่น ๆ</div></button>
                    </div>
                </div>

                <div className="mobile-pt mb-4">
                    <div className="title-green text-station mt-3">ค้นหาสถานีบริการ</div>
                    <img className="picture-station mt-4" src={`${process.env.PUBLIC_URL}/images/s-location.jpg`}/>
                    <div className="setmb-station pd-2">
                        <span><button className="btnstation"><span className="mt-2">สถานีปั้มน้ำมัน</span></button></span>
                        <span><button className="btnstation detail"><span className="mt-2">บริการอื่น ๆ</span></button></span>
                    </div>
                </div>

                <div className="mb-5">
                    <div className="title-green">สถานที่แนะนำ</div>
                    <div className="text-center">
                        <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place01.jpg`} /></div>
                        <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place02.jpg`} /></div>
                        <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place03.jpg`} /></div>
                        <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place04.jpg`} /></div>
                        <div className="set-inline"><img className="place-rec" src={`${process.env.PUBLIC_URL}/images/place04.jpg`} /></div>
                        <div className="mb-5" style={{marginBottom:'100'}}>
                            <div className="mobile-pt seeAllright"><button className="btn-custom border-rdss bb foot"><div className="font-recommend">ดูทั้งหมด</div></button></div>
                            <div className="pc-pt seeAllright mb-5">
                                <button className="btn-custom border-rds bb foot">
                                    <div className="font-recommend">ดูทั้งหมด</div>
                                </button>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div> 
                {this.state.modal}
            </div>
        )
    }
}