import React from 'react';
import '../../ptmax/ptmax.css';
import { maxcardActions } from '../../../_actions/maxcardActions'; 

export class myMaxCard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
           dataCard : [],
           showLoading: 'block',
           content_page: 'none',
        };
    }
    componentDidMount(){
        this.closeNav();
        this.closeNa();
        this.getCard();
    }

    getCard(){
        var userid = localStorage.getItem('uid');
        var showLoading = '';
        var  content_page = '';
        maxcardActions.getCardList(userid).then(e => {
            console.log(e)
            if(e.data.isSuccess === true){
                showLoading = 'none';
                content_page = 'block';
                this.setState({ dataCard : e.data.data })
            }else{

            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    openNav() {
        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("main").style.marginLeft = "300px";
    }

    openNa(){
        document.getElementById("myNav").style.width = "100%";
    }

    closeNa(){
        document.getElementById("myNav").style.width = "0%";
    }
      
    closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
    }

    render(){
        var cardList = this.state.dataCard;
        var disPlayCard = [];
        for(var i in cardList){
            disPlayCard.push(
                <div>
                    <div className="pc-pt mt-card row">
                        <div className="col-xs-6 col-sm-6 col-md-6 col-lg-5">
                            <img className="img-card" src={cardList[i].carD_IMAGE} />
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6 col-lg-7 bd-card-control">
                                <div className="name-card">{cardList[i].carD_TYPE_NAME}</div>
                                <div className="ptno-card mt-1">{cardList[i].carD_NO}</div>
                            
                            <div className="row">
                                <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3 cl-7-point">
                                    <div className="point-card">{parseInt(cardList[i].carD_POINT)}</div>
                                    <div className="set-point">คะแนน</div>
                                </div>
                                <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2"><hr className="mr-border" /></div>
                                <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7 cl-7-point">
                                    <div className="point-card">{parseInt(cardList[i].carD_ESTAMP)}</div>
                                    <div className="set-point">คะแนน</div>
                                </div>
                            </div>
                            <div className="detail-point">{parseInt(cardList[i].poinT_WILL_EXPIRED)} คะแนน | {parseInt(cardList[i].estamP_WILL_EXPIRED)} คะแนน E-Stamp จะหมดอายุวันที่ 28-12-19</div>
                            <div>
                                <button className="insert-reward">สิทธิพิเศษที่สามารถใช้ได้</button>
                            </div>
                        </div>
                    </div>
                    <div className="mobile-pt row">
                        <div className="col-xs-6 col-sm-6 col-md-6 col-lg-5">
                            <img className="img-card" src={cardList[i].carD_IMAGE} />
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6 col-lg-7 bd-card-control">
                                <div className="name-card">{cardList[i].carD_TYPE_NAME}</div>
                                <div className="ptno-card mt-1">{cardList[i].carD_NO}</div>
                            
                            <div className="row">
                                <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3 cl-7-point">
                                    <div className="point-card">{parseInt(cardList[i].carD_POINT)}</div>
                                    <div className="set-point">คะแนน</div>
                                </div>
                                <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2"><hr className="mr-border" /></div>
                                <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7 cl-7-point">
                                    <div className="point-card">{parseInt(cardList[i].carD_ESTAMP)}</div>
                                    <div className="set-point">คะแนน</div>
                                </div>
                            </div>
                            <div className="detail-point">{parseInt(cardList[i].poinT_WILL_EXPIRED)} คะแนน | {parseInt(cardList[i].estamP_WILL_EXPIRED)} คะแนน E-Stamp จะหมดอายุวันที่ 28-12-19</div>
                            <div>
                                <button className="insert-reward">สิทธิพิเศษที่สามารถใช้ได้</button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return(
            <div>
                <div id="loading" style={{display : this.state.showLoading}}>
                    <div className="content_class">
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                </div>
                <div className="content_page" style={{ display: this.state.content_page}}>
                    <div className="bg-ptmax">
                        <div id="mySidenav" className="sidenav pc-pt">
                            <div className="font-user-profile">
                                <img className="icon-width" src={`${process.env.PUBLIC_URL}/images/iconlogin-01.png`} />
                                <span className="pl-2">{localStorage.getItem('user')}</span>
                            </div>
                            <button onClick={() => this.closeNav()}>
                                <div className="closebtn">&times;</div>
                            </button>
                            <div className="block-navbar">
                                <a href={`${process.env.PUBLIC_URL}/ptmaxcard`}>บัตรพีที แมกซ์ ของฉัน</a>
                            </div>
                            <div className="block-navbar">
                                <a href="#">แก้ไขข้อมูลส่วนตัว</a>
                            </div>
                            <div className="block-navbar">
                                <a href="#">ประวัติการใช้งาน</a>
                            </div>
                            <div className="block-navbar">
                                <a href={`${process.env.PUBLIC_URL}/maxcard/report`}>แจ้งปัญหาการใช้งาน</a>
                            </div>
                            <div className="block-navbar">
                                <a href="#">เงื่อนไขและการบริการ</a>
                            </div>
                            <div className="block-navbar">
                                <a href="#">คำถามที่พบบ่อย</a>
                            </div>
                            <div className="block-navbar">
                                <a className="signout-font" href="#">ออกจากระบบ</a>
                            </div>
                        </div>
                        <div id="myNav" className="overlay">
                                <div>
                                    <button className="closebtn" onClick={() => this.closeNa()}>&times;</button>
                                </div>
                                <div className="font-user-profile mt-5">
                                    <img className="icon-width" src={`${process.env.PUBLIC_URL}/images/iconlogin-01.png`} />
                                    <span className="pl-2">{localStorage.getItem('user')}</span>
                                </div>
                            <div className="overlay-content">
                                <a href={`${process.env.PUBLIC_URL}/ptmaxcard`}>บัตรพีที แมกซ์ ของฉัน</a>
                                <a href="#">แก้ไขข้อมูลส่วนตัว</a>
                                <a href="#">ประวัติการใช้งาน</a>
                                <a href={`${process.env.PUBLIC_URL}/maxcard/report`}>แจ้งปัญหาการใช้งาน</a>
                                <a href="#">เงื่อนไขและการบริการ</a>
                                <a href="#">คำถามที่พบบ่อย</a>
                                <a className="signout-font" href="#">ออกจากระบบ</a>
                            </div>
                        </div>
                        <div id="main">
                            <div className="headet-ptmaxcard pc-pt">บัตรพีที แมกซ์ ของฉัน</div>
                            <div className="headet-ptmaxcard mobile-pt">บัตรพีที แมกซ์ ของฉัน</div>
                            <span className="mobile-pt span-heading" style={{cursor:'pointer'}} onClick={() => this.openNa()}><i className="fas fa-chevron-left"></i>&nbsp;&nbsp; บัตรพีที แมกซ์ ของฉัน</span>
                            <span className="pc-pt span-heading" style={{cursor:'pointer'}} onClick={() => this.openNav()}><i className="fas fa-chevron-left"></i>&nbsp;&nbsp; บัตรพีที แมกซ์ ของฉัน</span>
                                {disPlayCard}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


