import React from 'react';
import '../../privileges/privileges.css';


export class Detail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            setIcon : 'fas fa-plus',
            chkIcon : 'false'
        };
    }

    checkIcon(e){
        if(e.target.id === 'false'){
            this.setState({ chkIcon : 'true' , setIcon : 'fas fa-minus'});
        }else{
            this.setState({ chkIcon : 'false' , setIcon : 'fas fa-plus' });
        }
    }

    openNav(){
        window.location.href = `${process.env.PUBLIC_URL}/privileges`;
    }

    render(){
        return(
            <div>
                <div className="pc-pt container-fluid bg-web-pri">
                    <div className="py-5">
                        <span className="span-heading" style={{cursor:'pointer'}} onClick={() => this.openNav()}><i className="fas fa-chevron-left icon-back"></i><span className="pl-3 arrow-previous">สิทธิพิเศษและของรางวัล</span></span>
                    </div>
                    <div className="row">
                        <div className="col-6 text-center">
                            <img style={{width:'50%'}} src={`${process.env.PUBLIC_URL}/images/TC-PTMajor360x360(2)px-01.jpg`}/>
                        </div>
                        <div className="col-6">
                            <div className="header-name-reward">
                                Mc Donald เลือกรับฟรีเบอร์เกอร์ 1 ชิ้น
                            </div>
                            <div className="subheader-reward">
                                (ซามูไรเบอร์เกอร์หมู , แมคฟิช , แมคไก่)
                            </div>
                            <div className="subpoint-reward">
                                180 คะแนน
                            </div>
                            <div className="bd-insertrd">
                                <button className="insertedeem">แลกแต้ม</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="headdetail-reward">รายละเอียดของรางวัล</div>
                        <div className="subdetail-reward">สมาชิกใช้คะแนน 180 คะแนน</div>
                        <div className="headdetail-reward">ระยะเวลาในการแลกของรางวัล</div>
                        <div className="subdetail-reward">รับสิทธิพิเศษตั้งแต่วันที่ 05/09/2019 ถึง 31/12/2019</div>
                    </div>
                    <div className="mt-left-re">
                        <button className="btn btn-primary dot" id={this.state.chkIcon} type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" onClick={(e) => this.checkIcon(e)}>
                            <i className={`${this.state.setIcon} icon-dot`} ></i>
                        </button>
                        <span className="condition-redeem">เงื่อนไขในการแลกของรางวัล</span>
                    </div>
                </div>
                <div className="collapse" id="collapseExample">
                    <div className="card card-body card-condition">
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                        <p>1.สิทธิพิเศษสำหรับสมาชิก PT Max card กรุณาแสดงรหัสที่ร้านค้าพร้อมมือถือที่ใช้ก่อนรับสิทธิ์</p>
                    </div>
                </div>
            </div>
        )
    }
}