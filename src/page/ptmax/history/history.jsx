import React from 'react';
import '../../ptmax/ptmax.css';


export class History extends React.Component {
    constructor(props){
        super(props);
        this.state = {
           
        };
    }
    componentDidMount(){
        this.closeNav();
    }

    openNav() {
        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("main").style.marginLeft = "300px";
    }
      
    closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
    }

    render(){
        return(
            <div>
                <div className="bg-ptmax">
                    <div id="mySidenav" className="sidenav">
                        <div className="font-user-profile">
                            <img className="icon-width" src={`${process.env.PUBLIC_URL}/images/iconlogin-01.png`} />&nbsp;&nbsp;
                            Ptmaxpro09
                        </div>
                        <button onClick={() => this.closeNav()}>
                            <div className="closebtn">&times;</div>
                        </button>
                        <div className="block-navbar">
                            <a href={`${process.env.PUBLIC_URL}/ptmaxcard`}>บัตรพีที แมกซ์ ของฉัน</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">แก้ไขข้อมูลส่วนตัว</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">ประวัติการใช้งาน</a>
                        </div>
                        <div className="block-navbar">
                            <a href={`${process.env.PUBLIC_URL}/maxcard/report`}>แจ้งปัญหาการใช้งาน</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">เงื่อนไขและการบริการ</a>
                        </div>
                        <div className="block-navbar">
                            <a href="#">คำถามที่พบบ่อย</a>
                        </div>
                        <div className="block-navbar">
                            <a className="signout-font" href="#">ออกจากระบบ</a>
                        </div>
                    </div>
                    <div id="main">
                        <div className="headet-ptmaxcard">ประวัติการใช้งาน</div>
                        <span className="span-heading" style={{cursor:'pointer'}} onClick={() => this.openNav()}><i className="fas fa-chevron-left"></i>&nbsp;&nbsp; บัตรพีที แมกซ์ ของฉัน</span>
                        <div className="mt-3">
                        <nav>
                            <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#history-earn" role="tab" aria-controls="history-earn" aria-selected="true">ประวัติการสะสมคะแนน</a>
                                <a className="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#history-burn" role="tab" aria-controls="history-burn" aria-selected="false">ประวัติการแลกคะแนน</a>
                                <a className="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#history-total" role="tab" aria-controls="history-total" aria-selected="false">คะแนนหมดอายุ</a>
                            </div>
                        </nav>

                        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                            <div id="history-earn" className="tab-pane fade show active box-history" role="tabpanel" aria-labelledby="history-earn">
                                <div className="select-date">เลือกวันที่ทำรายการ</div>
                                <select className="dropdown-history">
                                    <option>เดือน</option>
                                </select>
                                <select className="dropdown-history">
                                    <option>ปี</option>
                                </select>
                                <div className="text-center">
                                    <button className="search-text">ค้นหา</button>
                                </div>
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-screen">+ 45.00</div>
                                    </div>
                                </div>
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-screen">+ 45.00</div>
                                    </div>
                                </div>
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-screen">+ 45.00</div>
                                    </div>
                                </div>
                                <div className="next-arrow">
                                    <i className="inextarrow fas fa-chevron-right"></i>
                                </div>
                            </div>
                        

                            <div className="tab-pane fade box-history" id="history-total" role="tabpanel" aria-labelledby="history-total">
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-burn">- 45.00</div>
                                    </div>
                                </div>
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-burn">- 45.00</div>
                                    </div>
                                </div>
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-burn">- 45.00</div>
                                    </div>
                                </div>
                                <div className="next-arrow">
                                    <i className="inextarrow fas fa-chevron-right"></i>
                                </div>
                            </div>

                            <div className="tab-pane fade box-history" id="history-burn" role="tabpanel" aria-labelledby="history-burn">
                                <div className="select-date">เลือกวันที่ทำรายการการ</div>
                                <select className="dropdown-history">
                                    <option>เดือน</option>
                                </select>
                                <select className="dropdown-history">
                                    <option>ปี</option>
                                </select>
                                <div className="text-center">
                                    <button className="search-text">ค้นหา</button>
                                </div>
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-burn">- 45.00</div>
                                    </div>
                                </div>
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-burn">- 45.00</div>
                                    </div>
                                </div>
                                <div className="row bt-border">
                                    <div className="col-8">
                                        <div className="name-history">เติมน้ำมันพีที สาขาตลิ่งชัน</div>
                                        <div className="detail-history">24 ตุลาคม 2562 เวลา 19.00 น.</div>
                                    </div>
                                    <div className="col-4">
                                        <div className="point-burn">- 45.00</div>
                                    </div>
                                </div>
                                <div className="next-arrow">
                                    <i className="inextarrow fas fa-chevron-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}


